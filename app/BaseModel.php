<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Model as TraitModel;

class BaseModel extends Model
{
	use SoftDeletes;
	use TraitModel;
	
	protected $appends = ['created_formated','created_humans'];

	


	public static function createslug($model){
		$slug_base = uniqid();
		if($model->slug != ""){
			$slug_base = $model->slug;
		}else if(\Schema::hasColumn($model->getTable(),"name")){
			$slug_base = $model->name;
		}else if(\Schema::hasColumn($model->getTable(),"title")){
			$slug_base = $model->title;
		}else if(\Schema::hasColumn($model->getTable(),"designation")){
			$slug_base = $model->designation;
		}
		
		$slug_base = str_slug($slug_base);
		$slug = $slug_base;
		
		$get = true;
		$i = 1;
		while($get){
			
			$isexist=\DB::table($model->getTable())->where('slug',$slug)->where('id','<>',$model->id)->count();
			if($isexist > 0){
				$slug = $slug_base.'-'.$i;
			}else{
				$slug_base = $slug;
				$get = false;
			}
			
			$i++;
		}
		
		return $slug_base;
	}
		 
}
