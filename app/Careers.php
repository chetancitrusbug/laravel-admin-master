<?php
    namespace App;
    use App\BaseModel;
    class Careers extends BaseModel
    {
        //declare Tabel Name
        protected $table = "career";
    
            //declare Fillable Variable
            protected $fillable = [
                'designation','description_title','description','opportunity','position','slug','ordering','work_type','status' 
            ];
         
    }
?>