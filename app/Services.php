<?php
namespace App;

use App\BaseModel;
 
class Services extends BaseModel

{
    protected $table = "services";
    protected $fillable = [
        'title','slug','intro','description' , 'service_id', 'icon'
    ];

    public function servicedetails(){
        return $this->hasMany('App\Servicedetails');
    }

    public  function projects()
    {
        return $this->belongsToMany('App\Projects', 'projects_services','service_id','project_id');
    }

    public  function technologies()
    {
        return $this->belongsToMany('App\Technologies', 'services_technology','service_id','tech_id');
    }
 
}

