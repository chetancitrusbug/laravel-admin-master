<?php
    namespace App;
    use App\BaseModel;
    class Testimonials extends BaseModel
    {
        //declare Tabel Name
        protected $table = "testimonials";
    
            //declare Fillable Variable
            protected $fillable = [
                'name','position','title','description' 
            ];
         
        public  function projects()
        {
            return $this->belongsToMany('App\Projects', 'projects_testimonials','testimonial_id','project_id');
        }
    }
?>