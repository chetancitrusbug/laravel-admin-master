<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder; 
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::component('layouts.admin.fields.relation.multiselect', 'multiselect'); 

        // Blade::component('layouts.admin.fields.relation.multiselect', 'multiselect'); 

        //
        Builder::defaultStringLength(191);

        // View::composer(
        //     '*', 'App\Http\View\Composers\ModuleComposer'
        // );
                          
        

    }
}
