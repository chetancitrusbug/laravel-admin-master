<?php
namespace App;

use App\BaseModel;
 
class Projects extends BaseModel

{
    protected $table = "projects";
    
    protected $fillable = [
        'title','slug','desc','description', 'sow', 'menu', 'status',   'ordering'
    ];

    public  function technologies()
    {
        return $this->belongsToMany('App\Technologies', 'projects_tech','project_id','tech_id');
    }
    

    public  function services()
    {
        return $this->belongsToMany('App\Services', 'projects_services','project_id','service_id');
    }

    public  function testimonials()
    {
        return $this->belongsToMany('App\Testimonials', 'projects_testimonials','project_id','testimonial_id');
    } 

    public function projects_single_image()
    {
        return $this->hasMany('App\Refefile', 'refe_field_id', 'id')
        ->where('refe_table_name', $this->getTable())
        ->where('file_type','projects_single_image')
        ->orderby("priority","DESC")
        ->orderby("created_at","DESC")
        ;
    }
 
}

