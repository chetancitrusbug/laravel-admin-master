<?php

namespace App;

use App\BaseModel;

class Categories extends BaseModel
{
	protected $table = 'categories';

    
    protected $primaryKey = 'id';
	
    protected $fillable = [
        'name','slug','icon','parent_id','ordering' 
    ];
	
	
	public function child()
    {
        return $this->hasMany('App\Categories', 'parent_id', 'id');
    }
	public function parent()
    {
        return $this->belongsTo('App\Categories', 'parent_id', 'id');
    }
	 public function scopeParentonly($query)
    {
        return $query->where('parent_id', 0);
    }
	public function scopeChildonly($query)
    {
        return $query->where('parent_id','>',0);
    }
	public function refefile()
    {
        return $this->hasMany('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'cat_id');
    }
}
