<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Inquiries;

class InquiryRecived extends Mailable 
{
    use Queueable, SerializesModels;

    protected $inquiry;
    protected $admin;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inquiry, $admin = false)
    {
        $this->inquiry = $inquiry;
        $this->admin = $admin;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $layout = 'emails.inquiries.recived';
        if($this->admin){
            $layout = 'emails.admin.inquiries.recived';
        }
        return $this->markdown($layout)
        ->with([
            'inquiry' => $this->inquiry,
        ]);     
    }
}
