<?php 
 namespace App\Traits;
 use Auth;
 use App\BaseModel;
 trait Model
 {
	protected  static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
			$user = Auth::user();
		 
			if (\Schema::hasColumn($model->getTable(),"created_by") && $user) {
                $model->created_by = $user->id;
               
			}

			if (\Schema::hasColumn($model->getTable(),"updated_by") && $user) {
                $model->updated_by = $user->id;
            }
			 
			
			if(\Schema::hasColumn($model->getTable(),"slug")){
				$model->slug = BaseModel::createslug($model);
            }
            
            if(\Schema::hasColumn($model->getTable(),"status")){
				$model->status = $model->status == "on" ? 1 : 0;
			}

			if(\Schema::hasColumn($model->getTable(),"param")){

				

				$model->status = $model->status == "on" ? 1 : 0;
			}

        });

        static::updating(function ($model) {
			$user = Auth::user();
		 
		
			if (\Schema::hasColumn($model->getTable(),"created_by") && $user) {
                $model->created_by = $user->id;
               
			}

			if (\Schema::hasColumn($model->getTable(),"updated_by") && $user) {
                $model->updated_by = $user->id;
            }
			 
			
			if(\Schema::hasColumn($model->getTable(),"slug")){
				$model->slug = BaseModel::createslug($model);
			}
        });

        static::created(function ($model) {

			// $request = request();

			// $relations = request('relation', null);
			// dd($relations);
			// if($relations){
			// 	foreach($relations as $md=>$keys){
			// 		$relation = $md;
			// 		$relationMethod = $model->$relation();
			// 		$relationMethod->detach(); 
			// 		//$mdl = 'App\\'.$md;
			// 		foreach($keys as $key){
			// 			$relationMethod->attach($key); 
			// 		} 
					
			// 	}
			// }
 

           
        });
        static::updated(function ($model) {

			 

            // $relations = request('relation', null);
			// dd($relations);
			// if($relations){
			// 	foreach($relations as $md=>$keys){
			// 		$relation = $md;
			// 		$relationMethod = $model->$relation();
			// 		dd($relationMethod);
			// 		$relationMethod->detach(); 
			// 		//$mdl = 'App\\'.$md;
			// 		foreach($keys as $key){
			// 			$relationMethod->attach($key); 
			// 		} 
					
			// 	}
			// }

        });
        static::deleted(function ($model) {
            
        });
        
    }
    
    public function getCreatedHumansAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->diffForHumans();
        }
		return $this->created_at;
    }
	public function getCreatedFormatedAttribute()
    {
		if($this->created_at != ""){
           return \Carbon\Carbon::parse($this->created_at)->format(\config('settings.date_format'));
        }
		return $this->created_at;
    }
    public function onefile()
    {
        return $this->hasOne('App\Refefile', 'refe_field_id', 'id')
		->where('refe_table_name', $this->getTable())
		->orderby("priority","DESC")
		->orderby("created_at","DESC");
    }
	public function manyfile() 
    {
        return $this->hasMany('App\Refefile', 'refe_field_id', 'id')
		->where('refe_table_name', $this->getTable())
		->orderby("priority","DESC")
		->orderby("created_at","DESC");
		
    }

 }   