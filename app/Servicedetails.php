<?php
namespace App;

use App\BaseModel;
 
class Servicedetails extends BaseModel

{
    protected $table = "servicedetails";
    protected $fillable = [
        'title','slug','desc','description', 'service_id'
    ];

 
    public function service(){
        return $this->hasOne('App\Services');
    }

}

