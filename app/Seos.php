<?php
    namespace App;
    use App\BaseModel;
    class Seos extends BaseModel
    {
        //declare Tabel Name
        protected $table = "seos";
    
            //declare Fillable Variable
            protected $fillable = [
                'url','title','description','og_type','twitter_card' 
            ];
         
    }
?>