<?php

namespace App\Http\Middleware\Site;

use App\Technologies;
use App\Settings;
use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ModuleTechnologies
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
         
        $technologies = \Cache::remember('site_technologies', 60*24, function () {
            return Technologies::with('technologies_single_main')->where('featured','on')->orderby('ordering')->get();
        });

        // $technologies =  Technologies::with('technologies_single_main')->where('featured','on')->orderby('ordering')->get();
         
        
        View::share('technologies',$technologies);

        $response = $next($request);
        
        return $response;
    }
}
