<?php

namespace App\Http\Middleware\Site;

use App\Testimonials;
use App\Settings;
use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ModuleTestimonial
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Perform action before
        // $services = Services::get();
        $testimonials = \Cache::remember('site_testimonials', 60*24, function () {
            return Testimonials::with('manyfile')->orderby('ordering')->get();
        });
        
        View::share('testimonials',$testimonials);

        $response = $next($request);
        
        return $response;
    }
}
