<?php

namespace App\Http\Middleware\Site;

use App\Services;
use App\Settings;
use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ModuleServices
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Perform action before
        // $services = Services::get();
        $services = \Cache::remember('site_services', 60*24, function () {
            return Services::with('manyfile')->orderby('ordering')->get();
        });
        
        View::share('services',$services);

        $response = $next($request);
        
        return $response;
    }
}
