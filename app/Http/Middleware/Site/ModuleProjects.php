<?php

namespace App\Http\Middleware\Site;

use App\Projects;
use App\Settings;
use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ModuleProjects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Perform action before
        //$projects = Projects::with('manyfile')->get();
        $projects = \Cache::remember('site_projects', 60*24, function () {
            return Projects::with(['projects_single_image','technologies'])->orderby('ordering')->get();
        });

        // $menuProjects = \Cache::remember('site_projects', 60*24, function () {
        //     return Projects::with('manyfile')->get();
        // });

        $menuProjects = \Cache::remember('site_menu_projects', 60*24, function () {
            return Projects::with('projects_single_image')->orderby('ordering')->where('menu','1')->get();
        });
        
        
        
        View::share('projects',$projects);
        View::share('menuProjects',$menuProjects);

        $response = $next($request);
         
        return $response;
    }
}
