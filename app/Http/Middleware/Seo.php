<?php

namespace App\Http\Middleware;

use App\Seos;
use App\Settings;
use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class Seo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Perform action before
        $url = $request->getrequestUri(); 
        // dd('_seo_link_'.$url);
        // $seoLink = \Cache::remember('_seo_link_'.$url, 60*24, function () use ($url) {
        //     return Seos::where('url',$url)->first();
        // });

         $seoLink = Seos::where('url',$url)->first();

      

        $inValidUrlTags = [
            '.js','.css','.png','.jpg', 'media-manager' ,'stylesheets?v', '/datatable?','javascript?v','/admin/','/_debugbar/','/images/'
        ];
        $isinValid = false;
        foreach($inValidUrlTags as $inValidUrlTag){
             
            if(strpos($url,$inValidUrlTag) !== false){
                $isinValid = true;
            }
        }  
        if($isinValid === false  ){
            if(!$seoLink){
                Seos::create([
                    'url'=>$url
                ]);
            }
        }

      

        if($seoLink){
            $seo['site_name'] = config('app.name', 'Laravel');
            $seo['url'] = $request->url();
            $seo['fb_admins'] = getSetting('seos','fb_admins');
            if($seoLink){
                $seo['title']= $seoLink->title ? $seoLink->title : getSetting('seos','default_title');
                $seo['description']= $seoLink->description ? $seoLink->description : getSetting('seos','default_description');
                $seo['og_type']= $seoLink->og_type ? $seoLink->og_type : getSetting('seos','default_og_type');
                $seo['og_image']= @$seoLink->manyfile->where('file_type','og_image')->first()->file_url;
                $seo['twitter_card']= $seoLink->twitter_card ? $seoLink->twitter_card : getSetting('seos','default_twitter_card');
            }             
            
            View::share('seo',$seo);
        }        
        $response = $next($request);
        // Perform action after
        if(@$response->exception){
            $seoLink = Seos::where('url',$url)->first();
            // add new fields to database for status code and update status code here
            //dd($response->exception->getstatusCode());
        }
        return $response;
    }
}
