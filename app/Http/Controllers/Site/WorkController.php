<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Site\Controller;
use App\Services;
use Illuminate\Support\Str;
use App\Projects;

use Illuminate\Support\Facades\View;
 

use Illuminate\Http\Request; 

class WorkController extends Controller
{
    
    public function __construct() {
         
    } 

    public function index(Request $request){ 

        $services = Services::get();
        $works = Projects::get();
 

        $obj = new  \stdClass();
        $obj->title = 'Works';
        $obj->url = route('site.works');
        $breadcrumbs[] = $obj;

        $obj = new  \stdClass();
        $obj->title = 'Home';
        $obj->url = route('site.home');
        $breadcrumbs[] = $obj;


        return view('site.work',[
            'services'=>$services,
            'works'=>$works,
            'breadcrumbs'=>$breadcrumbs 
        ]);
    }

    public function work( $slug, Request $request){ 
 
       
        $work = Projects::where('slug',$slug)->first();
        $services = Services::get();

         

        $obj = new \stdClass();
        $obj->title = $work->title;    
        $obj->url = route('site.work',['slug'=>$work->slug]);
        $breadcrumbs[] = $obj;

       
      

        $obj = new  \stdClass();
        $obj->title = 'Works';
        $obj->url = route('site.works');
        $breadcrumbs[] = $obj;

        $obj = new  \stdClass();
        $obj->title = 'Home';
        $obj->url = route('site.home');
        $breadcrumbs[] = $obj;

        
 
        
        return view('site.single-work',[
            'work'=>$work,
            'services'=>$services,
            'breadcrumbs'=>$breadcrumbs 
        ]);

 
    }

}
