<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Site\Controller;
use App\Services;
use App\Careers;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\View;
 

use Illuminate\Http\Request; 

class CareerController extends Controller
{
    
    public function __construct() {
         
    } 

    public function index(Request $request){ 

        $services = Services::get();
        $careers = Careers::where('status','active')->orderBy('ordering','asc')->get();
        $theme = 'dark';

        return view('site.career',[
            'services'=>$services,
            'careers'=>$careers,
            'theme'=>$theme
        ]);
    }

    public function career( $slug, Request $request){ 

        $services = Services::get();
        $career = Careers::where('status','active')->where('slug',$slug)->first();
        return view('site.single-career',[
            'services'=>$services,
            'career'=>$career,
            'slug'=>$slug
        ]);
    }

}
