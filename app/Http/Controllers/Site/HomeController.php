<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Site\Controller;
use App\Baners;
use App\Technologies;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\View;
 

use Illuminate\Http\Request; 
// use App\Projects;

class HomeController extends Controller
{ 
    
    public function __construct() {
         
    } 

    public function index(Request $request){ 

        // $banner = Baners::where('slug','home')->first();

        // $technologies =  Technologies::with('technologies_single_main')->where('featured','on')->orderby('ordering')->get();
         
       
        return view('site.index',[
            // 'banner'=>$banner,
            // 'technologies'=>$technologies
        ]);
    }

}
