<?php

namespace App\Http\Controllers\Site;
use App\Http\Controllers\Site\Controller;
use App\Inquiries;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Mail;
 
use Illuminate\Http\Request; 
use Request as ServerRequest;
use App\Mail\InquiryRecived; 

class InquiriesController extends Controller
{
    use ValidatesRequests;
    
    public function __construct() {
         
    }  

    public function index(Request $request){  

        
    }

    public function save( Request $request){ 

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'company' => 'required',
            'message' => 'required',
            'country_code' => 'required',
        ]);

        $input = $request->except(['']);
        $input['status'] = 0;
        $input['page'] = ServerRequest::server('HTTP_REFERER');
        $input['ip'] = ServerRequest::ip();
         
        $item = Inquiries::create($input);

        Mail::to($request->email)->send(new InquiryRecived($item));
        $emails = ['themalkesh@gmail.com', 'ishan@citrusbug.com','info@citrusbug.com'];
        Mail::to($emails)->send(new InquiryRecived($item,true));

        if($item){
            $result['message'] = trans('common.responce_msg.inquiries_success');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.inquiries_fail');
            $result['code'] = 400;
        }    

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }
         
    }

}
