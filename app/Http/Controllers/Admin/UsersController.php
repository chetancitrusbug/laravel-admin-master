<?php
namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Admin\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache ;

use App\Libraries\Fields\Text;

class UsersController extends Controller
{
    public function __construct() {
        
        $this->context = str_plural('user');
        $this->modal = "App\\".ucfirst($this->context);
        parent::__construct();
        View::share('context',  $this->context);

    } 

    
    function validationrule(){
 
        return [
            
            'name' => [
                'rule' =>'',
                'message' => [
                    '' => trans($this->context.'.validation.name_'),
                ]
            ],
            
            'email' => [
                'rule' =>'',
                'message' => [
                    '' => trans($this->context.'.validation.email_'),
                ]
            ],
            
            'password' => [
                'rule' =>'',
                'message' => [
                    '' => trans($this->context.'.validation.password_'),
                ]
            ],
            
            'status' => [
                'rule' =>'',
                'message' => [
                    '' => trans($this->context.'.validation.status_'),
                ]
            ],
            
            
        ];
 
    }
    
	    
}
?>
