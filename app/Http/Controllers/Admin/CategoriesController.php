<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Rolls;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use DataTables;

use Illuminate\Support\Str;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\View;
use App\Userrolls;
use Auth;

class CategoriesController extends Controller
{
    public function __construct() {
        
        $this->context = 'categories';
        $this->modal = 'App\\'.ucfirst($this->context);
        parent::__construct();
        // canAccessModule($this->context, 'Access');
        View::share('context',  $this->context);
        
    } 
	public function datatable(Request $request) {
        
        $record = $this->modal::with('parent')->where("id",">",0);
		if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '') {
            $record->where('status',$request->get('status'));
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }
        return Datatables::of($record)->make(true);
    }

    public function store(Request $request)
    {
        $result = array();		
		$varr = [
            'name' => 'required',
            'slug' => 'unique:'.$this->context,
        ];
		
		$this->validate($request,$varr);

        $input = $request->except(['']);
        $item = $this->modal::create($input);
        
        if($item){
            Session::flash('flash_success',trans('common.responce_msg.record_created_succes'));
        }else{
            Session::flash('flash_error',trans('common.responce_msg.something_went_wr'));
        }
         return redirect()->route('admin.'.$this->context,['id',$item->id]);
	}

    public function update($id, Request $request)
    {
         
        $varr = [
            'name' => 'required',
            'slug' => 'required|unique:'.$this->context.',slug,' . $id,
        ];
		$this->validate($request,$varr,[],[]);
		
        $item = $this->modal::where("id",$id)->first();
        $requestData = $request->except(['']);
		if($item){
            $item->update($requestData);
			Session::flash('flash_success',trans('common.responce_msg.record_updated_succes'));
		}else{
             Session::flash('flash_error',trans('common.responce_msg.something_went_wr'));
		}
        return redirect()->route('admin.'.$this->context);
    }
}
