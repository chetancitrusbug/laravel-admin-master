<?php

Route::get('/', 'Site\HomeController@index')->name('site.home');
Route::get('/about', 'Site\AboutController@index')->name('site.about');
Route::get('/team', 'Site\TeamController@index')->name('site.team');

Route::get('/career', 'Site\CareerController@index')->name('site.careers');
Route::get('/career/{slug?}', 'Site\CareerController@career')->name('site.career');


Route::get('/work', 'Site\WorkController@index')->name('site.works');
Route::get('/work/{slug?}', 'Site\WorkController@work')->name('site.work');

Route::get('/technology', 'Site\TechnologyController@index')->name('site.technology');
Route::get('/technology/{slug?}', 'Site\TechnologyController@tech')->name('site.tech');

Route::get('/services', 'Site\ServiceController@index')->name('site.services');
Route::get('/services/{slug?}', 'Site\ServiceController@service')->name('site.service');


Route::post('/inquiries/save', 'Site\InquiriesController@save')->name('site.inquiries.save');

Route::get('mailable', function () {
    $invoice = App\Inquiries::find(1);

    return new App\Mail\InquiryRecived($invoice);
});

