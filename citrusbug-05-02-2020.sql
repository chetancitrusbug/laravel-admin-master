/*
SQLyog Community v13.1.4  (64 bit)
MySQL - 10.1.34-MariaDB : Database - citrusbug_laravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`citrusbug_laravel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `citrusbug_laravel`;

/*Table structure for table `baners` */

DROP TABLE IF EXISTS `baners`;

CREATE TABLE `baners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `baners` */

insert  into `baners`(`id`,`name`,`slug`,`description`,`images`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'home','home','<span class=\"span-block main_slide\" data-slide=\"we\" data-slide-theme=\"dark\" id=\"slide-we\">We are reliable</span> <span class=\"span-block bold-line\"> <a class=\"main_slide\" data-slide=\"developement\" data-slide-theme=\"light\" href=\"#\">Developement</a> <a class=\"main_slide\" data-slide=\"partner\" data-slide-theme=\"dark\" href=\"#\">Partner</a></span> <span class=\"span-block\">since 2013</span>','<div class=\"slide slide1\" data-slide=\"we\" data-slide-type=\"image\">\r\n        <div class=\"slide_inner\"  >\r\n            <img src=\"/assets/images/we.jpg\" alt=\"\" srcset=\"\">\r\n        </div>\r\n    </div>\r\n    <div class=\"slide slide2\" data-slide=\"developement\" data-slide-type=\"image\">\r\n        <div class=\"slide_inner\"  >\r\n            <img src=\"/assets/images/development.jpg\" alt=\"\" srcset=\"\">\r\n        </div>\r\n    </div>\r\n    <div class=\"slide slide3\" data-slide=\"partner\" data-slide-type=\"video\">\r\n        <div class=\"slide_inner\"  >\r\n            <video src=\"/assets/images/partner.mp4\" autoplay preload  loop >\r\n                <source src=\"/assets/images/partner.mp4\" type=\"video/mp4\">\r\n            </video>\r\n        </div>\r\n    </div>','2019-12-30 06:59:46','2019-12-30 07:20:54',NULL),
(2,'service','service','<span class=\"span-block main_slide\" data-slide=\"we\" data-slide-theme=\"dark\" id=\"slide-we\">\r\nWeb Design & Development</span> \r\n\r\n<span class=\"span-block bold-line\"> \r\n<a class=\"main_slide\" data-slide=\"developement\" data-slide-theme=\"light\" href=\"#\">DevOps services</a> \r\n<a class=\"main_slide\" data-slide=\"partner\" data-slide-theme=\"dark\" href=\"#\">Artificial Intelligence</a></span> \r\n\r\n<span class=\"span-block\">API Development</span>','<div class=\"slide slide1\" data-slide=\"we\" data-slide-type=\"image\">\r\n        <div class=\"slide_inner\"  >\r\n            <img src=\"/assets/images/we.jpg\" alt=\"\" srcset=\"\">\r\n        </div>\r\n    </div>\r\n    <div class=\"slide slide2\" data-slide=\"developement\" data-slide-type=\"image\">\r\n        <div class=\"slide_inner\"  >\r\n            <img src=\"/assets/images/development.jpg\" alt=\"\" srcset=\"\">\r\n        </div>\r\n    </div>\r\n    <div class=\"slide slide3\" data-slide=\"partner\" data-slide-type=\"video\">\r\n        <div class=\"slide_inner\"  >\r\n            <video src=\"/assets/images/partner.mp4\" autoplay preload  loop >\r\n                <source src=\"/assets/images/partner.mp4\" type=\"video/mp4\">\r\n            </video>\r\n        </div>\r\n    </div>','2019-12-30 07:26:08','2019-12-30 07:42:12',NULL);

/*Table structure for table `cache` */

DROP TABLE IF EXISTS `cache`;

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cache` */

/*Table structure for table `career` */

DROP TABLE IF EXISTS `career`;

CREATE TABLE `career` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `designation` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `opportunity` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) DEFAULT '0',
  `work_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `career_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `career` */

insert  into `career`(`id`,`designation`,`description_title`,`description`,`opportunity`,`position`,`slug`,`ordering`,`work_type`,`status`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Developer','Developer\'s Mission','<p>Citrusbug is a design and experience agency that partners with modern, global brands. At Citrusbug we are on a mission to relentlessly generate smart, creative, forward-thinking solutions in the space between people and technology. As our new Senior Designer, you will be bringing not only your impeccable design skills and passion for art &amp; technology, but also expertise in client management, problem solving and strategic thinking to our Creative Department.</p>\r\n\r\n<p>You&rsquo;ll be part of an international team, comprised of some of the most talented makers, producers, and creative minds in the industry. We are people who aspire</p>\r\n\r\n<p>Citrusbug is a design and experience agency that partners with modern, global brands. At Citrusbug we are on a mission to relentlessly generate smart, creative, forward-thinking solutions in the space between people and technology. As our new Senior Designer, you will be bringing not only your impeccable design skills and passion for art &amp; technology, but also expertise in client management, problem solving and strategic thinking to our Creative Department.</p>\r\n\r\n<p>You&rsquo;ll be part of an international team, comprised of some of the most talented makers, producers, and creative minds in the industry. We are people who aspire</p>','<ul>\r\n                                                <li>\r\n                                                    <span class=\"arrow-icons\"><i class=\"fe fe-arrow-right\"></i></span>\r\n                                                    <p>Citrusbug is a design and experience agency that partners with modern, global brands. </p>\r\n                                                </li>\r\n                                                <li>\r\n                                                    <span class=\"arrow-icons\"><i class=\"fe fe-arrow-right\"></i></span>\r\n                                                    <p>At Citrusbug we are on a mission to relentlessly generate smart, creative, forward-thinking solutions in the space between people and technology. </p>\r\n                                                </li>\r\n                                                <li>\r\n                                                    <span class=\"arrow-icons\"><i class=\"fe fe-arrow-right\"></i></span>\r\n                                                    <p>As our new Senior Designer, you will be bringing not only your impeccable design skills and passion for art & technology, but also expertise in client management, problem solving and strategic thinking to our Creative Department.</p>\r\n                                                </li>\r\n                                                <li>\r\n                                                    <span class=\"arrow-icons\"><i class=\"fe fe-arrow-right\"></i></span>\r\n                                                    <p>You’ll be part of an international team, comprised of some of the most talented makers, producers, and creative minds in the industry. We are people who aspire </p>\r\n                                                </li>\r\n                                            </ul>',1,'developer',1,'FULLTIME','active','2019-12-27 01:11:21','2019-12-27 01:22:58',NULL);

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `display_order` int(11) DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

/*Data for the table `country` */

insert  into `country`(`id`,`iso`,`name`,`nicename`,`iso3`,`numcode`,`phonecode`,`deleted_at`) values 
(1,'AF','AFGHANISTAN','Afghanistan','AFG',4,93,NULL),
(2,'AL','ALBANIA','Albania','ALB',8,355,NULL),
(3,'DZ','ALGERIA','Algeria','DZA',12,213,NULL),
(4,'AS','AMERICAN SAMOA','American Samoa','ASM',16,1684,NULL),
(5,'AD','ANDORRA','Andorra','AND',20,376,NULL),
(6,'AO','ANGOLA','Angola','AGO',24,244,NULL),
(7,'AI','ANGUILLA','Anguilla','AIA',660,1264,NULL),
(8,'AQ','ANTARCTICA','Antarctica',NULL,NULL,0,NULL),
(9,'AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28,1268,NULL),
(10,'AR','ARGENTINA','Argentina','ARG',32,54,NULL),
(11,'AM','ARMENIA','Armenia','ARM',51,374,NULL),
(12,'AW','ARUBA','Aruba','ABW',533,297,NULL),
(13,'AU','AUSTRALIA','Australia','AUS',36,61,NULL),
(14,'AT','AUSTRIA','Austria','AUT',40,43,NULL),
(15,'AZ','AZERBAIJAN','Azerbaijan','AZE',31,994,NULL),
(16,'BS','BAHAMAS','Bahamas','BHS',44,1242,NULL),
(17,'BH','BAHRAIN','Bahrain','BHR',48,973,NULL),
(18,'BD','BANGLADESH','Bangladesh','BGD',50,880,NULL),
(19,'BB','BARBADOS','Barbados','BRB',52,1246,NULL),
(20,'BY','BELARUS','Belarus','BLR',112,375,NULL),
(21,'BE','BELGIUM','Belgium','BEL',56,32,NULL),
(22,'BZ','BELIZE','Belize','BLZ',84,501,NULL),
(23,'BJ','BENIN','Benin','BEN',204,229,NULL),
(24,'BM','BERMUDA','Bermuda','BMU',60,1441,NULL),
(25,'BT','BHUTAN','Bhutan','BTN',64,975,NULL),
(26,'BO','BOLIVIA','Bolivia','BOL',68,591,NULL),
(27,'BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70,387,NULL),
(28,'BW','BOTSWANA','Botswana','BWA',72,267,NULL),
(29,'BV','BOUVET ISLAND','Bouvet Island',NULL,NULL,0,NULL),
(30,'BR','BRAZIL','Brazil','BRA',76,55,NULL),
(31,'IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL,246,NULL),
(32,'BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96,673,NULL),
(33,'BG','BULGARIA','Bulgaria','BGR',100,359,NULL),
(34,'BF','BURKINA FASO','Burkina Faso','BFA',854,226,NULL),
(35,'BI','BURUNDI','Burundi','BDI',108,257,NULL),
(36,'KH','CAMBODIA','Cambodia','KHM',116,855,NULL),
(37,'CM','CAMEROON','Cameroon','CMR',120,237,NULL),
(38,'CA','CANADA','Canada','CAN',124,1,NULL),
(39,'CV','CAPE VERDE','Cape Verde','CPV',132,238,NULL),
(40,'KY','CAYMAN ISLANDS','Cayman Islands','CYM',136,1345,NULL),
(41,'CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140,236,NULL),
(42,'TD','CHAD','Chad','TCD',148,235,NULL),
(43,'CL','CHILE','Chile','CHL',152,56,NULL),
(44,'CN','CHINA','China','CHN',156,86,NULL),
(45,'CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL,61,NULL),
(46,'CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL,672,NULL),
(47,'CO','COLOMBIA','Colombia','COL',170,57,NULL),
(48,'KM','COMOROS','Comoros','COM',174,269,NULL),
(49,'CG','CONGO','Congo','COG',178,242,NULL),
(50,'CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180,242,NULL),
(51,'CK','COOK ISLANDS','Cook Islands','COK',184,682,NULL),
(52,'CR','COSTA RICA','Costa Rica','CRI',188,506,NULL),
(53,'CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384,225,NULL),
(54,'HR','CROATIA','Croatia','HRV',191,385,NULL),
(55,'CU','CUBA','Cuba','CUB',192,53,NULL),
(56,'CY','CYPRUS','Cyprus','CYP',196,357,NULL),
(57,'CZ','CZECH REPUBLIC','Czech Republic','CZE',203,420,NULL),
(58,'DK','DENMARK','Denmark','DNK',208,45,NULL),
(59,'DJ','DJIBOUTI','Djibouti','DJI',262,253,NULL),
(60,'DM','DOMINICA','Dominica','DMA',212,1767,NULL),
(61,'DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214,1809,NULL),
(62,'EC','ECUADOR','Ecuador','ECU',218,593,NULL),
(63,'EG','EGYPT','Egypt','EGY',818,20,NULL),
(64,'SV','EL SALVADOR','El Salvador','SLV',222,503,NULL),
(65,'GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226,240,NULL),
(66,'ER','ERITREA','Eritrea','ERI',232,291,NULL),
(67,'EE','ESTONIA','Estonia','EST',233,372,NULL),
(68,'ET','ETHIOPIA','Ethiopia','ETH',231,251,NULL),
(69,'FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238,500,NULL),
(70,'FO','FAROE ISLANDS','Faroe Islands','FRO',234,298,NULL),
(71,'FJ','FIJI','Fiji','FJI',242,679,NULL),
(72,'FI','FINLAND','Finland','FIN',246,358,NULL),
(73,'FR','FRANCE','France','FRA',250,33,NULL),
(74,'GF','FRENCH GUIANA','French Guiana','GUF',254,594,NULL),
(75,'PF','FRENCH POLYNESIA','French Polynesia','PYF',258,689,NULL),
(76,'TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL,0,NULL),
(77,'GA','GABON','Gabon','GAB',266,241,NULL),
(78,'GM','GAMBIA','Gambia','GMB',270,220,NULL),
(79,'GE','GEORGIA','Georgia','GEO',268,995,NULL),
(80,'DE','GERMANY','Germany','DEU',276,49,NULL),
(81,'GH','GHANA','Ghana','GHA',288,233,NULL),
(82,'GI','GIBRALTAR','Gibraltar','GIB',292,350,NULL),
(83,'GR','GREECE','Greece','GRC',300,30,NULL),
(84,'GL','GREENLAND','Greenland','GRL',304,299,NULL),
(85,'GD','GRENADA','Grenada','GRD',308,1473,NULL),
(86,'GP','GUADELOUPE','Guadeloupe','GLP',312,590,NULL),
(87,'GU','GUAM','Guam','GUM',316,1671,NULL),
(88,'GT','GUATEMALA','Guatemala','GTM',320,502,NULL),
(89,'GN','GUINEA','Guinea','GIN',324,224,NULL),
(90,'GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624,245,NULL),
(91,'GY','GUYANA','Guyana','GUY',328,592,NULL),
(92,'HT','HAITI','Haiti','HTI',332,509,NULL),
(93,'HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL,0,NULL),
(94,'VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336,39,NULL),
(95,'HN','HONDURAS','Honduras','HND',340,504,NULL),
(96,'HK','HONG KONG','Hong Kong','HKG',344,852,NULL),
(97,'HU','HUNGARY','Hungary','HUN',348,36,NULL),
(98,'IS','ICELAND','Iceland','ISL',352,354,NULL),
(99,'IN','INDIA','India','IND',356,91,NULL),
(100,'ID','INDONESIA','Indonesia','IDN',360,62,NULL),
(101,'IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364,98,NULL),
(102,'IQ','IRAQ','Iraq','IRQ',368,964,NULL),
(103,'IE','IRELAND','Ireland','IRL',372,353,NULL),
(104,'IL','ISRAEL','Israel','ISR',376,972,NULL),
(105,'IT','ITALY','Italy','ITA',380,39,NULL),
(106,'JM','JAMAICA','Jamaica','JAM',388,1876,NULL),
(107,'JP','JAPAN','Japan','JPN',392,81,NULL),
(108,'JO','JORDAN','Jordan','JOR',400,962,NULL),
(109,'KZ','KAZAKHSTAN','Kazakhstan','KAZ',398,7,NULL),
(110,'KE','KENYA','Kenya','KEN',404,254,NULL),
(111,'KI','KIRIBATI','Kiribati','KIR',296,686,NULL),
(112,'KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408,850,NULL),
(113,'KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410,82,NULL),
(114,'KW','KUWAIT','Kuwait','KWT',414,965,NULL),
(115,'KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417,996,NULL),
(116,'LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418,856,NULL),
(117,'LV','LATVIA','Latvia','LVA',428,371,NULL),
(118,'LB','LEBANON','Lebanon','LBN',422,961,NULL),
(119,'LS','LESOTHO','Lesotho','LSO',426,266,NULL),
(120,'LR','LIBERIA','Liberia','LBR',430,231,NULL),
(121,'LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434,218,NULL),
(122,'LI','LIECHTENSTEIN','Liechtenstein','LIE',438,423,NULL),
(123,'LT','LITHUANIA','Lithuania','LTU',440,370,NULL),
(124,'LU','LUXEMBOURG','Luxembourg','LUX',442,352,NULL),
(125,'MO','MACAO','Macao','MAC',446,853,NULL),
(126,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807,389,NULL),
(127,'MG','MADAGASCAR','Madagascar','MDG',450,261,NULL),
(128,'MW','MALAWI','Malawi','MWI',454,265,NULL),
(129,'MY','MALAYSIA','Malaysia','MYS',458,60,NULL),
(130,'MV','MALDIVES','Maldives','MDV',462,960,NULL),
(131,'ML','MALI','Mali','MLI',466,223,NULL),
(132,'MT','MALTA','Malta','MLT',470,356,NULL),
(133,'MH','MARSHALL ISLANDS','Marshall Islands','MHL',584,692,NULL),
(134,'MQ','MARTINIQUE','Martinique','MTQ',474,596,NULL),
(135,'MR','MAURITANIA','Mauritania','MRT',478,222,NULL),
(136,'MU','MAURITIUS','Mauritius','MUS',480,230,NULL),
(137,'YT','MAYOTTE','Mayotte',NULL,NULL,269,NULL),
(138,'MX','MEXICO','Mexico','MEX',484,52,NULL),
(139,'FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583,691,NULL),
(140,'MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498,373,NULL),
(141,'MC','MONACO','Monaco','MCO',492,377,NULL),
(142,'MN','MONGOLIA','Mongolia','MNG',496,976,NULL),
(143,'MS','MONTSERRAT','Montserrat','MSR',500,1664,NULL),
(144,'MA','MOROCCO','Morocco','MAR',504,212,NULL),
(145,'MZ','MOZAMBIQUE','Mozambique','MOZ',508,258,NULL),
(146,'MM','MYANMAR','Myanmar','MMR',104,95,NULL),
(147,'NA','NAMIBIA','Namibia','NAM',516,264,NULL),
(148,'NR','NAURU','Nauru','NRU',520,674,NULL),
(149,'NP','NEPAL','Nepal','NPL',524,977,NULL),
(150,'NL','NETHERLANDS','Netherlands','NLD',528,31,NULL),
(151,'AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530,599,NULL),
(152,'NC','NEW CALEDONIA','New Caledonia','NCL',540,687,NULL),
(153,'NZ','NEW ZEALAND','New Zealand','NZL',554,64,NULL),
(154,'NI','NICARAGUA','Nicaragua','NIC',558,505,NULL),
(155,'NE','NIGER','Niger','NER',562,227,NULL),
(156,'NG','NIGERIA','Nigeria','NGA',566,234,NULL),
(157,'NU','NIUE','Niue','NIU',570,683,NULL),
(158,'NF','NORFOLK ISLAND','Norfolk Island','NFK',574,672,NULL),
(159,'MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580,1670,NULL),
(160,'NO','NORWAY','Norway','NOR',578,47,NULL),
(161,'OM','OMAN','Oman','OMN',512,968,NULL),
(162,'PK','PAKISTAN','Pakistan','PAK',586,92,NULL),
(163,'PW','PALAU','Palau','PLW',585,680,NULL),
(164,'PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL,970,NULL),
(165,'PA','PANAMA','Panama','PAN',591,507,NULL),
(166,'PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598,675,NULL),
(167,'PY','PARAGUAY','Paraguay','PRY',600,595,NULL),
(168,'PE','PERU','Peru','PER',604,51,NULL),
(169,'PH','PHILIPPINES','Philippines','PHL',608,63,NULL),
(170,'PN','PITCAIRN','Pitcairn','PCN',612,0,NULL),
(171,'PL','POLAND','Poland','POL',616,48,NULL),
(172,'PT','PORTUGAL','Portugal','PRT',620,351,NULL),
(173,'PR','PUERTO RICO','Puerto Rico','PRI',630,1787,NULL),
(174,'QA','QATAR','Qatar','QAT',634,974,NULL),
(175,'RE','REUNION','Reunion','REU',638,262,NULL),
(176,'RO','ROMANIA','Romania','ROM',642,40,NULL),
(177,'RU','RUSSIAN FEDERATION','Russian Federation','RUS',643,70,NULL),
(178,'RW','RWANDA','Rwanda','RWA',646,250,NULL),
(179,'SH','SAINT HELENA','Saint Helena','SHN',654,290,NULL),
(180,'KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659,1869,NULL),
(181,'LC','SAINT LUCIA','Saint Lucia','LCA',662,1758,NULL),
(182,'PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666,508,NULL),
(183,'VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670,1784,NULL),
(184,'WS','SAMOA','Samoa','WSM',882,684,NULL),
(185,'SM','SAN MARINO','San Marino','SMR',674,378,NULL),
(186,'ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678,239,NULL),
(187,'SA','SAUDI ARABIA','Saudi Arabia','SAU',682,966,NULL),
(188,'SN','SENEGAL','Senegal','SEN',686,221,NULL),
(189,'CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL,381,NULL),
(190,'SC','SEYCHELLES','Seychelles','SYC',690,248,NULL),
(191,'SL','SIERRA LEONE','Sierra Leone','SLE',694,232,NULL),
(192,'SG','SINGAPORE','Singapore','SGP',702,65,NULL),
(193,'SK','SLOVAKIA','Slovakia','SVK',703,421,NULL),
(194,'SI','SLOVENIA','Slovenia','SVN',705,386,NULL),
(195,'SB','SOLOMON ISLANDS','Solomon Islands','SLB',90,677,NULL),
(196,'SO','SOMALIA','Somalia','SOM',706,252,NULL),
(197,'ZA','SOUTH AFRICA','South Africa','ZAF',710,27,NULL),
(198,'GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL,0,NULL),
(199,'ES','SPAIN','Spain','ESP',724,34,NULL),
(200,'LK','SRI LANKA','Sri Lanka','LKA',144,94,NULL),
(201,'SD','SUDAN','Sudan','SDN',736,249,NULL),
(202,'SR','SURINAME','Suriname','SUR',740,597,NULL),
(203,'SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744,47,NULL),
(204,'SZ','SWAZILAND','Swaziland','SWZ',748,268,NULL),
(205,'SE','SWEDEN','Sweden','SWE',752,46,NULL),
(206,'CH','SWITZERLAND','Switzerland','CHE',756,41,NULL),
(207,'SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760,963,NULL),
(208,'TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158,886,NULL),
(209,'TJ','TAJIKISTAN','Tajikistan','TJK',762,992,NULL),
(210,'TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834,255,NULL),
(211,'TH','THAILAND','Thailand','THA',764,66,NULL),
(212,'TL','TIMOR-LESTE','Timor-Leste',NULL,NULL,670,NULL),
(213,'TG','TOGO','Togo','TGO',768,228,NULL),
(214,'TK','TOKELAU','Tokelau','TKL',772,690,NULL),
(215,'TO','TONGA','Tonga','TON',776,676,NULL),
(216,'TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780,1868,NULL),
(217,'TN','TUNISIA','Tunisia','TUN',788,216,NULL),
(218,'TR','TURKEY','Turkey','TUR',792,90,NULL),
(219,'TM','TURKMENISTAN','Turkmenistan','TKM',795,7370,NULL),
(220,'TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796,1649,NULL),
(221,'TV','TUVALU','Tuvalu','TUV',798,688,NULL),
(222,'UG','UGANDA','Uganda','UGA',800,256,NULL),
(223,'UA','UKRAINE','Ukraine','UKR',804,380,NULL),
(224,'AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784,971,NULL),
(225,'GB','UNITED KINGDOM','United Kingdom','GBR',826,44,NULL),
(226,'US','UNITED STATES','United States','USA',840,1,NULL),
(227,'UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL,1,NULL),
(228,'UY','URUGUAY','Uruguay','URY',858,598,NULL),
(229,'UZ','UZBEKISTAN','Uzbekistan','UZB',860,998,NULL),
(230,'VU','VANUATU','Vanuatu','VUT',548,678,NULL),
(231,'VE','VENEZUELA','Venezuela','VEN',862,58,NULL),
(232,'VN','VIET NAM','Viet Nam','VNM',704,84,NULL),
(233,'VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92,1284,NULL),
(234,'VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850,1340,NULL),
(235,'WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876,681,NULL),
(236,'EH','WESTERN SAHARA','Western Sahara','ESH',732,212,NULL),
(237,'YE','YEMEN','Yemen','YEM',887,967,NULL),
(238,'ZM','ZAMBIA','Zambia','ZMB',894,260,NULL),
(239,'ZW','ZIMBABWE','Zimbabwe','ZWE',716,263,NULL);

/*Table structure for table `inquiries` */

DROP TABLE IF EXISTS `inquiries`;

CREATE TABLE `inquiries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `company` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `country_code` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `inquiries` */

insert  into `inquiries`(`id`,`name`,`email`,`phone`,`status`,`created_at`,`updated_at`,`deleted_at`,`company`,`message`,`country_code`,`ip`,`page`) values 
(1,'Amazon Pay','amazon-pay','https://www.google.com/search?q=Apple Pay',1,'2019-12-27 05:52:57','2020-02-11 01:02:25','2020-02-11 01:02:25',NULL,NULL,NULL,'',''),
(2,'Visa','visa','https://www.google.com//search?q=visa',1,'2019-12-27 05:56:51','2020-02-11 01:02:25','2020-02-11 01:02:25',NULL,NULL,NULL,'',''),
(3,'Paypal','paypal','https://www.google.com/search?q=paypal',1,'2019-12-27 05:57:25','2020-02-11 01:02:26','2020-02-11 01:02:26',NULL,NULL,NULL,'',''),
(4,'Apple Pay','apple-pay','https://www.google.com/search?q=Apple Pay',1,'2019-12-27 05:58:04','2020-02-11 01:02:26','2020-02-11 01:02:26',NULL,NULL,NULL,'',''),
(5,'Transfer Wise','transfer-wise','https://www.google.com/search?q=Transfer Wise',1,'2019-12-27 05:58:57','2020-02-11 01:02:27','2020-02-11 01:02:27',NULL,NULL,NULL,'',''),
(6,'Payoneer','payoneer','https://www.google.com/search?q=Payoneer',1,'2019-12-27 05:59:41','2020-02-11 01:02:28','2020-02-11 01:02:28',NULL,NULL,NULL,'',''),
(7,'Skrill','skrill','https://www.google.com/search?q=Skrill',1,'2019-12-27 06:00:21','2020-02-11 01:02:28','2020-02-11 01:02:28',NULL,NULL,NULL,'',''),
(8,'Stripe','stripe','https://stripe.com/',1,'2019-12-27 06:01:08','2020-02-11 01:02:28','2020-02-11 01:02:28',NULL,NULL,NULL,'',''),
(9,'asd','asd@gmail.com','asd',1,'2020-02-11 05:31:13','2020-02-11 05:31:13',NULL,'ad','asdasd',NULL,NULL,''),
(10,'asdasd','asd@adasd.com','Sdasd',1,'2020-02-11 05:40:11','2020-02-11 05:40:11',NULL,'asdasd','asasdasd',NULL,NULL,''),
(11,'asd','ada@gamil.com','asda',1,'2020-02-11 06:03:48','2020-02-11 06:03:48',NULL,'asd','asda',NULL,'127.0.0.1','http://127.0.0.1:8000/work'),
(12,'asdasdasd','asdasdsad@gamil.com','asdasd',1,'2020-02-12 07:15:35','2020-02-12 07:15:35',NULL,'asdsad','asdsadasd',NULL,'127.0.0.1','http://127.0.0.1:8000/'),
(13,'asdsad','ad@adas.acom','asd',1,'2020-02-12 08:12:36','2020-02-12 08:12:36',NULL,'asdasd','asdasdasd','93','127.0.0.1','http://127.0.0.1:8000/'),
(14,'asdsad','ad@adas.acom','asd',1,'2020-02-12 08:15:25','2020-02-12 08:15:25',NULL,'asdasd','asdasdasd','93','127.0.0.1','http://127.0.0.1:8000/'),
(15,'asdasd','asdasd@gmail.com','asd',1,'2020-02-12 08:22:08','2020-02-12 08:22:08',NULL,'asdasd','asdasdasd','6','127.0.0.1','http://127.0.0.1:8000/'),
(16,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:19:40','2020-02-12 09:19:40',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(17,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:21:12','2020-02-12 09:21:12',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(18,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:24:35','2020-02-12 09:24:35',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(19,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:25:51','2020-02-12 09:25:51',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(20,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:27:25','2020-02-12 09:27:25',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(21,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:27:54','2020-02-12 09:27:54',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(22,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:28:34','2020-02-12 09:28:34',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(23,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:29:13','2020-02-12 09:29:13',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(24,'asdasd','asd@gmail.com','ad',1,'2020-02-12 09:30:12','2020-02-12 09:30:12',NULL,'asd','asd','1','127.0.0.1','http://127.0.0.1:8000/'),
(25,'asd','ad@gmai.com','asd',1,'2020-02-12 09:50:51','2020-02-12 09:50:51',NULL,'asd','asd','0','127.0.0.1','http://127.0.0.1:8000/'),
(26,'asd','ad@gmai.com','asd',1,'2020-02-12 09:51:46','2020-02-12 09:51:46',NULL,'asd','asd','0','127.0.0.1','http://127.0.0.1:8000/');

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `languages` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2019_11_01_112016_create_settings_table',1),
(2,'2019_11_01_112017_create_users_table',1),
(3,'2019_11_11_112144_create_languages_table',1),
(4,'2019_11_11_112222_create_permissions_table',1),
(5,'2019_11_11_112254_create_roles_table',1),
(6,'2019_11_11_112326_create_roles_permission_table',1),
(7,'2019_11_11_112342_create_user_roles_table',1),
(8,'2019_11_12_100000_create_password_resets_table',1),
(9,'2019_11_14_063157_create_category_table',1),
(10,'2019_11_14_071454_create_products_table',1),
(11,'2019_11_14_142410_create_refe_file_table',1),
(12,'2019_12_03_092304_create_seo_table',1),
(13,'2019_12_03_095353_update_seo_table',1),
(14,'2019_12_03_113946_create_cache_table',1),
(15,'2019_12_05_071410_add_module_to_settings_table',1),
(16,'2019_12_06_111339_create_services_table',1),
(17,'2019_12_06_114850_add_updated_by_to_services_table',1),
(18,'2019_12_06_115556_add_icon_to_services_table',1),
(19,'2019_12_12_092458_add_information_field_in_settings_table',1),
(20,'2019_12_13_100742_update_settings_add_fildtype',1),
(21,'2019_12_24_100216_create_projects_table',2),
(22,'2019_12_25_052116_create_servicedetails_table',3),
(23,'2019_12_25_053017_add_service_id_to_servicedetails_table',4),
(24,'2019_12_25_083608_change_services_id_to_servicedetails_table',5),
(25,'2019_12_26_070221_create_table_technology',6),
(27,'2019_12_26_084418_create_team_table',7),
(28,'2019_12_26_083555_add_sow_to_projects_table',8),
(32,'2019_12_26_122451_create_career_table',9),
(33,'2019_12_27_110617_create_partner_table',10),
(34,'2019_12_30_120621_create_baners_table',11),
(35,'2019_12_30_121720_add_dates_to_baners_table',12),
(36,'2020_01_08_073326_create_projects_techs',13),
(37,'2020_01_09_103033_create_projects_services',14),
(39,'2020_01_22_122732_students',15);

/*Table structure for table `partners` */

DROP TABLE IF EXISTS `partners`;

CREATE TABLE `partners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  `partner_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `partners` */

insert  into `partners`(`id`,`name`,`slug`,`ordering`,`partner_url`,`status`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Amazon Pay','amazon-pay',1,'https://www.google.com/search?q=Apple Pay','active','2019-12-27 05:52:57','2019-12-27 06:01:29',NULL),
(2,'Visa','visa',2,'https://www.google.com//search?q=visa','active','2019-12-27 05:56:51','2019-12-27 05:56:51',NULL),
(3,'Paypal','paypal',3,'https://www.google.com/search?q=paypal','active','2019-12-27 05:57:25','2019-12-27 05:57:25',NULL),
(4,'Apple Pay','apple-pay',4,'https://www.google.com/search?q=Apple Pay','active','2019-12-27 05:58:04','2019-12-27 05:58:04',NULL),
(5,'Transfer Wise','transfer-wise',5,'https://www.google.com/search?q=Transfer Wise','active','2019-12-27 05:58:57','2019-12-27 05:58:57',NULL),
(6,'Payoneer','payoneer',6,'https://www.google.com/search?q=Payoneer','active','2019-12-27 05:59:41','2019-12-27 05:59:41',NULL),
(7,'Skrill','skrill',7,'https://www.google.com/search?q=Skrill','active','2019-12-27 06:00:21','2019-12-27 06:00:21',NULL),
(8,'Stripe','stripe',8,'https://stripe.com/','active','2019-12-27 06:01:08','2019-12-27 06:01:08',NULL);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'View','2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(2,'Add','2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(3,'Edit','2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(4,'Delete','2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(5,'Access','2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(6,'Permission','2019-12-19 09:16:21','2019-12-19 09:16:21',NULL);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `price_currency` enum('USD','EUR') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USD',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `products` */

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sow` mediumtext COLLATE utf8mb4_unicode_ci,
  `ordering` int(11) DEFAULT NULL,
  `param` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects` */

insert  into `projects`(`id`,`title`,`slug`,`desc`,`description`,`created_at`,`updated_at`,`deleted_at`,`sow`,`ordering`,`param`,`status`,`menu`) values 
(1,'UVE','uve','La Morra is one of the most beautiful spots in the Bassa Langa del Barolo area, offering splendid views from Piazza Castello out over the Langhe.','<p>UVE is a Boutique Hotel, Wine Bar and Gourmet Restaurant located in La Morra, in the heart of the Langhe, one of Italy&rsquo;s finest food and wine areas, with vineyards that since 2014 have featured on the UNESCO World Heritage list as a cultural landscape.</p>\r\n\r\n<p>Situated in the historic center, the centuries-old building boasts a distinctively sophisticated, contemporary style, able to create a unique atmosphere<br />\r\nand offer guests a warm welcome and a fully rounded experience of the area.</p>\r\n\r\n<p>The warm elegance of the interiors and the exquisite attention to detail come together to shape the inviting ambiance of a private home, where special attention is paid to every guest.</p>','2019-12-23 23:30:46','2020-02-12 03:30:02',NULL,'UI/UX developer\r\nPHP developer\r\nQA',NULL,'a:3:{s:12:\"project_type\";s:6:\"mobile\";s:5:\"title\";s:4:\"aaaa\";s:4:\"desc\";s:22:\"asdas dsa dsad sad asd\";}',NULL,1),
(2,'Perk Coffee','perk-coffee','Perk Coffe a unique experience for coffee lovers','<p>Perk Coffee provides you the unique way to purchase coffee started by coffee-crazy couple Paul and Serena who started their journey from Africa. After traveling from Australia, Kenya, Hawaii, and Kona Big Island in search of great coffee, their spirit of adventure finally brought them to Singapore where they decided to put their roots down. Perk was born out of their passion for great coffee and travel.</p>\r\n\r\n<p>Perk is the platform to get fresh, ethically sourced, roasted weekly right in Singapore and send out your beans within 48 hours of roasting. It also gives you commitment to their customers of an ethical, sustainable product quality. Perk scour the globe to bring you the best beans from around the world. And they proudly support the amazing farmers by ensuring their pay above Fairtrade rates for all the coffees.</p>\r\n\r\n<p>Whether you want your standard brew, or you&rsquo;re searching for something slightly different, Perk&#39;s flexible system allows you to customize your coffee order with just a few clicks!!</p>','2019-12-23 23:31:36','2020-02-12 03:29:57',NULL,'UI/UX developer\r\nPHP developer\r\nQA',NULL,'a:3:{s:12:\"project_type\";s:7:\"website\";s:5:\"title\";N;s:4:\"desc\";N;}',NULL,1),
(3,'Dummy Project','dummy-project','Dummy Project Dummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy Project',NULL,'2019-12-24 00:57:22','2020-01-23 07:54:59','2020-01-23 07:54:59','',NULL,'',NULL,NULL),
(26,'Traverse','traverse','Travrse studios is at the forefront of cinematic storytelling in virtual reality','<p>TraVRse studio is at the forefront of cinematic storytelling in virtual reality. It delivers cutting edge 360-degree films by offering high-quality Virtual Reality content production and consultation.</p>\r\n\r\n<p>With an office brimming with ideas, the Traverse is well equipped to give you the best virtual reality content and all that in 360 degrees. Traverse consists of people with expertise in Cinematography, Editing, Sound-designing, Scripting, Production and VR developers.</p>\r\n\r\n<p>Services they provide:</p>\r\n\r\n<p>Virtual Reality is a rapidly growing promotional strategy. Some of the biggest brands in the world are employing it at events and trade shows,<br />\r\nboth because of the medium&rsquo;s novelty, and the effective way that it can show off products by creating an immersive experience for the customer.<br />\r\nVirtual Reality presentations create an impact that lasts even after the experience has ended. traVRse&rsquo;s Virtual Reality team will create the story behind the experience, produce it, and provide the necessary equipment and knowledge to deliver it to your customers in a live setting.</p>\r\n\r\n<p>360&deg; Live Action Films<br />\r\n360&deg; Animation Films<br />\r\n360&deg; Virtual Tours<br />\r\n360&deg; Photography<br />\r\nSetting up VR Experiential Zones<br />\r\nPersonalized VR Mobile Application<br />\r\nVR Technical Consultation</p>\r\n\r\n<p>A young and enthusiastic team of film-makers will make sure there is something new on offer just about every time you work with them. Through the symbiosis of artistic and technological innovation, the studio is reinventing storytelling through the creation of visceral and intimate experiences that<br />\r\nprovide an unprecedented sense of presence, awareness, and emotional engagement.</p>','2020-02-10 05:21:55','2020-02-12 00:48:59',NULL,'Design developer\r\nUI/UX developer\r\nQA',NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,1),
(27,'Electra','electra','Electrotherm is a renowned name globally for Engineering & Projects, Steel & Pipes, Power Transmission, Transformers, Electric Vehicles like YObykes & Renewable Energy.','<p>Electrotherm Solar Limited which is a group company under Electrotherm (India) Limited. Founded in 1983, Electrotherm is a well-known INR. 2500 Cr global major in Metal &amp; Heavy Engineering Industry.</p>\r\n\r\n<p>Today Electrotherm is a renowned name globally for Engineering &amp; Projects, Steel &amp; Pipes, Power Transmission, Transformers, Electric Vehicles like YObykes &amp; Renewable Energy. Electrotherm is globally present in more than 35 countries in the Middle East, Africa, SAARC and Europe.</p>\r\n\r\n<p>Electrotherm Solar Limited is MNRE approved and awarded manufacturer. Offered technologies include Evacuated Tube Collector (ETC), Flat Plate Collector (FPC), Heat Pipe, Heat Pump, Air Dryer etc.</p>\r\n\r\n<p>In last few years, they have installed more than 50 Lakh LPD Solar Water Heating Systems across India. &lsquo;EleCtra&rsquo; brand SWHS is performing to its best from sub-zero climatic condition like in Leh-Ladakh to the hottest zone of India like Rajasthan &amp; Gujarat.</p>','2020-02-10 05:23:21','2020-02-12 00:49:00',NULL,'Design developer\r\nUI/UX developer\r\nQA',NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,1),
(28,'Reverb','reverb','Reverb is the World\'s Largest Marketplace for Buying & Selling Music Gear','<p><a href=\"http://reverb.com/\" target=\"_blank\">Reverb.com</a>&nbsp;is the online marketplace for musicians to buy, sell and learn about new, used, vintage and handmade music gear. When Reverb launched in 2013, it was founded on the principle that buying and selling musical instruments should be easy and affordable. Since then they&rsquo;ve become a thriving marketplace that connects millions of people around the world to the gear and the inspiration needed to make music.</p>\r\n\r\n<p>The Reverb Marketplace is made up of hundreds of thousands of buyers and sellers &ndash; from beginner musicians to collectors, mom-and-pop shops to large retailers, and popular manufacturers to boutique builders and luthiers. You might even run into some of your favorite rock stars buying and selling on Reverb!</p>\r\n\r\n<p>Entrepreneur David Kalt launched&nbsp;<a href=\"http://reverb.com/\" target=\"_blank\">Reverb.com</a>&nbsp;in 2013 in response to the frustration he felt while trying to buy and sell guitars online. Traditional marketplace platforms proved to be expensive and generally provided a poor user experience since they&#39;re not tailored for musicians. The idea was simple: An online community created and run by musicians were buying and selling music gear is easy and affordable.</p>','2020-02-10 05:24:35','2020-02-12 00:49:04',NULL,'Project Manager\r\nDesign developer\r\nUI/UX developer\r\nROR developer\r\nQA',NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,1),
(29,'Red Cappi','red-cappi',' Eltiempo.es is the leading digital support of meteorological information in Spain','<p>RedCappi was established in 2012 with the idea that growing a business through email should be easy&mdash;so easy a baby could do it. That&rsquo;s why they set out to create email marketing software that didn&rsquo;t just help businesses crawl toward results, it helped them take giant baby leaps toward success.</p>\r\n\r\n<p>It serves over 10,000 customers worldwide and has sent over 1 billion emails. There innovative features and user-friendly interface have helped companies big and small, new and established, feel empowered to take their email marketing to the next level. Whether you are hoping to increase sales or keep subscribers informed, RedCappi makes it easy to grow your business.</p>\r\n\r\n<p>RedCappi believes in giving back to those in need. That&rsquo;s why they are supporting two amazing charities who help struggling families find shelter, food and hope when they need it most. You too can support Holy Family Shelter and Feed My Starving Children and they&rsquo;ll match your donation dollar-for-dollar up to $100. Just email at&nbsp;<a href=\"mailto:support@redcappi.com\">support@redcappi.com</a>&nbsp;letting them know your name &amp; how much you donated, and they&rsquo;ll match your contributions.</p>','2020-02-11 23:01:39','2020-02-11 23:01:39',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(30,'John Hopkins University','john-hopkins-university','John Hopkins is the America’s first research university, a place that has revolutionized higher education in the U.S. and continues to bring knowledge and discoveries to the world.','<p>John Hopkins University is America&rsquo;s first research university, a place that has revolutionized higher education in the U.S. and continues to bring knowledge and discoveries to the world.</p>\r\n\r\n<p>The University&rsquo;s first president, Daniel Coit Gilman believed that teaching and research go hand in hand&mdash;that success in one depends on success in the other&mdash;and that a modern university must do both well. He also believed that sharing our knowledge and discoveries would help make the world a better place.</p>\r\n\r\n<p>In 140 years, we haven&rsquo;t strayed from that vision. This is still a destination for excellent, ambitious scholars and a world leader in teaching and research. Distinguished professors mentor students in the arts and music, humanities, social and natural sciences, engineering, international studies, education, business, and the health professions. Those same faculty members, along with their colleagues at the university&rsquo;s Applied Physics Laboratory, have made us the nation&rsquo;s leader in federal research and development funding every year since 1979.</p>','2020-02-11 23:10:01','2020-02-11 23:10:01',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(31,'Sous Chef','sous-chef','Sous Chef is the shop for people who love cooking, inspired by leading restaurants and international food.','<p>Sous Chef is the shop for people who love cooking! They are the place-to-be online for cooks and chefs &ndash; with ingredients, equipment, tableware, and gifts inspired by leading restaurants and international food.</p>\r\n\r\n<p>Based in north London with a warehouse full of fabulous things, ready for next day delivery. Sous Chef ships to thousands of customers each month, including some of the top Michelin-starred restaurants across Europe. Sous Chef&rsquo;s innovative product range has been widely featured in the press, including the Observer Food Monthly, Vogue, The Independent, Sunday Telegraph, Evening Standard, Radio Times, Stylist, Shortlist, BBC Good Food, Delicious, Olive, Good Housekeeping and Homes &amp; Gardens magazines.</p>\r\n\r\n<p>Nicola Lando started Sous Chef together with her husband Nick in 2012. Working in a Michelin-starred kitchen, she had been cooking with amazing ingredients and wanted to make them available more widely. So Sous Chef was born. And today they are still an independent family business. Every month they add hundreds of new products, inspired by leading restaurant menus, chefs we speak to, and our travels to producers around the world.</p>\r\n\r\n<p>Sous Chef is all about Flavor, Discovery, Provenance, Excitement, Knowledge<br />\r\nand Service.</p>','2020-02-11 23:35:32','2020-02-11 23:35:32',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(32,'Wave','wave','Wave - Find your friends: view each others\' live location on a private map!','<p>Wave - Find your friends: view each others&#39; live location on a private map!</p>\r\n\r\n<p>Wave makes it fun &amp; safe for you to find your friends and get found by them. With the most modern geo-technology, you can stay connected by sharing live locations, always on a private map. Wave has millions of happy users all over the world.</p>\r\n\r\n<p>With Wave you can:</p>\r\n\r\n<ul>\r\n	<li>View the real-time location of your contacts upon a request on a private map.</li>\r\n	<li>Create groups with your friends, colleagues or family members to view their real-time location and chat with them for free.</li>\r\n	<li>Choose the duration or start an unlimited session with the ones that matter the most.</li>\r\n	<li>View the estimated time of arrival to your contacts.</li>\r\n	<li>Set editable meeting points for you and your friends on the map.</li>\r\n	<li>Navigate to the meeting point without changing to a different app and call an Uber if your destination is too far.</li>\r\n	<li>So much more!</li>\r\n</ul>','2020-02-11 23:37:37','2020-02-11 23:37:37',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(33,'Hotel Tonight','hotel-tonight','Booking amazing deals at Great Hotels','<p>HotelTonight: Book amazing deals at great hotels</p>\r\n\r\n<p>Amazing hotel deals for tonight, tomorrow and beyond! Hotels give us discounts on their empty rooms. You get the best rates and deals, whether last minute or in advance. HotelTonight makes it incredibly easy to find and reserve a sweet deal at a great hotel. Three taps, one swipe, you&rsquo;re booked!</p>\r\n\r\n<p>&bull; From top-rated luxury hotels to tried-and-true favorite rooms to cool, revamped former motels, we work with great hotels across the globe to get you the best deals (and the only partner with hotels where we&rsquo;d wanna stay, too)<br />\r\n&bull; Book a room for tonight, tomorrow, next week, next month and beyond &ndash; up to 100 days in advance in our most popular locations<br />\r\n&bull; Great for spontaneous vacations or planning a trip in advance. We&rsquo;ve got the hookup for discounted last minute hotel bookings wherever you want to be around the world: North America, Europe, Australia and more!<br />\r\n&bull; Search by city, attraction or map location<br />\r\n&bull; See ratings, reviews, and photos from fellow bookers<br />\r\n&bull; Score extra Geo Rate savings off our already-discounted rates, based on your current location (find these deals marked in green in the app)<br />\r\n&bull; HT Perks program - the more you book, the better our deals get! Level up to score even bigger discounts<br />\r\n&bull; Hotel descriptions that boil down the top 3 reasons why we like the hotels we work with &ndash; and why you will, too<br />\r\n&bull; Simple categories (like Basic, Hip, and Luxe) to make it easy to find the perfect hotel, inn, bed and breakfast, motel, resort or another lodging for you<br />\r\n&bull; 24/7 customer support (from real, live, nice people) for every booking<br />\r\n&bull; Access to HT Pros, our in-app concierge (a real-live person at the ready to make your stay great, from grabbing you an extra toothbrush to making a dinner reservation at a hot restaurant or bar near your hotel)<br />\r\n&bull; Supports paying with Google Pay</p>','2020-02-11 23:38:36','2020-02-11 23:38:37',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(34,'Wola Schools','wola-schools','Wola - Bus Routes Live Locations','<p>Wola Schools - Track the school bus</p>\r\n\r\n<p>Track the school bus in real time and anticipate when your children arrive from school in the simplest way. Wola Schools uses the location in real time to connect parents with their children and plan better the pick-up time.</p>\r\n\r\n<p>On Wola Schools, parents set notifications that alert them when the bus is arriving at their nearest bus stop. Its system tracks the school bus in real time and lets parents know if the bus is late, if it has stopped along the route or if it has not left the school yet.</p>\r\n\r\n<p>HOW DOES WOLA SCHOOLS TRACKS THE SCHOOL BUS?</p>\r\n\r\n<p>Wola Schools is an app that both parents and the monitor that goes with the children should have. It is activated when the monitor chooses the option to leave the school from his/her smartphone. From that moment, parents receive a notification and can access the map that tracks the school bus in real time.</p>\r\n\r\n<p>This is a novelty way to connect with children in the school and feel safe and sound about their location, without children having to acquire a smartphone to communicate with their families.</p>','2020-02-11 23:39:06','2020-02-11 23:39:06',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(35,'Vivino','vivino','Vivino is the world’s most popular wine community and most downloaded mobile wine app.','<p>With 30 million users, Vivino is the world&rsquo;s largest wine community and the ultimate destination for discovering and buying wines.</p>\r\n\r\n<p>Vivino is the ultimate online destination for all your wine buying needs - the largest wine selection, personalized wine recommendations, free shipping options and honest community ratings and review.</p>\r\n\r\n<p>Vivino Premium: Paying for wine shipping can be a hassle so we offer an annual subscription service that provides free shipping on all your purchases. Try free for 30 days and never pay to ship on wine again!</p>\r\n\r\n<p>Crowdsourced Ratings &amp; Reviews: Leverage reviews and ratings from the world&rsquo;s largest community of wine drinkers to always select the best wine.</p>\r\n\r\n<p>Restaurant Wine List Scanner: Always select the best wine from any restaurant wine list menu.</p>\r\n\r\n<p>Quick Compare: Pick the best wine right at the shelf by scanning multiple wine labels to instantly view ratings, region, price and food pairings all on one page.</p>\r\n\r\n<p>Taste Profile: Track and organize your scanned and rated wines to determine your personal taste profile, discover new wines and see how you rank against friends and the Vivino community.</p>','2020-02-11 23:39:29','2020-02-11 23:39:29',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(36,'Fabago','fabago','Fabogo is online beauty and wellness aggregator aimed at creating a great experience for its users to discover spas, fitness centers, cosmetic clinics and salons','<p>Fabogo is all about looking good. Discover the trendiest salons, the most luxurious spas, state-of-the-art gymnasiums or even the local barbershop in your neighborhood. You can buy curated beauty packages from hundreds of venues in your city, book an appointment for the services in your package and redeem them at the venue.</p>\r\n\r\n<p>DISCOVER SALONS &amp; SPAS<br />\r\nYou can discover salons, spas, gyms and wellness centers in and around your locality, find salon &amp; spa offers, check user ratings &amp; reviews, see the menu and photos of their interior.</p>\r\n\r\n<p>BUY PACKAGES<br />\r\nChoose from a wide range of beauty and wellness packages specially tailored by keeping your preference in mind. If you are looking to economise on your monthly beauty spend, these packages are surely going to woo you with fabulous discounted rates on bulk services. You can even pick a single salon/spa service &amp; proceed for a hassle-free appointment. These combinations are made in heaven, perfect for all your needs!</p>\r\n\r\n<p>BOOK AN APPOINTMENT<br />\r\nNow that you have purchased your package, you have the freedom to book an appointment for the services included, as and when you want to avail them. You do not need to avail all services at the same time. Booking an appointment on Fabogo is quick, easy and hassle-free. No need to call the venue multiple times and struggle to get an appointment or get mad remembering all your different beauty appointments.</p>\r\n\r\n<p>REDEEM SERVICES<br />\r\nGo to the venue at the time of your appointment. Avail the service, pamper yourself, leave no stone unturned in getting that snazzy look or just being zen post the long pain-relieving body massage. Once you&rsquo;re happy, just share the Purchase ID with the venue, and that&rsquo;s it!</p>\r\n\r\n<p>EARN FABOGO CREDITS<br />\r\nSurprise! Surprise! For our loyal Fabianas &amp; Fabians (did we go just overboard?), you can earn cashback on all your purchases.</p>\r\n\r\n<p>Key Features<br />\r\nDiscover salons and spas in your neighborhood<br />\r\nView menu, photos, address, directions, phone number, highlights and all other information<br />\r\nBuy beauty &amp; wellness packages<br />\r\nBook online appointment<br />\r\nRate &amp; review the venues<br />\r\nEarn fabogo credits<br />\r\nGet reminders for your appointments</p>\r\n\r\n<p>Fabogo is currently available in Dubai, Mumbai and Pune. We will be expanding to other cities soon!</p>','2020-02-11 23:40:00','2020-02-11 23:40:00',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(37,'Go Wall Paper','go-wall-paper','Go Wallpaper is with more than 21 yers of expeirnece in decorating market.','<p>Customer service is key to successful long terms business, Go Wallpaper prides itself on this; offering expert and friendly advice, in-depth product knowledge and fantastic after sales service.</p>\r\n\r\n<p>They are here from start to finish always on hand to answer any questions even on Weekends.</p>\r\n\r\n<p>With over 21 years&rsquo; experience in the decorating market, they have developed close working relationships with their suppliers, enabling them to offer the best quality products at affordable prices every time.</p>\r\n\r\n<p>What you can Buy&nbsp;@GoWallpaper&nbsp;UK??</p>\r\n\r\n<ul>\r\n	<li>Decorating &amp; DIY products</li>\r\n	<li>A Fantastic Range of Brand Wallpapers and Beautiful Designs for Everyone</li>\r\n	<li>Find Out About Wallrock Thermal Liner</li>\r\n	<li>Lining Paper Grades</li>\r\n	<li>Sound &amp; Damp Proofing Wall Paper</li>\r\n	<li>Eco Wallpaper and Much More</li>\r\n	<li>Our Paints include the Full Range of Little Greene Paint</li>\r\n</ul>','2020-02-11 23:40:21','2020-02-11 23:40:22',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(38,'Gigable LLC','gigable-llc','Gigable is a music discovery platform dedicated to live music.','<p>Gigable is a music discovery platform dedicated to live music. They are driven to build communities with dynamic music scenes.</p>\r\n\r\n<p>With the insurgence of emerging artists around the world and the cluttered landscape of current digital music platforms, Gigable helps music fans find their next favorite band and see them in concert.</p>\r\n\r\n<p>How they do it?</p>\r\n\r\n<p>Their curators organize the best and brightest performers in playlist fashion. Music fans can easily find amazing artists and purchase concert tickets. Gigable is an uncluttered service that is simple to use and enables concert-goers to find the best shows near them.</p>\r\n\r\n<p>Features:</p>\r\n\r\n<p>Discover the best emerging artists touring near you.</p>\r\n\r\n<p>Simply pick a playlist and start listening to amazing new music.</p>\r\n\r\n<p>The &quot;On-Tour&quot; playlist automatically builds a playlist of artists touring in your area. Get VIP access and tickets to concerts.</p>\r\n\r\n<p>Save shows in a convenient list and get reminded of ticket availability.</p>\r\n\r\n<p>Favor songs and create your own playlist with unlimited streaming!</p>\r\n\r\n<p>No ads and unlimited skips.</p>','2020-02-11 23:40:47','2020-02-11 23:40:47',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(39,'Flipp','flipp','Flipp is the essential app for your weekly shopping.','<p>Flipp is the essential app for your weekly shopping. Search flyer deals and coupons by item, brand, or category to quickly find the best deals on your weekly essentials and make saving money super easy. Flipp is the only App that matches local flyer deals with coupons from the brands you love to bring you the most savings. To keep you on track of your weekly shopping, Flipp&rsquo;s ultimate shopping list will help you plan ahead by finding what&rsquo;s on sale and stay organized in-store.</p>\r\n\r\n<p>For American users simply add loyalty cards from your favorite stores on to Flipp and then clip coupon deals to your card for instant savings at checkout.</p>\r\n\r\n<p>Flipp brings you the latest flyers from your favorite stores including Walmart, Target, Dollar General, Walgreens, The Home Depot, Macy&rsquo;s, Dick&rsquo;s Sporting Goods, CVS, JCPenney, Best Buy, Lowe&rsquo;s and over 800 more retailers. You can save 20-50% on the items you need every week.</p>\r\n\r\n<p>Features include:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Quickly browse through your favorite local flyers including Dollar General, BI-LO, Winn-Dixie, London Drugs, Co-op and more.</p>\r\n	</li>\r\n	<li>\r\n	<p>Match hundreds of coupons with flyers from the brands you love for the most savings.</p>\r\n	</li>\r\n	<li>\r\n	<p>Plan ahead and discover deals for each item in your Shopping List.</p>\r\n	</li>\r\n	<li>\r\n	<p>Search through hundreds of items, retailers, and brands.</p>\r\n	</li>\r\n	<li>\r\n	<p>Clip items to organize deals to make your shopping trip easy.</p>\r\n	</li>\r\n	<li>\r\n	<p>Discover deals and ultimate savings with the Discount Slider and save up to 50% off.</p>\r\n	</li>\r\n	<li>\r\n	<p>Receive reminders about expiring deals, new offers, and updates from your favorite and nearby retailers to stay on top of your deals.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Here&rsquo;s what the experts are saying about Flipp:</p>\r\n\r\n<p>Good Housekeeping: Apps We&rsquo;re Loving -- &ldquo;I know that moms are time-pressed and busy, and apps need to be seamless, intuitive, and helpful for them to take notice. Flipp is all of these things&rdquo;</p>\r\n\r\n<p>Flipp has been named a top shopping app to help save money in over 350 publications including TechCrunch, ABC News, Forbes, USA Today, The Washington Post, The Boston Globe, The Marilyn Denis Show, CP24, The Globe and Mail, and Metro News. Top Mom Bloggers are calling Flipp their &ldquo;shopping buddy&rdquo;, the &ldquo;go-to app for saving money&rdquo;, their &ldquo;best friend&rdquo; and a &ldquo;life saver&rdquo;.</p>','2020-02-11 23:41:18','2020-02-11 23:41:18',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(40,'Dr. Greger\'s Daily Dozen','dr-gregers-daily-dozen','Dr. Greger’s Daily Dozen details the healthiest foods and how many servings of each we should try to check off every day. He explains his rationale in his book How Not to Die.','<p>Dr. Greger&#39;s Daily Dozenhelps you to Keep track of the foods recommended by Dr. Greger in his New York Times Bestselling book, How Not to Die.</p>\r\n\r\n<p>In the years of research required to create the more than a thousand evidence-based videos on his website&nbsp;<a href=\"http://nutritionfacts.org/\" target=\"_blank\">NutritionFacts.org</a>, Michael Greger, M.D, FACLM, has arrived at a list of what he considers the most important foods to include in a healthy daily diet.</p>\r\n\r\n<p>Dr. Greger&rsquo;s Daily Dozen details the healthiest foods and how many servings of each we should try to check off every day. He explains his rationale in his book How Not to Die.</p>\r\n\r\n<p>All his proceeds from his books, DVDs, and speaking engagements are all donated to charity.</p>\r\n\r\n<p><a href=\"http://nutritionfacts.org/\" target=\"_blank\">NutritionFacts.org</a>&nbsp;is a non-commercial, non-profit, science-based public service provided by Dr. Greger, providing free daily updates on the latest in nutrition research via bite-sized videos. He has nearly a thousand videos on every aspect of healthy eating, with new videos and articles uploaded every day.</p>','2020-02-11 23:41:55','2020-02-11 23:41:55',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(41,'Day One','day-one','Day One is a journaling app that records life as you live it. From once-in-a-lifetime events to everyday moments.','<p>From once-in-a-lifetime events to everyday moments, Day One&rsquo;s elegant interface makes journaling about your life a simple pleasure.</p>\r\n\r\n<p>Award Winning: App of the Year and Apple Design Award</p>\r\n\r\n<p>CREATE HABITS: Make journaling a part of your life:<br />\r\n&bull; Reminders<br />\r\n&bull; In-app and System Notifications<br />\r\n&bull; Calendar</p>\r\n\r\n<p>LIFE-ENRICHING BENEFITS: Reap the rewards of your consistency:<br />\r\n&bull; This Day flashbacks and Nearby entries<br />\r\n&bull; On-the-spot memory enhancement<br />\r\n&bull; Book printing</p>\r\n\r\n<p>PRESERVE EVERY MOMENT: There&rsquo;s no limit to the memories you can save in Day One:<br />\r\n&bull; Unlimited photo storage<br />\r\n&bull; Unlimited journals<br />\r\n&bull; Social media with Day One&#39;s Activity Feed and IFTTT integration</p>\r\n\r\n<p>SECURE YOUR MEMORIES: Day One Sync provides peace of mind:<br />\r\n&bull; End-to-End Encryption<br />\r\n&bull; Sync across all your devices (Premium only)<br />\r\n&bull; Passcode and Touch ID lock</p>\r\n\r\n<p>FOCUSED WRITING EXPERIENCE: Day One&rsquo;s clean, the distraction-free interface makes it easy to write down what&rsquo;s important:<br />\r\n&bull; Powerful text formatting with Markdown<br />\r\n&bull; Templates<br />\r\n&bull; Cross-platform support (mobile, desktop, and watch)</p>\r\n\r\n<p>RECALL EVERY DETAIL: Metadata automatically records the finer points of life:<br />\r\n&bull; Location &amp; historical visits<br />\r\n&bull; Time and date<br />\r\n&bull; Temperature and weather<br />\r\n&bull; Activity&mdash;motion and step count<br />\r\n&bull; Music playing</p>\r\n\r\n<p>ORGANIZE YOUR ENTRIES: Finding any memory is fast and easy:<br />\r\n&bull; Powerful search<br />\r\n&bull; Multiple journals (Premium only)<br />\r\n&bull; Tags<br />\r\n&bull; Favorite entries<br />\r\n&bull; Browse by calendar, timeline, maps, or photos</p>\r\n\r\n<p>SHARE YOUR MEMORIES: Our export options make it easy for you share your journals:<br />\r\n&bull; Export to PDF, HTML, JSON<br />\r\n&bull; Print your journals with Day One Book</p>\r\n\r\n<p>&ldquo;This superb journaling app remains pleasant to behold, easy to use, and a tough act for any rival to follow.&rdquo;<br />\r\n&mdash;Macworld</p>\r\n\r\n<p>&ldquo;If you&rsquo;re looking for a fantastic journaling app or a great app for logging and recording various events and milestones of your life, then by far and away the best pick is Day One.&rdquo;<br />\r\n&mdash;The Sweet Setup</p>','2020-02-11 23:42:24','2020-02-11 23:42:24',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(42,'Fax Pro','fax-pro','Scanner & Fax Pro is the mobile scanner app for documents. Create free, high-quality PDF or JPG scans with just one tap. Send files via email or as a fax.','<p>Scanner &amp; Fax Pro is the mobile scanner app for documents. Create free, high-quality PDF or JPG scans with just one tap. Send files via email or as a fax.</p>\r\n\r\n<p>Advanced PDF Scanner app:</p>\r\n\r\n<ul>\r\n	<li>Scan to high-quality PDF or JPEG.</li>\r\n	<li>Save scans in grayscale, black and white or color</li>\r\n	<li>Automatic shutter and border detection for any scannable object</li>\r\n	<li>Advanced pic processing with enhancement and color correction, noise removing, automatic perspective correction and more</li>\r\n	<li>Choose between low, medium and HD scan quality</li>\r\n	<li>Multipage scanning - scan as many pages as you like</li>\r\n	<li>Batch processing mode</li>\r\n</ul>\r\n\r\n<p>Moreover, the text recognition function (OCR) allows to copy and send portions of the text from PDF documents.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Fast and secure scanning +<br />\r\n	Paper documents of any importance, bills, checks, agreements, invoices, inscriptions on tablets - Scanner Pro will help save them all! All files can be saved in PDF or JPEG formats for further work.</p>\r\n\r\n	<p>&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>Text recognition technology (OCR) +<br />\r\n	Any scan can become a text, the parts of which can be selected and copied. Scanner &amp; Fax App supports 21 recognition languages: English, German, Italian, French, Spanish, Russian, Portuguese, Dutch, Turkish, Polish, Swedish, Norwegian, Japanese, Simplified Chinese, Traditional Chinese, Czech, Danish, Greek, Croatian, Estonian and Ukrainian.</p>\r\n\r\n	<p>&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>Share information +<br />\r\n	Once a scan is made, click the &quot;Share&quot; button to send it by mail, export to the Photos folder or to another app of your choice. Click &quot;Print&quot;, and the paper version is already printing, or &quot;Fax&quot; to send the scan by fax.</p>\r\n\r\n	<p>&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>PROTECT your files using Touch ID or password.<br />\r\n	Add a password to make sure only those you want to can open your document.</p>\r\n\r\n	<p>&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>SIGN documents right on the screen of your device! Create and save one or more signatures and add them to scans at any time.</p>\r\n	</li>\r\n	<li>\r\n	<p>FAX any kinds of documents: contracts, agreements, checks, bills, records, schedules, work schedules - all that you need in your daily working life.</p>\r\n	</li>\r\n	<li>\r\n	<p>HIGHLIGHT the key parts of the scanned materials with a marker not to lose sight of the most important.</p>\r\n	</li>\r\n</ul>','2020-02-11 23:43:03','2020-02-11 23:43:03',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(43,'Red Cross','red-cross','The International Red Cross and Red Crescent Movement is an international humanitarian movement with approximately 17 million volunteers, members and staff worldwideto protect human life and health, to ensure respect for all human beings, and to prevent and alleviate human suffering.','<p>Singapore Red Cross is an independent humanitarian organization dedicated to protecting human life and dignity, relieving human suffering and responding to emergencies.</p>\r\n\r\n<p>Singapore Red Cross is a member of the International Federation of the Red Cross and Red Crescent Societies and as such adheres to the Geneva Conventions (1949).</p>\r\n\r\n<p>The work of the Red Cross in Singapore began on 30 September 1949 as a branch of the British Red Cross. On 6 April 1973, it was incorporated as an Act of Parliament and became known as the Singapore Red Cross (SRC).</p>\r\n\r\n<p>The SRC is governed by a 19-member Council headed by a Chairman who is appointed by the President of the Republic of Singapore, the Patron of the SRC. The Council is responsible for pursuing the objective of the SRC as laid down by the Act of Parliament and its Constitution. The Council has four oversight committees providing the relevant advice and expertise; namely the Finance and Investment, Audit, Corporate Governance and Nomination and Human Resource and Compensation Committees.</p>\r\n\r\n<p>The general management of the SRC is overseen by the Management Committee, headed by the Secretary General / Chief Executive Officer (CEO) of the SRC. Implementation of the policies and directives laid down by the Council is done by the Secretariat which is headed by the Secretary General / CEO.</p>\r\n\r\n<p>The Secretariat is organised into three divisions; Operations, Administration and the Red Cross Youth. The strength and commitment of our volunteer corps is critical for the realisation of our vision and to carry out our vision. Volunteers and staff work closely together in planning, organising and implementing the activities and programmes of the SRC.</p>\r\n\r\n<p>In 2013, SRC was awarded the Charity Governance Award which honours charities that have adopted the highest standards of governance and implemented the best practices to ensure sustained effectiveness.</p>\r\n\r\n<p>Consecutively in 2016 and 2017, we received the Charity Transparency Award for being one of the nation&#39;s best governed charities.</p>\r\n\r\n<p>This attests to SRC&#39;s commitment in upholding the highest standards in transparency - a key pillar in governance.</p>','2020-02-11 23:43:31','2020-02-11 23:43:31',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(44,'The Altman Brothers','the-altman-brothers','The Altman Brothers are among the top producing real estate agents in the country. Josh and Matt Altman managed to carve out a niche in the Los Angeles high end market.','<p>The Altman Brothers are among the top producing real estate agents in the country. Josh and Matt Altman managed to carve out a niche in the Los Angeles high end market. This includes staking claim to top tinsel town turf in the Platinum Triangle of Beverly Hills, Bel-Air, and Holmby Hills, and everywhere from Malibu to the Hollywood Hills and Downtown LA. The Altman Brothers are full-service real estate experts who strive to service the entire spectrum of exclusive clients Los Angeles has to offer. Altman clients are luxury buyers and high-end sellers: entertainers, professional athletes and high-net- worth individuals whose real estate holdings dot the globe, to clients with small family homes and short sale needs.</p>','2020-02-11 23:44:10','2020-02-11 23:44:11',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(45,'Phish Train','phish-train','PhishTrain was founded in 2014 to combat the damage phishing attacks have inflicted on businesses worldwide.','<p>PhishTrain was founded in 2014 to combat the damage phishing attacks have inflicted on businesses worldwide. By focusing on identifying and training human vulnerabilities rather than buffing the network&#39;s firewall, PhishTrain helps to create a culture of security within company. Now, you can be reassured that your employees possesses the knowledge to identify and avoid malicious phishing attacks.</p>','2020-02-11 23:44:50','2020-02-11 23:44:50',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(46,'Chewy Pet Ecommerce','chewy-pet-ecommerce','Chewy with a mission to become the largest pet retailer in the world with the most satisfied customers.','<p>Chewy with a mission to become the largest pet retailer in the world with the most satisfied customers. Chevy have everything you need for your pet at amazing prices, every day with more than 1,000 favorite brands, including Blue Buffalo, Nutro, Natural Balance, and Tidy Cats &ndash; all from the comfort of home.</p>','2020-02-11 23:45:16','2020-02-11 23:45:16',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL),
(47,'Restudy','restudy','Restudy is an online learning platform with more than 1,900 learning-rich educational videos, divided into 22 subjects from elementary school to high school level.','<p>Restudy is an online learning platform with more than 1,900 learning-rich educational videos, divided into 22 subjects from elementary school to high school level.</p>','2020-02-11 23:45:51','2020-02-11 23:45:51',NULL,NULL,NULL,'a:2:{s:5:\"title\";N;s:4:\"desc\";N;}',1,NULL);

/*Table structure for table `projects_services` */

DROP TABLE IF EXISTS `projects_services`;

CREATE TABLE `projects_services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=429 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_services` */

insert  into `projects_services`(`id`,`created_at`,`updated_at`,`project_id`,`service_id`,`deleted_at`) values 
(371,NULL,NULL,1,3,NULL),
(372,NULL,NULL,26,3,NULL),
(373,NULL,NULL,27,3,NULL),
(374,NULL,NULL,28,3,NULL),
(375,NULL,NULL,29,3,NULL),
(376,NULL,NULL,30,3,NULL),
(377,NULL,NULL,39,3,NULL),
(378,NULL,NULL,44,3,NULL),
(379,NULL,NULL,45,3,NULL),
(380,NULL,NULL,46,3,NULL),
(381,NULL,NULL,32,2,NULL),
(382,NULL,NULL,33,2,NULL),
(383,NULL,NULL,34,2,NULL),
(384,NULL,NULL,35,2,NULL),
(385,NULL,NULL,36,2,NULL),
(386,NULL,NULL,37,2,NULL),
(387,NULL,NULL,38,2,NULL),
(388,NULL,NULL,39,2,NULL),
(389,NULL,NULL,40,2,NULL),
(390,NULL,NULL,41,2,NULL),
(391,NULL,NULL,42,2,NULL),
(392,NULL,NULL,1,4,NULL),
(393,NULL,NULL,2,4,NULL),
(394,NULL,NULL,26,4,NULL),
(395,NULL,NULL,27,4,NULL),
(396,NULL,NULL,28,4,NULL),
(397,NULL,NULL,29,4,NULL),
(398,NULL,NULL,30,4,NULL),
(399,NULL,NULL,31,4,NULL),
(400,NULL,NULL,32,4,NULL),
(401,NULL,NULL,33,4,NULL),
(402,NULL,NULL,34,4,NULL),
(403,NULL,NULL,35,4,NULL),
(404,NULL,NULL,36,4,NULL),
(405,NULL,NULL,37,4,NULL),
(406,NULL,NULL,38,4,NULL),
(407,NULL,NULL,39,4,NULL),
(408,NULL,NULL,40,4,NULL),
(409,NULL,NULL,41,4,NULL),
(410,NULL,NULL,42,4,NULL),
(411,NULL,NULL,43,4,NULL),
(412,NULL,NULL,44,4,NULL),
(413,NULL,NULL,45,4,NULL),
(414,NULL,NULL,46,4,NULL),
(415,NULL,NULL,47,4,NULL),
(416,NULL,NULL,1,1,NULL),
(417,NULL,NULL,2,1,NULL),
(418,NULL,NULL,26,1,NULL),
(419,NULL,NULL,27,1,NULL),
(420,NULL,NULL,28,1,NULL),
(421,NULL,NULL,29,1,NULL),
(422,NULL,NULL,30,1,NULL),
(423,NULL,NULL,31,1,NULL),
(424,NULL,NULL,43,1,NULL),
(425,NULL,NULL,44,1,NULL),
(426,NULL,NULL,45,1,NULL),
(427,NULL,NULL,46,1,NULL),
(428,NULL,NULL,47,1,NULL);

/*Table structure for table `projects_tech` */

DROP TABLE IF EXISTS `projects_tech`;

CREATE TABLE `projects_tech` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `tech_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=413 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_tech` */

insert  into `projects_tech`(`id`,`created_at`,`updated_at`,`project_id`,`tech_id`,`deleted_at`) values 
(255,NULL,NULL,30,9,NULL),
(256,NULL,NULL,30,10,NULL),
(257,NULL,NULL,30,11,NULL),
(258,NULL,NULL,30,12,NULL),
(260,NULL,NULL,31,9,NULL),
(261,NULL,NULL,31,10,NULL),
(262,NULL,NULL,31,11,NULL),
(263,NULL,NULL,31,12,NULL),
(298,NULL,NULL,29,13,NULL),
(299,NULL,NULL,29,12,NULL),
(300,NULL,NULL,29,9,NULL),
(301,NULL,NULL,29,11,NULL),
(302,NULL,NULL,29,10,NULL),
(303,NULL,NULL,29,109,NULL),
(367,NULL,NULL,26,105,NULL),
(368,NULL,NULL,26,12,NULL),
(369,NULL,NULL,26,9,NULL),
(370,NULL,NULL,26,11,NULL),
(371,NULL,NULL,26,10,NULL),
(373,NULL,NULL,27,105,NULL),
(374,NULL,NULL,27,12,NULL),
(375,NULL,NULL,27,9,NULL),
(376,NULL,NULL,27,11,NULL),
(377,NULL,NULL,27,10,NULL),
(379,NULL,NULL,28,105,NULL),
(380,NULL,NULL,28,110,NULL),
(381,NULL,NULL,28,12,NULL),
(382,NULL,NULL,28,9,NULL),
(383,NULL,NULL,28,11,NULL),
(384,NULL,NULL,28,10,NULL),
(385,NULL,NULL,28,109,NULL),
(393,NULL,NULL,2,12,NULL),
(394,NULL,NULL,2,9,NULL),
(395,NULL,NULL,2,11,NULL),
(396,NULL,NULL,2,10,NULL),
(397,NULL,NULL,2,14,NULL),
(399,NULL,NULL,1,105,NULL),
(400,NULL,NULL,1,12,NULL),
(401,NULL,NULL,1,104,NULL),
(402,NULL,NULL,1,9,NULL),
(403,NULL,NULL,1,11,NULL),
(404,NULL,NULL,1,10,NULL),
(405,NULL,NULL,1,3,NULL),
(406,NULL,NULL,2,3,NULL),
(407,NULL,NULL,26,3,NULL),
(408,NULL,NULL,27,3,NULL),
(409,NULL,NULL,29,3,NULL),
(410,NULL,NULL,30,3,NULL),
(411,NULL,NULL,31,3,NULL),
(412,NULL,NULL,28,6,NULL);

/*Table structure for table `projects_testimonials` */

DROP TABLE IF EXISTS `projects_testimonials`;

CREATE TABLE `projects_testimonials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `testimonial_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_testimonials` */

insert  into `projects_testimonials`(`id`,`created_at`,`updated_at`,`project_id`,`testimonial_id`,`deleted_at`) values 
(133,NULL,NULL,26,4,NULL),
(134,NULL,NULL,28,4,NULL),
(136,NULL,NULL,2,4,NULL),
(137,NULL,NULL,1,4,NULL);

/*Table structure for table `refe_file` */

DROP TABLE IF EXISTS `refe_file`;

CREATE TABLE `refe_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `refe_table_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `refe_field_id` int(11) NOT NULL DEFAULT '0',
  `refe_file_path` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refe_file_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_real_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `refe_file` */

insert  into `refe_file`(`id`,`refe_table_name`,`refe_field_id`,`refe_file_path`,`refe_file_name`,`file_real_name`,`file_code`,`file_type`,`priority`,`created_at`,`updated_at`) values 
(5,'projects',3,'uploads/projects/3','5e0206bccdd81_3.png','image-slide.png',NULL,'projects_single_image',0,'2019-12-24 01:38:21','2019-12-24 01:38:21'),
(6,'servicedetails',1,'uploads/servicedetails/1','5e031d3e1a442_1.jpg','service1.jpg',NULL,'servicedetails_single_image',0,'2019-12-24 21:26:38','2019-12-24 21:26:38'),
(7,'servicedetails',2,'uploads/servicedetails/2','5e031d75404d3_2.jpg','service1.jpg',NULL,'servicedetails_single_image',0,'2019-12-24 21:27:33','2019-12-24 21:27:33'),
(8,'servicedetails',3,'uploads/servicedetails/3','5e031db2c39bf_3.jpg','service1.jpg',NULL,'servicedetails_single_image',0,'2019-12-24 21:28:34','2019-12-24 21:28:34'),
(11,'team',1,'uploads/team/1','5e04a0e5e7947_1.png','download (1).png',NULL,'teams_single_image',0,'2019-12-26 01:00:38','2019-12-26 01:00:38'),
(12,'partners',1,'uploads/partners/1','5e05e991781f6_1.svg','amazonpay.svg',NULL,'partners_single_image',0,'2019-12-27 05:52:57','2019-12-27 05:52:57'),
(13,'partners',2,'uploads/partners/2','5e05ea7b5e1c0_2.svg','visa.svg',NULL,'partners_single_image',0,'2019-12-27 05:56:51','2019-12-27 05:56:51'),
(14,'partners',3,'uploads/partners/3','5e05ea9d259da_3.svg','paypal.svg',NULL,'partners_single_image',0,'2019-12-27 05:57:25','2019-12-27 05:57:25'),
(15,'partners',4,'uploads/partners/4','5e05eac41eac7_4.svg','applepay.svg',NULL,'partners_single_image',0,'2019-12-27 05:58:04','2019-12-27 05:58:04'),
(16,'partners',5,'uploads/partners/5','5e05eaf946892_5.svg','transferwise.svg',NULL,'partners_single_image',0,'2019-12-27 05:58:57','2019-12-27 05:58:57'),
(17,'partners',6,'uploads/partners/6','5e05eb257f909_6.svg','payoneer.svg',NULL,'partners_single_image',0,'2019-12-27 05:59:41','2019-12-27 05:59:41'),
(18,'partners',7,'uploads/partners/7','5e05eb4ddb64f_7.svg','skrill.svg',NULL,'partners_single_image',0,'2019-12-27 06:00:21','2019-12-27 06:00:21'),
(19,'partners',8,'uploads/partners/8','5e05eb7c75559_8.svg','stripe.svg',NULL,'partners_single_image',0,'2019-12-27 06:01:08','2019-12-27 06:01:08'),
(22,'projects',1,'uploads/projects/1','5e10283c80932_1.jpg','vivino.jpg',NULL,'projects_single_image',0,'2020-01-04 00:23:00','2020-01-04 00:23:00'),
(23,'projects',2,'uploads/projects/2','5e10298fcf0cb_2.jpg','coffee.jpg',NULL,'projects_single_image',0,'2020-01-04 00:28:40','2020-01-04 00:28:40'),
(24,'projects',4,'uploads/projects/4','5e1427bac94c3_4.png','Screenshot_1.png',NULL,'projects_single_image',0,'2020-01-07 01:09:55','2020-01-07 01:09:55'),
(25,'projects',5,'uploads/projects/5','5e142838583c5_5.png','Screenshot_2.png',NULL,'projects_single_image',0,'2020-01-07 01:12:00','2020-01-07 01:12:00'),
(28,'projects',1,'uploads/projects/1','5e2698b816de1_1.png','56A5C62C.png',NULL,'projects_projects_single_main',0,'2020-01-21 00:52:49','2020-01-21 00:52:49'),
(30,'projects',1,'uploads/projects/1','5e269fdbef1d9_1.png','56A5C62C.png',NULL,'projects_single_cart',0,'2020-01-21 01:23:17','2020-01-21 01:23:17'),
(31,'projects',1,'uploads/projects/1','5e26a0107b92d_1.png','5AE7696A.png',NULL,'projects_multiple_color_images',0,'2020-01-21 01:24:08','2020-01-21 01:24:08'),
(32,'projects',1,'uploads/projects/1','5e26a010a5c42_1.png','5AE76962.png',NULL,'projects_multiple_color_images',0,'2020-01-21 01:24:08','2020-01-21 01:24:08'),
(33,'projects',1,'uploads/projects/1','5e26a010ce452_1.png','5AE76964.png',NULL,'projects_multiple_color_images',0,'2020-01-21 01:24:08','2020-01-21 01:24:08'),
(35,'projects',1,'uploads/projects/1','5e26a052a1f75_1.png','56A5C62C.png',NULL,'projects_multiple_color_images',0,'2020-01-21 01:25:16','2020-01-21 01:25:16'),
(36,'projects',1,'uploads/projects/1','5e26a0540d2a5_1.png','B38015AD.png',NULL,'projects_multiple_color_images',0,'2020-01-21 01:25:17','2020-01-21 01:25:17'),
(37,'projects',1,'uploads/projects/1','5e26a0558f4c4_1.png','B38015AE.png',NULL,'projects_multiple_color_images',0,'2020-01-21 01:25:19','2020-01-21 01:25:19'),
(38,'projects',1,'uploads/projects/1','5e26a0571df02_1.png','56A5C62D.png',NULL,'projects_multiple_size_images',0,'2020-01-21 01:25:19','2020-01-21 01:25:19'),
(39,'projects',1,'uploads/projects/1','5e26a057ddfc2_1.png','B38015A9.png',NULL,'projects_multiple_size_images',0,'2020-01-21 01:25:21','2020-01-21 01:25:21'),
(40,'projects',1,'uploads/projects/1','5e26a0590eb85_1.png','B38015AF.png',NULL,'projects_multiple_size_images',0,'2020-01-21 01:25:21','2020-01-21 01:25:21'),
(49,'projects',1,'uploads/projects/1','5e29a07570f69_1.png','project-2.png',NULL,'projects_single_main',0,'2020-01-23 08:02:37','2020-01-23 08:02:37'),
(50,'projects',1,'uploads/projects/1','5e29a075cd9d3_1.png','project-2.png',NULL,'projects_single_cover',0,'2020-01-23 08:02:38','2020-01-23 08:02:38'),
(51,'projects',1,'uploads/projects/1','5e29a0762a0cf_1.png','LFK.png',NULL,'projects_single_scroll',0,'2020-01-23 08:02:42','2020-01-23 08:02:42'),
(52,'projects',1,'uploads/projects/1','5e29a07a55a13_1.png','project.png',NULL,'projects_multiple_images',0,'2020-01-23 08:02:42','2020-01-23 08:02:42'),
(53,'projects',1,'uploads/projects/1','5e29a07aa65a5_1.png','project-2.png',NULL,'projects_multiple_images',0,'2020-01-23 08:02:42','2020-01-23 08:02:42'),
(54,'projects',1,'uploads/projects/1','5e29a07aed0d3_1.png','project-3.png',NULL,'projects_multiple_images',0,'2020-01-23 08:02:43','2020-01-23 08:02:43'),
(55,'projects',1,'uploads/projects/1','5e29a07b25f2f_1.png','project-4.png',NULL,'projects_multiple_images',0,'2020-01-23 08:02:43','2020-01-23 08:02:43'),
(56,'technology',1,'uploads/technology/1','5e2a84644ac4a_1.png','career-img.png',NULL,'technologies_single_cover',0,'2020-01-24 00:15:08','2020-01-24 00:15:08'),
(57,'technology',2,'uploads/technology/2','5e2a85d4eee20_2.png','blog-item-2.png',NULL,'technologies_single_cover',0,'2020-01-24 00:21:17','2020-01-24 00:21:17'),
(58,'technology',4,'uploads/technology/4','5e2a861c414b0_4.jpg','coffee.jpg',NULL,'technologies_single_cover',0,'2020-01-24 00:22:28','2020-01-24 00:22:28'),
(59,'technology',5,'uploads/technology/5','5e2a863498535_5.png','Mask Group 4.png',NULL,'technologies_single_cover',0,'2020-01-24 00:22:53','2020-01-24 00:22:53'),
(61,'technology',6,'uploads/technology/6','5e2a8659b9748_6.png','our-team-img.png',NULL,'technologies_single_cover',0,'2020-01-24 00:23:30','2020-01-24 00:23:30'),
(62,'technology',7,'uploads/technology/7','5e2a8680cdd0b_7.png','service-banner.png',NULL,'technologies_single_cover',0,'2020-01-24 00:24:10','2020-01-24 00:24:10'),
(63,'technology',8,'uploads/technology/8','5e2a873190ba4_8.png','project-4.png',NULL,'technologies_single_cover',0,'2020-01-24 00:27:05','2020-01-24 00:27:05'),
(64,'testimonials',4,'uploads/testimonials/4','5e318e241ac3c_4.png','testimonial.png',NULL,'testimonials_single_main',0,'2020-01-29 08:22:36','2020-01-29 08:22:36'),
(65,'testimonials',5,'uploads/testimonials/5','5e318eb729062_5.png','testimonial.png',NULL,'testimonials_single_main',0,'2020-01-29 08:25:03','2020-01-29 08:25:03'),
(66,'team',1,'uploads/team/1','5e326d7378e5f_1.png','testimonial.png',NULL,'teams_single_main',0,'2020-01-30 00:15:23','2020-01-30 00:15:23'),
(67,'team',2,'uploads/team/2','5e326facc476b_2.jpg','team1.jpg',NULL,'teams_single_main',0,'2020-01-30 00:24:52','2020-01-30 00:24:52'),
(68,'team',3,'uploads/team/3','5e326fec71fa6_3.png','user.png',NULL,'teams_single_main',0,'2020-01-30 00:25:56','2020-01-30 00:25:56'),
(80,'technology',2,'uploads/technology/2','5e425f64a46a2_2.png','Bootstrap.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(81,'technology',2,'uploads/technology/2','5e425f64b1028_2.png','D3 JS.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(82,'technology',2,'uploads/technology/2','5e425f64b6416_2.png','Django.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(83,'technology',2,'uploads/technology/2','5e425f64ba42c_2.png','Flask.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(84,'technology',2,'uploads/technology/2','5e425f64be8e4_2.png','Git.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(85,'technology',2,'uploads/technology/2','5e425f64c2c3f_2.png','Jinja 2.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(86,'technology',2,'uploads/technology/2','5e425f64ccc67_2.png','Jupiter book.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(87,'technology',2,'uploads/technology/2','5e425f64d7654_2.png','Mercurial.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(88,'technology',2,'uploads/technology/2','5e425f64dfb5f_2.png','MySQL.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(89,'technology',2,'uploads/technology/2','5e425f64e883f_2.png','PowerShell.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:40','2020-02-11 02:31:40'),
(90,'technology',2,'uploads/technology/2','5e425f64effba_2.png','Pycharm.png',NULL,'technologies_multiple_tech',0,'2020-02-11 02:31:41','2020-02-11 02:31:41'),
(91,'technology',1,'uploads/technology/1','5e4401edd9bae_1.png','Node.png',NULL,'technologies_single_main',0,'2020-02-12 08:17:26','2020-02-12 08:17:26'),
(92,'technology',2,'uploads/technology/2','5e44020231df1_2.png','Python.png',NULL,'technologies_single_main',0,'2020-02-12 08:17:46','2020-02-12 08:17:46'),
(93,'technology',3,'uploads/technology/3','5e44020e404f2_3.png','Php.png',NULL,'technologies_single_main',0,'2020-02-12 08:17:58','2020-02-12 08:17:58'),
(94,'technology',4,'uploads/technology/4','5e4402198c714_4.png','Anhgular.png',NULL,'technologies_single_main',0,'2020-02-12 08:18:10','2020-02-12 08:18:10'),
(95,'technology',5,'uploads/technology/5','5e440220c7aac_5.png','React Native.png',NULL,'technologies_single_main',0,'2020-02-12 08:18:17','2020-02-12 08:18:17'),
(96,'technology',6,'uploads/technology/6','5e44022e8dbf7_6.png','React.png',NULL,'technologies_single_main',0,'2020-02-12 08:18:30','2020-02-12 08:18:30'),
(97,'technology',7,'uploads/technology/7','5e4402395957a_7.png','ML AI.png',NULL,'technologies_single_main',0,'2020-02-12 08:18:41','2020-02-12 08:18:41'),
(98,'technology',8,'uploads/technology/8','5e44023f20bce_8.png','Cloud devops.png',NULL,'technologies_single_main',0,'2020-02-12 08:18:47','2020-02-12 08:18:47');

/*Table structure for table `roll_permissions` */

DROP TABLE IF EXISTS `roll_permissions`;

CREATE TABLE `roll_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `roll_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roll_permissions_roll_id_permission_id_index` (`roll_id`,`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roll_permissions` */

insert  into `roll_permissions`(`id`,`roll_id`,`permission_id`,`module`,`value`,`created_at`,`updated_at`) values 
(1,1,1,'global',1,'2019-12-19 10:07:50','2019-12-19 10:07:50'),
(2,1,2,'global',1,'2019-12-19 10:07:53','2019-12-19 10:07:53'),
(3,1,3,'global',1,'2019-12-19 10:07:53','2019-12-19 10:07:53'),
(4,1,4,'global',1,'2019-12-19 10:07:54','2019-12-19 10:07:54'),
(5,1,5,'global',1,'2019-12-19 10:07:55','2019-12-19 10:07:55'),
(6,1,6,'global',1,'2019-12-19 10:07:56','2019-12-19 10:07:56'),
(7,1,1,'projects',1,'2019-12-23 23:28:48','2019-12-23 23:28:50');

/*Table structure for table `rolls` */

DROP TABLE IF EXISTS `rolls`;

CREATE TABLE `rolls` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `rolls` */

insert  into `rolls`(`id`,`title`,`admin`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Super Admin',1,'2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(2,'Admin',0,'2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(3,'Manager',0,'2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(4,'Programmer',0,'2019-12-19 09:16:21','2019-12-19 09:16:21',NULL);

/*Table structure for table `seos` */

DROP TABLE IF EXISTS `seos`;

CREATE TABLE `seos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `og_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_card` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seos` */

insert  into `seos`(`id`,`url`,`title`,`description`,`og_type`,`og_image`,`twitter_card`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'/',NULL,NULL,NULL,NULL,NULL,'2019-12-19 08:11:58','2020-01-29 03:42:58','2020-01-29 03:42:58'),
(4,'/about',NULL,NULL,NULL,NULL,NULL,'2019-12-22 23:54:29','2020-01-29 03:42:59','2020-01-29 03:42:59'),
(6,'/services',NULL,NULL,NULL,NULL,NULL,'2019-12-23 01:28:27','2020-01-29 03:43:02','2020-01-29 03:43:02'),
(7,'/team',NULL,NULL,NULL,NULL,NULL,'2019-12-23 01:33:25','2020-01-29 03:43:06','2020-01-29 03:43:06'),
(8,'/technology',NULL,NULL,NULL,NULL,NULL,'2019-12-23 17:21:03','2020-01-29 03:43:06','2020-01-29 03:43:06'),
(9,'/technology/uiux',NULL,NULL,NULL,NULL,NULL,'2019-12-23 17:27:34','2020-01-29 03:43:22','2020-01-29 03:43:22'),
(10,'/work',NULL,NULL,NULL,NULL,NULL,'2019-12-23 17:33:15','2020-01-29 03:43:22','2020-01-29 03:43:22'),
(11,'/work/work1',NULL,NULL,NULL,NULL,NULL,'2019-12-23 17:35:32','2020-01-29 03:43:25','2020-01-29 03:43:25'),
(12,'/career',NULL,NULL,NULL,NULL,NULL,'2019-12-23 17:47:55','2020-01-29 03:43:00','2020-01-29 03:43:00'),
(13,'/career/php',NULL,NULL,NULL,NULL,NULL,'2019-12-23 17:52:41','2020-01-29 03:43:01','2020-01-29 03:43:01'),
(14,'/career/index.html',NULL,NULL,NULL,NULL,NULL,'2019-12-23 18:15:46','2020-01-29 03:43:01','2020-01-29 03:43:01'),
(15,'/case-study2',NULL,NULL,NULL,NULL,NULL,'2019-12-24 00:59:05','2020-01-29 03:43:01','2020-01-29 03:43:01'),
(16,'/services/dedicated-development','Dedicated Development','Dedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated DevelopmentDedicated Development',NULL,NULL,NULL,'2019-12-24 17:38:51','2020-01-29 03:43:03','2020-01-29 03:43:03'),
(17,'/work/worlds-largest-wine-community-1',NULL,NULL,NULL,NULL,NULL,'2019-12-24 21:56:42','2020-01-29 03:43:25','2020-01-29 03:43:25'),
(18,'/technology/frontend',NULL,NULL,NULL,NULL,NULL,'2019-12-25 21:31:20','2020-01-29 03:43:07','2020-01-29 03:43:07'),
(19,'/technology/php',NULL,NULL,NULL,NULL,NULL,'2019-12-25 21:31:35','2020-01-29 03:43:21','2020-01-29 03:43:21'),
(20,'/career/developer',NULL,NULL,NULL,NULL,NULL,'2019-12-27 00:30:13','2020-01-29 03:43:00','2020-01-29 03:43:00'),
(21,'/work/dummy-project',NULL,NULL,NULL,NULL,NULL,'2019-12-27 02:45:21','2020-01-29 03:43:23','2020-01-29 03:43:23'),
(22,'/work/the-best-coffee-youve-ever-made-1',NULL,NULL,NULL,NULL,NULL,'2019-12-27 03:26:04','2020-01-29 03:43:24','2020-01-29 03:43:24'),
(23,'/services/custom-software-development',NULL,NULL,NULL,NULL,NULL,'2019-12-27 04:02:19','2020-01-29 03:43:03','2020-01-29 03:43:03'),
(24,'/work/dedicated-development',NULL,NULL,NULL,NULL,NULL,'2019-12-27 04:29:00','2020-01-29 03:43:23','2020-01-29 03:43:23'),
(25,'/work/uxui-design',NULL,NULL,NULL,NULL,NULL,'2019-12-27 04:30:00','2020-01-29 03:43:24','2020-01-29 03:43:24'),
(26,'/technology/mobile-app',NULL,NULL,NULL,NULL,NULL,'2019-12-27 06:42:15','2020-01-29 03:43:19','2020-01-29 03:43:19'),
(29,'/services/uxui-design',NULL,NULL,NULL,NULL,NULL,'2020-01-02 07:48:37','2020-01-29 03:43:05','2020-01-29 03:43:05'),
(30,'/services/mobile-app-developement',NULL,NULL,NULL,NULL,NULL,'2020-01-02 07:48:39','2020-01-29 03:43:04','2020-01-29 03:43:04'),
(31,'/services/node',NULL,NULL,NULL,NULL,NULL,'2020-01-07 01:23:50','2020-01-29 03:43:05','2020-01-29 03:43:05'),
(33,'/technology/node',NULL,NULL,NULL,NULL,NULL,'2020-01-07 01:25:10','2020-01-29 03:43:20','2020-01-29 03:43:20'),
(34,'/services/frontend-development',NULL,NULL,NULL,NULL,NULL,'2020-01-07 01:27:06','2020-01-29 03:43:04','2020-01-29 03:43:04'),
(35,'/services/cloud-dev-ops',NULL,NULL,NULL,NULL,NULL,'2020-01-07 03:27:34','2020-01-29 03:43:02','2020-01-29 03:43:02'),
(37,'/work/react',NULL,NULL,NULL,NULL,NULL,'2020-01-07 04:50:31','2020-01-29 03:43:23','2020-01-29 03:43:23'),
(38,'/technology/react',NULL,NULL,NULL,NULL,NULL,'2020-01-07 04:53:20','2020-01-29 03:43:20','2020-01-29 03:43:20'),
(39,'/technology/angular',NULL,NULL,NULL,NULL,NULL,'2020-01-07 04:53:46','2020-01-29 03:43:07','2020-01-29 03:43:07'),
(40,'/approach',NULL,NULL,NULL,NULL,NULL,'2020-01-09 03:25:48','2020-01-29 03:43:00','2020-01-29 03:43:00'),
(41,'/technology/react-native',NULL,NULL,NULL,NULL,NULL,'2020-01-09 05:24:58','2020-01-29 03:43:21','2020-01-29 03:43:21'),
(43,'/app-assets/vendors/js/datatable/buttons.print.min',NULL,NULL,NULL,NULL,NULL,'2020-01-18 03:05:09','2020-01-29 03:42:59','2020-01-29 03:42:59'),
(44,'/app-assets/vendors/js/datatable/buttons.html5.min',NULL,NULL,NULL,NULL,NULL,'2020-01-18 03:05:09','2020-01-29 03:42:59','2020-01-29 03:42:59'),
(45,'/services/web-application-development',NULL,NULL,NULL,NULL,NULL,'2020-01-27 01:00:16','2020-01-29 03:43:06','2020-01-29 03:43:06'),
(46,'/services/mobile-app-development',NULL,NULL,NULL,NULL,NULL,'2020-01-27 01:27:32','2020-01-29 03:43:04','2020-01-29 03:43:04'),
(47,'/services/dedicated-digital-teams',NULL,NULL,NULL,NULL,NULL,'2020-01-27 01:28:13','2020-01-29 03:43:03','2020-01-29 03:43:03'),
(48,'/','Citrusbug Technolabs','Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs Citrusbug Technolabs',NULL,NULL,NULL,'2020-01-29 03:43:53','2020-01-29 07:58:24',NULL),
(49,'/services/web-application-development','Web Application Development','Web Application Development Web Application Development Web Application Development Web Application Development Web Application Development Web Application Development Web Application Development Web Application Development Web Application Development Web Application Development Web Application Development',NULL,NULL,NULL,'2020-01-29 03:44:01','2020-01-29 08:00:55',NULL),
(50,'/services/custom-software-development',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:04','2020-01-29 03:44:04',NULL),
(51,'/services/mobile-app-development',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:06','2020-01-29 03:44:06',NULL),
(52,'/services/dedicated-digital-teams',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:08','2020-01-29 03:44:08',NULL),
(53,'/technology/node',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:11','2020-01-29 03:44:11',NULL),
(54,'/technology/python',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:13','2020-01-29 03:44:13',NULL),
(55,'/technology/php',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:14','2020-01-29 03:44:14',NULL),
(56,'/technology/angular',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:15','2020-01-29 03:44:15',NULL),
(57,'/technology/react-native',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:16','2020-01-29 03:44:16',NULL),
(58,'/technology/react',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:17','2020-01-29 03:44:17',NULL),
(59,'/technology/ml-ai',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:19','2020-01-29 03:44:19',NULL),
(60,'/technology/cloud-devops',NULL,NULL,NULL,NULL,NULL,'2020-01-29 03:44:20','2020-01-29 03:44:20',NULL),
(61,'/work',NULL,NULL,NULL,NULL,NULL,'2020-01-29 06:57:37','2020-01-29 06:57:37',NULL),
(62,'/technology',NULL,NULL,NULL,NULL,NULL,'2020-01-29 06:58:23','2020-01-29 06:58:23',NULL),
(63,'/team',NULL,NULL,NULL,NULL,NULL,'2020-01-29 06:59:43','2020-01-29 06:59:43',NULL),
(64,'/about',NULL,NULL,NULL,NULL,NULL,'2020-01-29 07:02:12','2020-01-29 07:02:12',NULL),
(65,'/career',NULL,NULL,NULL,NULL,NULL,'2020-01-29 07:03:09','2020-01-29 07:03:09',NULL),
(66,'/work/worlds-largest-wine-community-1',NULL,NULL,NULL,NULL,NULL,'2020-01-30 00:06:16','2020-01-30 00:06:16',NULL),
(67,'/admin',NULL,NULL,NULL,NULL,NULL,'2020-02-10 02:40:12','2020-02-10 02:40:12',NULL),
(68,'/services',NULL,NULL,NULL,NULL,NULL,'2020-02-10 05:48:51','2020-02-10 05:48:51',NULL),
(69,'/work/uve',NULL,NULL,NULL,NULL,NULL,'2020-02-10 06:36:01','2020-02-10 06:36:01',NULL),
(70,'/work/reverb',NULL,NULL,NULL,NULL,NULL,'2020-02-10 06:59:11','2020-02-10 06:59:11',NULL),
(71,'/inquiries/save',NULL,NULL,NULL,NULL,NULL,'2020-02-11 04:27:55','2020-02-11 04:27:55',NULL),
(72,'/work/perk-coffee',NULL,NULL,NULL,NULL,NULL,'2020-02-12 03:31:50','2020-02-12 03:31:50',NULL),
(73,'/technology/jsx-reactjs',NULL,NULL,NULL,NULL,NULL,'2020-02-12 06:16:59','2020-02-12 06:16:59',NULL),
(74,'/mailable',NULL,NULL,NULL,NULL,NULL,'2020-02-12 09:31:54','2020-02-12 09:31:54',NULL);

/*Table structure for table `servicedetails` */

DROP TABLE IF EXISTS `servicedetails`;

CREATE TABLE `servicedetails` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumtext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `services_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `servicedetails_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `servicedetails` */

insert  into `servicedetails`(`id`,`title`,`slug`,`desc`,`description`,`created_at`,`updated_at`,`deleted_at`,`services_id`) values 
(1,'Approach','understand-the-project-1','Understand the project','<p>The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn&#39;t find the right beige from a set of 200 and had to design a new beige. He seemed like the ultimate perfectionist &ndash; sometimes.</p>','2019-12-24 21:26:38','2019-12-24 21:50:16',NULL,1),
(2,'APPROACH','site-architecture-and-wireframes-1','Site architecture and wireframes','<p>The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn&#39;t find the right beige from a set of 200 and had to design a new beige. He seemed like the ultimate perfectionist &ndash; sometimes.</p>','2019-12-24 21:27:33','2019-12-24 21:50:37',NULL,1),
(3,'Approach','visual-design-1','Visual\r\ndesign','<p>After the wireframes are perfected, we start work on the look of each webpage. We keep the best UI practices at the center and incorporate your businesses&rsquo; creative guidelines to design a website that is a seamless extension of your brand in the digital world.</p>','2019-12-24 21:28:34','2019-12-24 21:50:56',NULL,1);

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intro` mediumtext COLLATE utf8mb4_unicode_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `ordering` int(11) DEFAULT NULL,
  `param` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `services_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `services` */

insert  into `services`(`id`,`title`,`slug`,`icon`,`intro`,`description`,`created_at`,`updated_at`,`deleted_at`,`updated_by`,`image`,`ordering`,`param`) values 
(1,'Web Application Development','web-application-development','/storage/uploads/service_icon/front-end.svg','<p>We enable businesses to deliver transformational services for their customers and employees via the internet by building seamless internet powered applications</p>','<h2>Citrusbug partners enterprises to build and grow their presence on the internet through rich customer facing web apps and powerful enterprise cloud applications.</h2>\r\n\r\n<p><strong>Build for the web, billed for the success</strong></p>\r\n\r\n<p><em>At Citrusbug, our services go beyond building what customers demand alone. We partner in their journey to building new experiences on the web for their customers and operations and use our experience to help them discover new revenue opportunities, digital channels and scalable business models powered by the internet</em>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Our areas of expertise include:</p>\r\n\r\n<ul>\r\n	<li>Enterprise Web Application Development</li>\r\n	<li>Custom Web portal development</li>\r\n	<li>e-Commerce website development</li>\r\n	<li>Customizable Content Management Systems</li>\r\n</ul>','2019-12-23 18:41:01','2020-02-12 04:40:55',NULL,1,NULL,1,'a:3:{s:8:\"tag_line\";s:159:\"We enable businesses to deliver transformational services for their customers and employees via the internet by building seamless internet powered applications\";s:10:\"hire_title\";s:15:\"Web Application\";s:9:\"hire_text\";s:419:\"Forget about project complexity and hire an expert web developer to help you accomplish your business projects today. No matter you are searching custom web developer or want to hire web developer to build a website from scratch, we have ardent staff, who deliver excellent work with super quality & perfection. If you want to be on the top, get equipped with an army of web developers provided by Citrusbug Technolabs.\";}'),
(2,'Mobile App Development','mobile-app-development','/storage/uploads/service_icon/ui-ux.svg','<p>Build more connected experiences for your customers on their favorite devices with our mobile app development services</p>','<p>We empower businesses to build a connected customer experience with mobile-ready business apps across a range of smart devices and platforms.</p>\r\n\r\n<p><strong>Built for the next generation of mobility</strong></p>\r\n\r\n<p><em>Our mobile app development services focus on building apps to meet the end-to-end needs of today&rsquo;s smartphone driven business and operational landscape. From deeper functionality integration to intelligent automation, our mobility practice thrives to deliver apps that are profitable and future proof.</em></p>\r\n\r\n<p>Our areas of expertise include:</p>\r\n\r\n<ul>\r\n	<li>Native mobile application development</li>\r\n	<li>Hybrid mobile application development</li>\r\n	<li>Smart device application development</li>\r\n</ul>','2019-12-23 18:41:22','2020-02-12 04:38:58',NULL,1,NULL,3,'a:3:{s:8:\"tag_line\";s:118:\"Build more connected experiences for your customers on their favorite devices with our mobile app development services\";s:10:\"hire_title\";s:10:\"Mobile App\";s:9:\"hire_text\";N;}'),
(3,'Custom Software Development','custom-software-development','/storage/uploads/service_icon/custom-software-developememnt.svg','<p>Enhance your digital competitiveness with unique software solutions tailored to suit your unique business models</p>','<p>To embrace success in a future governed by digital, Citrusbug partners businesses to strategically identify and build custom software for digital success.</p>\r\n\r\n<p><strong>Software designed for customer delight</strong></p>\r\n\r\n<p><em>Businesses small and big partner us to drive their digital ambitions forward by leveraging our expertise in custom software development. From on-boarding and customizing innovative cloud-based business systems to building innovative solutions from the ground up, we help businesses realize better value for their software by using the right technology and following best practices.</em></p>\r\n\r\n<p>Our areas of expertise include:</p>\r\n\r\n<ul>\r\n	<li>Enterprise Application Development</li>\r\n	<li>UI/UX Services</li>\r\n	<li>Systems Integration</li>\r\n	<li>Cloud Platform services</li>\r\n	<li>Product Engineering</li>\r\n</ul>','2019-12-23 18:41:43','2020-02-12 04:38:55',NULL,1,NULL,2,'a:3:{s:8:\"tag_line\";s:112:\"Enhance your digital competitiveness with unique software solutions tailored to suit your unique business models\";s:10:\"hire_title\";s:15:\"Custom Software\";s:9:\"hire_text\";N;}'),
(4,'Dedicated Digital Teams','dedicated-digital-teams','/storage/uploads/service_icon/app-development.svg','<p>Enable faster digital success with a dedicated world-class technology team available exclusively for you</p>','<p>Assemble your own team for driving the future of your digital success with less overhead, unlimited flexibility and unparalleled cost efficiency.</p>\r\n\r\n<p><br />\r\n<strong>A loyal team to build your global ambitions</strong></p>\r\n\r\n<p><em>Elevate your digital ambitions by setting up your own remote team of experts from a large talent pool having rare skills that can augment your in-house workforce. From recruitment to managing contracts transparently, Citrusbug manages your overheads while you strategize your development goals utilizing our state-of-the-art infrastructure and niche talents in technology and management. </em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Our areas of expertise include:</p>\r\n\r\n<ul>\r\n	<li>Managed Application Development Services</li>\r\n	<li>UI/UX Services</li>\r\n	<li>Quality Assurance</li>\r\n	<li>Application Maintenance &amp; Support Services</li>\r\n</ul>','2019-12-23 18:42:07','2020-02-12 04:39:05',NULL,1,NULL,4,'a:3:{s:8:\"tag_line\";s:104:\"Enable faster digital success with a dedicated world-class technology team available exclusively for you\";s:10:\"hire_title\";s:9:\"Dedicated\";s:9:\"hire_text\";N;}'),
(5,'Cloud & Dev Ops','cloud-dev-ops','/storage/uploads/service_icon/cloud-and-developement.svg','<p>Cloud Solution</p>','<p>Cloud Solution</p>','2020-01-04 00:42:08','2020-01-29 05:17:35','2020-01-29 05:17:35',1,NULL,6,NULL),
(6,'ML & AI','ml-ai','/storage/uploads/service_icon/ml-ai.svg','<p>ML &amp; AI</p>','<p>ML &amp; AI</p>','2020-01-07 00:44:11','2020-01-29 05:17:36','2020-01-29 05:17:36',1,NULL,7,NULL),
(7,'Test','test',NULL,'<p>Test</p>','<p>Test</p>','2020-01-29 05:03:15','2020-01-29 07:23:43','2020-01-29 07:23:43',1,NULL,NULL,NULL);

/*Table structure for table `services_technology` */

DROP TABLE IF EXISTS `services_technology`;

CREATE TABLE `services_technology` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tech_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `services_technology` */

insert  into `services_technology`(`id`,`created_at`,`updated_at`,`tech_id`,`service_id`,`deleted_at`) values 
(54,NULL,NULL,1,7,NULL),
(55,NULL,NULL,2,7,NULL),
(156,NULL,NULL,8,3,NULL),
(157,NULL,NULL,7,3,NULL),
(158,NULL,NULL,1,3,NULL),
(159,NULL,NULL,3,3,NULL),
(160,NULL,NULL,2,3,NULL),
(161,NULL,NULL,6,3,NULL),
(162,NULL,NULL,5,2,NULL),
(163,NULL,NULL,4,4,NULL),
(164,NULL,NULL,8,4,NULL),
(165,NULL,NULL,7,4,NULL),
(166,NULL,NULL,1,4,NULL),
(167,NULL,NULL,3,4,NULL),
(168,NULL,NULL,2,4,NULL),
(169,NULL,NULL,6,4,NULL),
(170,NULL,NULL,5,4,NULL),
(171,NULL,NULL,4,1,NULL),
(172,NULL,NULL,1,1,NULL),
(173,NULL,NULL,3,1,NULL),
(174,NULL,NULL,2,1,NULL),
(175,NULL,NULL,6,1,NULL),
(176,NULL,NULL,13,1,NULL),
(177,NULL,NULL,12,1,NULL),
(178,NULL,NULL,9,1,NULL),
(179,NULL,NULL,11,1,NULL),
(180,NULL,NULL,10,1,NULL);

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fkey` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fvalue` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finformation` longtext COLLATE utf8mb4_unicode_ci,
  `ftype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`fkey`,`fvalue`,`created_at`,`updated_at`,`module`,`finformation`,`ftype`) values 
(1,'bg-color','primary','2019-12-19 09:16:21','2019-12-19 09:16:21','global',NULL,NULL),
(2,'sidebar-background','http://localhost:8000/app-assets/img/sidebar-bg/03.jpg','2019-12-19 09:16:21','2019-12-19 09:16:21','global',NULL,NULL),
(3,'sidebar-width','sidebar-lg','2019-12-19 09:16:21','2019-12-19 09:16:21','global',NULL,NULL),
(4,'theme-layout','light','2019-12-19 09:16:21','2019-12-27 03:15:02','global',NULL,NULL),
(5,'theme-transparent-bg','bg-glass-1','2019-12-19 09:16:21','2019-12-19 09:16:21','global',NULL,NULL),
(6,'compact-menu','0','2019-12-19 09:16:21','2019-12-19 09:16:21','global',NULL,NULL),
(7,'active_lang','en','2019-12-19 09:16:21','2019-12-19 09:16:21','global',NULL,NULL),
(8,'admin-menu-json','[{\"text\":\"Inquiries\",\"icon\":\"fa fa-adjust\",\"href\":\"/admin/inquiries\",\"target\":\"_self\",\"title\":\"\",\"rolls\":[\"1\",\"2\",\"3\"]},{\"text\":\"Baners\",\"href\":\"/admin/baners\",\"icon\":\"fa fa-align-center\",\"target\":\"_self\",\"title\":\"\",\"rolls\":[\"1\",\"2\",\"3\"]},{\"text\":\"Services\",\"href\":\"/admin/services\",\"icon\":\"fa fa-user-plus\",\"target\":\"_self\",\"title\":\"Services\",\"rolls\":[\"1\",\"3\",\"6\",\"7\"]},{\"text\":\"Technolgy\",\"href\":\"/admin/technologies\",\"icon\":\"fa fa-briefcase\",\"target\":\"_self\",\"title\":\"Technologies\",\"rolls\":[\"1\",\"2\"]},{\"text\":\"Projects\",\"href\":\"/admin/projects\",\"icon\":\"fa fa-american-sign-language-interpreting\",\"target\":\"_self\",\"title\":\"\",\"rolls\":[\"1\",\"2\",\"3\"]},{\"text\":\"Partners\",\"href\":\"/admin/partners\",\"icon\":\"fa fa-handshake-o\",\"target\":\"_self\",\"title\":\"Partners\",\"rolls\":[\"1\"]},{\"text\":\"Testimonial\",\"href\":\"/admin/testimonials\",\"icon\":\"fa fa-ambulance\",\"target\":\"_self\",\"title\":\"Testimonial\",\"rolls\":[\"1\",\"2\",\"3\"]},{\"text\":\"Team\",\"href\":\"/admin/teams\",\"icon\":\"fa fa-users\",\"target\":\"_self\",\"title\":\"\",\"rolls\":[\"1\"]},{\"text\":\"Career\",\"href\":\"/admin/career\",\"icon\":\"fa fa-graduation-cap\",\"target\":\"_self\",\"title\":\"\",\"rolls\":[\"1\"]},{\"text\":\"Setting\",\"href\":\"/admin/settings/module/global\",\"icon\":\"fa fa-database\",\"target\":\"_self\",\"title\":\"Settings\",\"rolls\":[\"1\",\"3\",\"6\"]},{\"text\":\"Seo\",\"href\":\"/admin/seos\",\"icon\":\"fa fa-buysellads\",\"target\":\"_self\",\"title\":\"SEO\",\"rolls\":[\"1\",\"3\",\"6\"]},{\"text\":\"Users\",\"href\":\"/admin/users\",\"icon\":\"fa fa-user\",\"target\":\"_self\",\"title\":\"\",\"rolls\":[\"1\",\"3\",\"6\",\"7\"]},{\"text\":\"Roll Permissions\",\"href\":\"/admin/rollpermissions/global\",\"icon\":\"fa fa-arrows\",\"target\":\"_self\",\"title\":\"Roll Permissions\",\"rolls\":[\"1\",\"3\"]},{\"text\":\"Rolls\",\"href\":\"/admin/rolls\",\"icon\":\"fa fa-address-book\",\"target\":\"_self\",\"title\":\"Rolls\",\"rolls\":[\"1\",\"3\"]}]','2019-12-19 09:16:21','2020-02-11 00:59:22','global',NULL,NULL),
(9,'default_title','Default Site Title','2019-12-19 09:16:21','2019-12-19 09:16:21','seos',NULL,NULL),
(10,'default_description','Description Description Description Description Description Description Description DescriptionDescription Description Description','2019-12-19 09:16:21','2019-12-19 09:16:21','seos',NULL,NULL),
(11,'default_og_type','website','2019-12-19 09:16:21','2019-12-19 09:16:21','seos',NULL,NULL),
(12,'default_twitter_card','summary','2019-12-19 09:16:21','2019-12-19 09:16:21','seos',NULL,NULL),
(13,'fb_admins','888777555555','2019-12-19 09:16:21','2019-12-19 09:16:21','seos',NULL,NULL),
(14,'ADMIN_LOGO','/storage/uploads/logo.png','2019-12-19 09:16:21','2019-12-23 22:34:10','global',NULL,'image'),
(15,'footer_button_text','<div class=\"messages-text-heading\">\r\n                                <h3> Want To Say <span class=\"font-italic\">Hello?</span> </h3>\r\n                            </div>\r\n                            <div class=\"button-row\"> \r\n                                <a href=\"#\" class=\"btn btn-messages start-project\">SEND US A MESSAGE</a>\r\n                            </div>','2019-12-19 10:11:09','2020-02-11 07:13:54','global',NULL,'textarea'),
(16,'footer_development_center','<h5>Development Center</h5>                                    <p>                                        <span class=\"span-block\">A 411, Shivalik Corporate Park </span>                                        <span class=\"span-block\">Satellite, Ahmedabad - 380015 India.</span>                                    </p>','2019-12-19 10:11:53','2020-02-11 07:13:40','global',NULL,'textarea'),
(17,'footer_phone','<h5>Phone: </h5>\r\n                                    <p> <a href=\"tel:19739476185\" class=\"co-link\">19739476185</a> </p>','2019-12-19 22:56:54','2019-12-19 22:56:54','global',NULL,NULL),
(18,'footer_email','<div class=\"contact-info-row\">\r\n                                    <h5>Email: </h5>\r\n                                    <p> <a href=\"mailto:hello@company.com\" class=\"co-link\">hello@company.com</a> </p>\r\n                                </div>','2019-12-19 22:57:11','2019-12-19 22:57:11','global',NULL,NULL),
(19,'footer_send_message','<p class=\"font-italic\">Send message</p>\r\n                                <p ><a href=\"mailto:info@citrusbug.com\" class=\"link font-italic\" >info@citrusbug.com</a></p>','2019-12-19 22:57:49','2019-12-19 22:57:49','global',NULL,NULL),
(20,'site_home_service_title','Helping brands solve real problems with technology','2019-12-23 18:45:45','2020-01-27 00:54:56','global',NULL,'editor'),
(21,'site_home_service_desc','<p>We make technology simple for businesses across industries and geographies and accelerate their digital journey cost-effectively.</p>','2019-12-23 18:46:17','2020-01-27 00:55:31','global',NULL,'editor'),
(22,'site_home_project_title','<span class=\"span-block\">A small selection</span>\r\n                                    <span class=\"span-block\">of <a href=\"#\" class=\"link1\">My work</a>, enjoy.</span>','2019-12-23 23:26:37','2019-12-23 23:27:51','projects',NULL,'textarea'),
(23,'site_home_project_desc','<p>We are a digital agency that specializes in User Experience Design</p>','2019-12-23 23:28:23','2019-12-23 23:28:23','projects',NULL,'textarea'),
(24,'site_service_service_title','Get to know a bit more about Our skills or feel free to look around <a href=\"#\" class=\"heading-link\">Our</a> <a href=\"#\" class=\"heading-link\">coding</a>and <a href=\"#\" class=\"heading-link\">design</a> <a href=\"#\" class=\"heading-link\">works. </a>','2019-12-24 17:20:19','2019-12-24 17:20:19','services',NULL,'textarea'),
(25,'site_service_service_desc','We are a digital agency that specializes in User Experience Design','2019-12-24 17:20:45','2019-12-24 17:20:45','services',NULL,'textarea'),
(26,'site_technology_technology_title','<span class=\"span-block\">Customer-first technology </span><span class=\"span-block\"><a href=\"#\" class=\"link link5\">crafted <span class=\"line-5span\">specifically</span></a> for the <span class=\"span-block\">IT industry.</span></span>','2019-12-25 19:55:28','2019-12-25 19:55:28','global',NULL,'textarea'),
(27,'site_technology_technology_description','We are a digital agency that specializes in User Experience Design','2019-12-25 19:58:03','2019-12-25 19:58:03','global',NULL,'text'),
(28,'site_team_title','<span class=\"span-block\">The </span><span class=\"span-block\">Team </span>','2019-12-25 21:37:16','2019-12-25 21:37:16','global',NULL,'text'),
(29,'site_team_description','The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn\'t find the right beige from a set of 200 and had to design a new beige. He seemed like the ultimate perfectionist – sometimes.','2019-12-25 21:38:05','2019-12-25 21:38:05','global',NULL,'text'),
(30,'site_career_title','Career','2019-12-26 01:11:32','2019-12-26 01:11:32','global',NULL,'text'),
(31,'site_career_description','We are growing, and we are looking for skilled individuals to grow with us.','2019-12-26 01:11:55','2019-12-26 01:11:55','global',NULL,'textarea'),
(32,'site_career_single_opportunity_title','The Real Opportunity!','2019-12-26 01:32:58','2019-12-26 01:32:58','global',NULL,'text'),
(33,'site_careers_tagline_below_career_list','<span class=\"span-block\"> Want to work in a beautiful office? Check. In a great city? </span> <span class=\"span-block\">Have a look on our jobs openings.</span>','2019-12-27 00:18:08','2019-12-27 00:19:42','global',NULL,'textarea'),
(34,'site_careers_middle_banner_title','<h2>Bringing your A <span class=\"line link7\">game</span> to an interview.</h2>','2019-12-27 00:22:24','2019-12-27 00:24:20','global',NULL,'editor'),
(35,'site_careers_middle_banner_desc','We are a digital agency that specializes in User Experience Design','2019-12-27 00:24:04','2019-12-27 00:24:04','global',NULL,'textarea'),
(36,'site_work_title','<h2>\r\n                                    <span class=\"span-block\">All Work </span>\r\n                                    <span class=\"span-block\">made with <a href=\"#\" class=\"heading-link\">passion.</a></span>\r\n                                </h2>','2019-12-27 02:51:02','2019-12-27 02:51:02','global',NULL,'textarea'),
(37,'site_work_description','We are a digital agency that specializes in User Experience Design','2019-12-27 02:54:44','2019-12-27 02:54:44','global',NULL,'text'),
(41,'site_work_below_banner_description','Whatever the challenge, we always deliver a solution.','2019-12-27 03:21:57','2019-12-27 03:21:57','global',NULL,'text'),
(42,'site_work_below_banner_titles','<ul>\r\n                                        <li><a><span>01</span>Strategy</a></li>\r\n                                        <li><a><span>02</span>Design</a></li>\r\n                                        <li><a><span>03</span>Development</a></li>\r\n                                    </ul>','2019-12-27 03:35:50','2019-12-27 03:35:50','global',NULL,'editor'),
(43,'site_work_below_banner_background_image','/storage/uploads/solution-img.png','2019-12-27 03:43:41','2019-12-27 03:47:53','global',NULL,'image'),
(44,'site_work_below_banner_image','/storage/uploads/user.png','2019-12-27 03:44:30','2019-12-27 03:48:29','global',NULL,'image'),
(45,'site_about_title','<h2>We work with aspiring businesses</h2>','2019-12-27 05:16:29','2020-02-10 04:01:44','global',NULL,'editor'),
(46,'site_about_description','<p>At Citrusbug, we build lasting relationships with businesses by providing them top notch<br />\r\ncustom software solutions. From a humble beginning in 2013, we have grown into a<br />\r\npremium digital partner for SMB&rsquo;s and startup businesses by helping them build enterprise<br />\r\nsoftware solutions for their everyday needs.<br />\r\nWe enable digital transformation for businesses through our talented pool of creative<br />\r\ndevelopers, designers, solution consultants and marketing professionals.</p>','2019-12-27 05:33:28','2020-02-10 04:02:04','global',NULL,'editor'),
(47,'site_about_excellence_title','Our Vision','2019-12-27 06:24:21','2020-02-10 04:34:57','global',NULL,'text'),
(48,'site_about_excellence_description','<p>We want to be the go-to partner for small and medium businesses for their digital<br />\r\nambitions irrespective of geographies.<br />\r\nWe want to make technology the best friend of a modern business by building simple and<br />\r\nscalable solutions to power their business needs.<br />\r\nWe want to enable innovation and sustained growth for our clients by helping them build<br />\r\nnew digital platforms to engage their customers.<br />\r\nWe want to be recognized as the best place to work for talented software engineers,<br />\r\ncreative UI/UX professionals and technology enthusiasts.</p>','2019-12-27 06:25:04','2020-02-10 04:35:18','global',NULL,'editor'),
(49,'site_about_core_values_title','<h2>The Citrusbug Advantage</h2>','2019-12-27 06:25:48','2020-02-10 04:55:40','global',NULL,'editor'),
(50,'site_about_core_values_description','<p>We want to be the go-to partner for small and medium businesses for their digital<br />\r\nambitions irrespective of geographies.<br />\r\nWe want to make technology the best friend of a modern business by building simple and<br />\r\nscalable solutions to power their business needs.<br />\r\nWe want to enable innovation and sustained growth for our clients by helping them build<br />\r\nnew digital platforms to engage their customers.<br />\r\nWe want to be recognized as the best place to work for talented software engineers,<br />\r\ncreative UI/UX professionals and technology enthusiasts.</p>\r\n\r\n<p>&nbsp;</p>','2019-12-27 06:26:29','2020-02-10 05:08:47','global',NULL,'editor'),
(51,'site_about_creative_agencies_title','<h2>Our Core Values</h2>','2019-12-27 06:27:52','2020-02-10 05:03:43','global',NULL,'editor'),
(52,'site_about_creative_agencies_description','<p>CitrusBug has its foundation strong and built on 3 key pillars which are</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Integrity</strong><br />\r\nBeing truthful to our customers, employees and partners and adhering to expectations</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Leadership</strong><br />\r\nGuiding clients and employees to achieve an enviable journey of growth in technology<br />\r\nsupremacy</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Innovation</strong><br />\r\nConstantly in pursuit of possibilities that can re-imagine business for our customers</p>','2019-12-27 06:28:46','2020-02-10 05:08:23','global',NULL,'editor'),
(53,'site_social_links','<h3>Social links</h3>\r\n                    <ul>\r\n                        <li><a href=\"#\">Facebook</a></li>\r\n                        <li><a href=\"#\">Dribble</a></li>\r\n                        <li><a href=\"#\">Dribble</a></li>\r\n                    </ul>','2019-12-30 02:28:55','2019-12-30 02:28:55','global','Social Links','textarea'),
(54,'site_careers_baner_image','/storage/uploads/Hero_Careers-4.jpg','2019-12-30 08:03:18','2019-12-30 08:26:10','global','Careers Page baner image','image'),
(55,'site_home_technology_title','Technology we Expert','2020-01-07 01:02:49','2020-01-07 01:02:49','global',NULL,'text'),
(56,'site_home_technology_desc','since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,','2020-01-07 01:03:25','2020-01-07 01:03:25','global',NULL,'text'),
(57,'footer_contact_info','<ul>\r\n                                           <li><a href=\"skype:19739476185\" class=\"contact-icon\">\r\n                                               <span class=\"icon-svg\"><img src=\"/assets/images/icons/skype.svg\" alt=\"\"></span>\r\n                                                <span class=\"contact-icon-slide\"><p>thecitrubug.info</p></span></a>\r\n                                            </li>\r\n                                            <li><a href=\"mailto:hello@company.com\" class=\"contact-icon\">\r\n                                                <span class=\"icon-svg\"><img src=\"/assets/images/icons/email.svg\" alt=\"\"></span>\r\n                                                 <span class=\"contact-icon-slide\"><p>thecitrubug.info</p></span></a>\r\n                                             </li>\r\n                                        </ul>','2020-01-23 00:19:14','2020-01-30 00:11:19','global',NULL,'textarea'),
(58,'footer_copy_info','<p><span>© 2020 CITRUSBUG.</span> All rights reserved</p>','2020-01-23 00:20:34','2020-01-23 00:20:34','global','<p><span>© 2020 CITRUSBUG.</span> All rights reserved</p>','textarea'),
(59,'footer_social_links','<ul>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-facebook\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-instagram\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-twitter\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-linkedin\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-behance\"></span></a></li>\r\n                                </ul>','2020-01-23 00:21:22','2020-01-23 00:21:22','global','<ul>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-facebook\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-instagram\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-twitter\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-linkedin\"></span></a></li>\r\n                                    <li><a href=\"#\"><span class=\"icon-svg icon-behance\"></span></a></li>\r\n                                </ul>','textarea'),
(60,'site_home_project_last_slide_text','<h2>What we<span>\'</span>re looking forword next</h2>','2020-02-12 04:23:03','2020-02-12 04:23:03','global',NULL,'textarea'),
(61,'site_home_hire_text','Forget about project complexity and hire an expert web developer to help you accomplish your business projects today. No matter you are searching custom web developer or want to hire web developer to build a website from scratch, we have ardent staff, who deliver excellent work with super quality & perfection. If you want to be on the top, get equipped with an army of web developers provided by Citrusbug Technolabs.','2020-02-12 04:26:49','2020-02-12 04:26:49','global',NULL,'textarea'),
(62,'google_analytics','<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-54524286-1\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n  gtag(\'config\', \'UA-54524286-1\');\r\n</script>','2020-02-12 10:03:12','2020-02-12 10:03:12','global',NULL,'textarea');

/*Table structure for table `students` */

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `students` */

/*Table structure for table `team` */

DROP TABLE IF EXISTS `team`;

CREATE TABLE `team` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `team` */

insert  into `team`(`id`,`name`,`designation`,`created_at`,`updated_at`,`deleted_at`,`ordering`) values 
(1,'Ishan Vyas','<h4> <span class=\"font-italic the-class span-block\">The</span> CEO </h4>','2019-12-25 22:04:02','2020-01-30 00:26:51',NULL,1),
(2,'Harsh Shah','<h4> <span class=\"font-italic the-class span-block\">The</span> Founder </h4>','2020-01-30 00:24:52','2020-01-30 00:26:53',NULL,2),
(3,'Karmrajsinh vaghela','<h4> <span class=\"font-italic the-class span-block\">The</span> CTO </h4>','2020-01-30 00:25:56','2020-01-30 00:26:54',NULL,3);

/*Table structure for table `technology` */

DROP TABLE IF EXISTS `technology`;

CREATE TABLE `technology` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `featured` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `param` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `technology_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `technology` */

insert  into `technology`(`id`,`title`,`slug`,`icon`,`desc`,`description`,`created_at`,`updated_at`,`deleted_at`,`ordering`,`featured`,`param`,`status`) values 
(1,'Node','node','/storage/uploads/icons/technologies/angular.svg','<p>Our Node.js expertise has been utilized by leading SMB&rsquo;s and startups to build their aspiring digital dreams. Our talented pool of Node.js developers can work with customers and handle end-to-end enterprise application development from planning, design, development and integrations with other business systems through API&rsquo;s, serverless architecture or microservices implementation.</p>','<h2><a href=\"#\">Node</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>Our Node.js expertise has been utilized by leading SMB&rsquo;s and startups to build their aspiring digital dreams. Our talented pool of Node.js developers can work with customers and handle end-to-end enterprise application development from planning, design, development and integrations with other business systems through API&rsquo;s, serverless architecture or microservices implementation.</p>','2019-12-25 20:19:47','2020-02-11 07:29:45',NULL,3,'on','a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',NULL),
(2,'Python','python','/storage/uploads/icons/technologies/php.svg','<p>Build the next generation of your technology solutions with our enterprise application development services using Python. From simple web forms to complex enterprise wide digital platforms having AI enabled capabilities, our Python consultants lead you in every step of the way from design to deployment while you get to focus on your core business activities.</p>','<h2>We are a digital agency that specializes in User Experience Design</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at lacinia lacus, at malesuada magna. Nam vitae urna in nisi pellentesque tempor. Praesent consequat augue non nunc volutpat ullamcorper. Suspendisse ultricies quam in mauris tincidunt, ac pulvinar felis commodo. Donec varius, nibh id luctus molestie, urna est cursus massa, molestie tempus urna ante sit amet dolor. Nunc vestibulum nisi vitae purus venenatis pulvinar. Nunc magna tortor, tempor ut quam at, luctus blandit magna.</p>','2019-12-25 20:21:07','2020-02-11 03:30:03',NULL,1,'on','a:3:{s:8:\"tag_line\";s:107:\"Build the next generation of your technology solutions with our enterprise application development services\";s:8:\"services\";s:187:\"Custom Python Development\r\nPython Machine Learning\r\nDjango\r\nFlask \r\nPython Enterprise Solutions\r\nPython Upgradation and Migration Services\r\nPython Hybrid Programming\r\nPython with Big Data\";s:8:\"hiretext\";s:341:\"Our expert team has rich experience in developing custom web and desktop applications in Python and Django. Being one of the leading Python Web Development Company in India & USA our experts have delivered 100+ web applications. Hire top Python Developers who are masters in working with Python 3.7.0 and Django, Flask and Web2py frameworks.\";}',NULL),
(3,'PHP','php','/storage/uploads/icons/technologies/ios.svg','<p>We partner with businesses across the globe who want to build modern customer facing web applications using PHP. From solution consulting to implementing framework-based client-server application interfaces and database connectivity, our PHP consultants enable faster roll-out of your web portals and enterprise applications with a scalable and highly extensible technology infrastructure.</p>','<h2><a href=\"#\">PHP</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at lacinia lacus, at malesuada magna. Nam vitae urna in nisi pellentesque tempor. Praesent consequat augue non nunc volutpat ullamcorper. Suspendisse ultricies quam in mauris tincidunt, ac pulvinar felis commodo. Donec varius, nibh id luctus molestie, urna est cursus massa, molestie tempus urna ante sit amet dolor. Nunc vestibulum nisi vitae purus venenatis pulvinar. Nunc magna tortor, tempor ut quam at, luctus blandit magna.</p>','2019-12-25 20:21:38','2020-02-12 08:17:58',NULL,5,'on','a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',NULL),
(4,'Angular','angular','/storage/uploads/icons/technologies/ios.svg','<p>Our Angular expertise allows business across industries engineer their front-end customer facing progressive web applications that are scalable, imbibes high end usability and above all enables seamless scalability and robustness across a range of consumer devices.</p>','<h2><a href=\"#\">Angular</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>Our Angular expertise allows business across industries engineer their front-end customer facing progressive web applications that are scalable, imbibes high end usability and above all enables seamless scalability and robustness across a range of consumer devices.</p>','2020-01-04 01:01:16','2020-02-12 08:18:10',NULL,6,'on','a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',NULL),
(5,'React Native','react-native','/storage/uploads/icons/technologies/ios.svg','<p>Our professional team of React Native developers help businesses create seamless cross-platform mobile apps that enable them to deliver rewarding experiences to customers on their favorite devices.</p>','<h2><a href=\"#\">React Native</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at lacinia lacus, at malesuada magna. Nam vitae urna in nisi pellentesque tempor. Praesent consequat augue non nunc volutpat ullamcorper. Suspendisse ultricies quam in mauris tincidunt, ac pulvinar felis commodo. Donec varius, nibh id luctus molestie, urna est cursus massa, molestie tempus urna ante sit amet dolor. Nunc vestibulum nisi vitae purus venenatis pulvinar. Nunc magna tortor, tempor ut quam at, luctus blandit magna.</p>','2020-01-07 00:56:58','2020-02-12 08:18:17',NULL,4,'on','a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',NULL),
(6,'React','react','/storage/uploads/icons/technologies/php.svg','<p>We help power digital ambitions of several SMB&rsquo;s and startups with rich internet applications developed with React. With a structured framework-oriented development principle, our skilled React.JS resources can help deliver powerful web applications to meet dynamic needs of today&rsquo;s highly competitive market.</p>','<h2><a href=\"#\">React</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>We help power digital ambitions of several SMB&rsquo;s and startups with rich internet applications developed with React. With a structured framework-oriented development principle, our skilled React.JS resources can help deliver powerful web applications to meet dynamic needs of today&rsquo;s highly competitive market.</p>','2020-01-07 00:59:12','2020-02-12 08:18:30',NULL,2,'on','a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',NULL),
(7,'ML & AI','ml-ai','/storage/uploads/icons/technologies/angular.svg','<p>Our AI &amp; ML practice empower organizations to propel their digital applications into the next level by making them autonomous and with self-learning features for heightened customer experiences. Our consultants help SMB&rsquo;s embrace the power of NLP, intelligent recommendations and predictive analytics to help bolster growth in customer loyalty like they have never witnessed before.</p>','<h2><a href=\"#\">ML &amp; AI</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>Our AI &amp; ML practice empower organizations to propel their digital applications into the next level by making them autonomous and with self-learning features for heightened customer experiences. Our consultants help SMB&rsquo;s embrace the power of NLP, intelligent recommendations and predictive analytics to help bolster growth in customer loyalty like they have never witnessed before.</p>','2020-01-22 23:19:58','2020-02-12 08:18:41',NULL,7,'on','a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',NULL),
(8,'Cloud & DevOps','cloud-devops','/storage/uploads/icons/technologies/ios.svg','<p>Our state-of-the art cloud &amp;devops services empower organizaitons to quickly move from infrastrcuture and cost heavy on-premise IT to a seamlessly scalable and flexible cloud based digital technology backbone. Our consultants assure constant availability, faster time to market and smooth deployment of cloud based applications for your every need.</p>','<h2><a href=\"#\">Cloud &amp; DevOps</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>Our state-of-the art cloud &amp;devops services empower organizaitons to quickly move from infrastrcuture and cost heavy on-premise IT to a seamlessly scalable and flexible cloud based digital technology backbone. Our consultants assure constant availability, faster time to market and smooth deployment of cloud based applications for your every need.</p>','2020-01-22 23:21:08','2020-02-12 08:18:47',NULL,8,'on','a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',NULL),
(9,'HTML','html',NULL,NULL,NULL,'2020-02-10 06:54:37','2020-02-10 06:54:37',NULL,NULL,NULL,NULL,NULL),
(10,'JS','js',NULL,NULL,NULL,'2020-02-10 06:55:48','2020-02-10 06:55:48',NULL,NULL,NULL,NULL,NULL),
(11,'jQuery','jquery',NULL,NULL,NULL,'2020-02-10 06:56:20','2020-02-10 06:56:20',NULL,NULL,NULL,NULL,NULL),
(12,'CSS','css',NULL,NULL,NULL,'2020-02-10 06:56:47','2020-02-10 06:56:47',NULL,NULL,NULL,NULL,NULL),
(13,'Code Igniter','code-igniter',NULL,NULL,NULL,'2020-02-10 06:57:11','2020-02-10 06:57:11',NULL,NULL,NULL,NULL,NULL),
(14,'Wordpress','wordpress',NULL,NULL,NULL,'2020-02-11 23:49:19','2020-02-11 23:49:19',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(15,'Magento','magento',NULL,NULL,NULL,'2020-02-11 23:50:29','2020-02-11 23:50:29',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(16,'AWS','aws',NULL,NULL,NULL,'2020-02-11 23:50:42','2020-02-11 23:50:42',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(17,'IOS','ios',NULL,NULL,NULL,'2020-02-11 23:50:51','2020-02-11 23:50:51',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(18,'REST API','rest-api',NULL,NULL,NULL,'2020-02-11 23:51:38','2020-02-11 23:51:38',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(19,'Flask','flask',NULL,NULL,NULL,'2020-02-11 23:56:27','2020-02-11 23:56:27',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(20,'PyCharm','pycharm',NULL,NULL,NULL,'2020-02-11 23:56:35','2020-02-11 23:56:35',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(21,'Jupyter Notebook','jupyter-notebook',NULL,NULL,NULL,'2020-02-11 23:56:59','2020-02-11 23:56:59',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(22,'PowerShell','powershell',NULL,NULL,NULL,'2020-02-11 23:57:12','2020-02-11 23:57:12',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(23,'Mercurial','mercurial',NULL,NULL,NULL,'2020-02-11 23:57:29','2020-02-11 23:57:29',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(24,'d3.js','d3js',NULL,NULL,NULL,'2020-02-11 23:57:41','2020-02-11 23:57:41',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(25,'Jinja2','jinja2',NULL,NULL,NULL,'2020-02-11 23:58:00','2020-02-11 23:58:00',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(26,'Material-UI','material-ui',NULL,NULL,NULL,'2020-02-11 23:58:18','2020-02-11 23:58:18',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(27,'React Bootstrap','react-bootstrap',NULL,NULL,NULL,'2020-02-11 23:58:34','2020-02-11 23:58:34',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(28,'Blueprint','blueprint',NULL,NULL,NULL,'2020-02-11 23:58:50','2020-02-11 23:58:50',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(29,'Semantic UI React','semantic-ui-react',NULL,NULL,NULL,'2020-02-11 23:59:09','2020-02-11 23:59:09',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(30,'React Toolbox','react-toolbox',NULL,NULL,NULL,'2020-02-11 23:59:30','2020-02-11 23:59:31',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(31,'React Desktop','react-desktop',NULL,NULL,NULL,'2020-02-11 23:59:41','2020-02-11 23:59:41',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(32,'Evergreen','evergreen',NULL,NULL,NULL,'2020-02-11 23:59:47','2020-02-11 23:59:47',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(33,'Elemental UI','elemental-ui',NULL,NULL,NULL,'2020-02-12 00:00:02','2020-02-12 00:00:02',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(34,'KendoReact','kendoreact',NULL,NULL,NULL,'2020-02-12 00:00:18','2020-02-12 00:00:18',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(35,'Redux','redux',NULL,NULL,NULL,'2020-02-12 00:00:19','2020-02-12 00:00:19',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(36,'JSX & ReactJS','jsx-reactjs',NULL,NULL,NULL,'2020-02-12 00:00:32','2020-02-12 00:00:32',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(37,'NativeBase','nativebase',NULL,NULL,NULL,'2020-02-12 00:01:15','2020-02-12 00:01:15',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(38,'React Native Elements','react-native-elements',NULL,NULL,NULL,'2020-02-12 00:01:19','2020-02-12 00:01:19',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(39,'Shoutem','shoutem',NULL,NULL,NULL,'2020-02-12 00:01:28','2020-02-12 00:01:28',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(40,'UI Kitten','ui-kitten',NULL,NULL,NULL,'2020-02-12 00:01:38','2020-02-12 00:01:38',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(41,'React Native Material UI','react-native-material-ui',NULL,NULL,NULL,'2020-02-12 00:01:43','2020-02-12 00:01:43',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(42,'React Native Material Kit','react-native-material-kit',NULL,NULL,NULL,'2020-02-12 00:01:58','2020-02-12 00:01:58',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(43,'Nachos UI','nachos-ui',NULL,NULL,NULL,'2020-02-12 00:02:07','2020-02-12 00:02:07',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(44,'React Native UI Library','react-native-ui-library',NULL,NULL,NULL,'2020-02-12 00:02:15','2020-02-12 00:02:15',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(45,'React Native Vector Icons','react-native-vector-icons',NULL,NULL,NULL,'2020-02-12 00:02:19','2020-02-12 00:02:19',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(46,'Teaset','teaset',NULL,NULL,NULL,'2020-02-12 00:02:25','2020-02-12 00:02:25',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(47,'Lottie for React Native','lottie-for-react-native',NULL,NULL,NULL,'2020-02-12 00:02:33','2020-02-12 00:02:33',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(48,'Ignite CLI','ignite-cli',NULL,NULL,NULL,'2020-02-12 00:02:41','2020-02-12 00:02:41',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(49,'React Native Mapview','react-native-mapview',NULL,NULL,NULL,'2020-02-12 00:02:49','2020-02-12 00:02:49',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(50,'React Native UI Kitten','react-native-ui-kitten',NULL,NULL,NULL,'2020-02-12 00:02:59','2020-02-12 00:03:00',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(51,'RNUILIB','rnuilib',NULL,NULL,NULL,'2020-02-12 00:03:05','2020-02-12 00:03:05',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(52,'AdonisJs','adonisjs',NULL,NULL,NULL,'2020-02-12 00:03:18','2020-02-12 00:03:18',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(53,'Express.js ','expressjs',NULL,NULL,NULL,'2020-02-12 00:03:23','2020-02-12 00:03:23',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(54,'Meteor.js','meteorjs',NULL,NULL,NULL,'2020-02-12 00:03:34','2020-02-12 00:03:34',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(55,'Nest.js','nestjs',NULL,NULL,NULL,'2020-02-12 00:03:38','2020-02-12 00:03:38',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(56,'Sails.js','sailsjs',NULL,NULL,NULL,'2020-02-12 00:03:49','2020-02-12 00:03:49',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(57,'Koa.js','koajs',NULL,NULL,NULL,'2020-02-12 00:03:55','2020-02-12 00:03:55',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(58,'LoopBack.js ','loopbackjs',NULL,NULL,NULL,'2020-02-12 00:04:04','2020-02-12 00:04:04',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(59,'Hapi.js ','hapijs',NULL,NULL,NULL,'2020-02-12 00:04:10','2020-02-12 00:04:10',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(60,'Derby.js','derbyjs',NULL,NULL,NULL,'2020-02-12 00:04:17','2020-02-12 00:04:17',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(61,'Total.js','totaljs',NULL,NULL,NULL,'2020-02-12 00:04:25','2020-02-12 00:28:49',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(62,'Socket.io','socketio',NULL,NULL,NULL,'2020-02-12 00:04:28','2020-02-12 00:04:28',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(63,'Mean.io','meanio',NULL,NULL,NULL,'2020-02-12 00:04:36','2020-02-12 00:04:36',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(64,'Laravel','laravel',NULL,NULL,NULL,'2020-02-12 00:04:58','2020-02-12 00:04:58',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(65,'Symfony','symfony',NULL,NULL,NULL,'2020-02-12 00:05:09','2020-02-12 00:05:09',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(66,'CakePHP','cakephp',NULL,NULL,NULL,'2020-02-12 00:05:16','2020-02-12 00:05:17',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(67,'CodeIgniter','codeigniter',NULL,NULL,NULL,'2020-02-12 00:05:19','2020-02-12 00:05:19',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(68,'Yii','yii',NULL,NULL,NULL,'2020-02-12 00:05:28','2020-02-12 00:05:28',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(69,'Zend Framework','zend-framework',NULL,NULL,NULL,'2020-02-12 00:05:35','2020-02-12 00:05:35',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(70,'Phalcon','phalcon',NULL,NULL,NULL,'2020-02-12 00:05:40','2020-02-12 00:05:40',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(71,'FuelPHP','fuelphp',NULL,NULL,NULL,'2020-02-12 00:05:47','2020-02-12 00:05:47',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(72,'PHPixie','phpixie',NULL,NULL,NULL,'2020-02-12 00:05:57','2020-02-12 00:05:57',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(73,'Slim','slim',NULL,NULL,NULL,'2020-02-12 00:06:05','2020-02-12 00:06:05',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(74,'material start','material-start',NULL,NULL,NULL,'2020-02-12 00:06:37','2020-02-12 00:06:37',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(75,'angular-cli','angular-cli',NULL,NULL,NULL,'2020-02-12 00:06:46','2020-02-12 00:06:46',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(76,'angular-oauth2','angular-oauth2',NULL,NULL,NULL,'2020-02-12 00:06:57','2020-02-12 00:06:57',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(77,'angular-data-table','angular-data-table',NULL,NULL,NULL,'2020-02-12 00:07:03','2020-02-12 00:07:03',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(78,'gladys','gladys',NULL,NULL,NULL,'2020-02-12 00:07:18','2020-02-12 00:07:19',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(79,'angular-es6','angular-es6',NULL,NULL,NULL,'2020-02-12 00:07:24','2020-02-12 00:07:24',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(80,'angular-virtual-dom','angular-virtual-dom',NULL,NULL,NULL,'2020-02-12 00:07:34','2020-02-12 00:07:34',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(81,'quantumui','quantumui',NULL,NULL,NULL,'2020-02-12 00:07:42','2020-02-12 00:07:42',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(82,'ng meditor','ng-meditor',NULL,NULL,NULL,'2020-02-12 00:07:54','2020-02-12 00:07:54',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(83,'Tensorflow','tensorflow',NULL,NULL,NULL,'2020-02-12 00:07:59','2020-02-12 00:07:59',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(84,'Theano','theano',NULL,NULL,NULL,'2020-02-12 00:08:07','2020-02-12 00:08:07',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(85,'Matplotlib','matplotlib',NULL,NULL,NULL,'2020-02-12 00:08:15','2020-02-12 00:08:15',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(86,'Seaborn','seaborn',NULL,NULL,NULL,'2020-02-12 00:08:26','2020-02-12 00:08:26',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(87,'Scikit-learn','scikit-learn',NULL,NULL,NULL,'2020-02-12 00:08:34','2020-02-12 00:08:34',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(88,'PyTorch','pytorch',NULL,NULL,NULL,'2020-02-12 00:08:52','2020-02-12 00:08:52',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(89,'Keras','keras',NULL,NULL,NULL,'2020-02-12 00:09:03','2020-02-12 00:09:03',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(90,'Pandas','pandas',NULL,NULL,NULL,'2020-02-12 00:09:11','2020-02-12 00:09:11',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(91,'Kamatera','kamatera',NULL,NULL,NULL,'2020-02-12 00:09:20','2020-02-12 00:09:20',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(92,'Buddy','buddy',NULL,NULL,NULL,'2020-02-12 00:09:28','2020-02-12 00:09:28',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(93,'Basis Technologies','basis-technologies',NULL,NULL,NULL,'2020-02-12 00:09:40','2020-02-12 00:09:40',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(94,'TestRail','testrail',NULL,NULL,NULL,'2020-02-12 00:09:52','2020-02-12 00:09:52',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(95,'CloudHealth','cloudhealth',NULL,NULL,NULL,'2020-02-12 00:10:02','2020-02-12 00:10:03',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(96,'QuerySurge','querysurge',NULL,NULL,NULL,'2020-02-12 00:10:09','2020-02-12 00:10:09',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(97,'Docker','docker',NULL,NULL,NULL,'2020-02-12 00:10:15','2020-02-12 00:10:41',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(98,'Vagrant','vagrant',NULL,NULL,NULL,'2020-02-12 00:10:25','2020-02-12 00:10:25',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(99,'PagerDuty','pagerduty',NULL,NULL,NULL,'2020-02-12 00:10:30','2020-02-12 00:10:30',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(100,'Tackify Retrace','tackify-retrace',NULL,NULL,NULL,'2020-02-12 00:10:53','2020-02-12 00:10:53',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(101,'Monit','monit',NULL,NULL,NULL,'2020-02-12 00:10:58','2020-02-12 00:10:59',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(102,'Graylog','graylog',NULL,NULL,NULL,'2020-02-12 00:11:09','2020-02-12 00:11:09',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(103,'UpGuard','upguard',NULL,NULL,NULL,'2020-02-12 00:11:12','2020-02-12 00:11:12',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(104,'GULP','gulp',NULL,NULL,NULL,'2020-02-12 00:14:57','2020-02-12 00:14:57',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(105,'Adobe Photosuite','adobe-photosuite',NULL,NULL,NULL,'2020-02-12 00:15:08','2020-02-12 00:15:08',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(106,'MySQL','mysql',NULL,NULL,NULL,'2020-02-12 00:17:54','2020-02-12 00:17:54',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(107,'Woo-commerce','woo-commerce',NULL,NULL,NULL,'2020-02-12 00:18:16','2020-02-12 00:18:16',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(108,'ROR','ror',NULL,NULL,NULL,'2020-02-12 00:31:09','2020-02-12 00:31:09',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(109,'Payment API','payment-api',NULL,NULL,NULL,'2020-02-12 00:31:28','2020-02-12 00:31:28',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1),
(110,'Adyen API','adyen-api',NULL,NULL,NULL,'2020-02-12 00:31:39','2020-02-12 00:31:39',NULL,NULL,NULL,'a:3:{s:8:\"tag_line\";N;s:8:\"services\";N;s:8:\"hiretext\";N;}',1);

/*Table structure for table `testimonials` */

DROP TABLE IF EXISTS `testimonials`;

CREATE TABLE `testimonials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` mediumtext COLLATE utf8mb4_unicode_ci,
  `title` mediumtext COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testimonials` */

insert  into `testimonials`(`id`,`name`,`position`,`title`,`description`,`created_at`,`updated_at`,`deleted_at`,`ordering`) values 
(1,'World\'s Largest | Wine Community','worlds-largest-wine-community-1','The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn\'t find the right beige from a set of 200 and had to design a new beige. He seemed like the ultimate perfectionist – sometimes.','<h2>Our&nbsp;<a href=\"#\">challenges</a>and&nbsp;<a href=\"#\">solutions</a></h2>\r\n\r\n<p>We are a digital agency that specializes in User Experience Design</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at lacinia lacus, at malesuada magna. Nam vitae urna in nisi pellentesque tempor. Praesent consequat augue non nunc volutpat ullamcorper. Suspendisse ultricies quam in mauris tincidunt, ac pulvinar felis commodo. Donec varius, nibh id luctus molestie, urna est cursus massa, molestie tempus urna ante sit amet dolor. Nunc vestibulum nisi vitae purus venenatis pulvinar. Nunc magna tortor, tempor ut quam at, luctus blandit magna.</p>','2019-12-23 23:30:46','2020-01-29 08:20:37','2020-01-29 08:20:37',NULL),
(2,'The | Best Coffee | You\'ve Ever Made','the-best-coffee-youve-ever-made-1','The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn\'t find the right beige from a set of 200 and had to design a new beige. He seemed like the ultimate perfectionist – sometimes.','<p>a</p>','2019-12-23 23:31:36','2020-01-29 08:20:38','2020-01-29 08:20:38',NULL),
(3,'Dummy Project','dummy-project','Dummy Project Dummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy ProjectDummy Project',NULL,'2019-12-24 00:57:22','2020-01-23 07:54:59','2020-01-23 07:54:59',NULL),
(4,'Shubham Vandikar','Citrusbug technolabs','<span>What</span> our customer says !','<p>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet,&nbsp;consectetur&nbsp;adipiscing&nbsp;elit.&nbsp;Etiam&nbsp;augue&nbsp;lectus,&nbsp;tempus&nbsp;eu&nbsp;dolor&nbsp;id,&nbsp;ultricies&nbsp;maximus justo.&nbsp;Nullam&nbsp;vitae&nbsp;auctor&nbsp;turpis,&nbsp;efficitur&nbsp;facilisis&nbsp;sapien.&nbsp;Etiam&nbsp;risus&nbsp;diam,&nbsp;bibendum&nbsp;eget&nbsp;sagittis&nbsp;vel,&nbsp;semper sit&nbsp;amet&nbsp;arcu.&nbsp;Proin&nbsp;volutpat&nbsp;purus&nbsp;nec&nbsp;orci&nbsp;porttitor&nbsp;tempus.&nbsp;Fusce&nbsp;eget&nbsp;varius&nbsp;tellus.</p>\r\n\r\n<p>tor&nbsp;turpis,&nbsp;efficitur&nbsp;facilisis&nbsp;sapien.&nbsp;Etiam&nbsp;risus&nbsp;diam,&nbsp;bibendum&nbsp;eget&nbsp;sagittis&nbsp;vel,&nbsp;sempersit&nbsp;amet&nbsp;arcu.&nbsp;Proin&nbsp;volutpat&nbsp;purus&nbsp;nec&nbsp;orci&nbsp;porttitor&nbsp;tempus.&nbsp;Fusce&nbsp;eget&nbsp;varius&nbsp;tellus.</p>','2020-01-29 08:22:36','2020-01-29 08:22:36',NULL,NULL),
(5,'Ishan Vyash','Citrusbug Technolabs','<span>What</span> our customer says !','<p>We make technology simple for businesses across industries and geographies and accelerate their digital journey cost-effectively.&nbsp;We make technology simple for businesses across industries and geographies and accelerate their digital journey cost-effectively.</p>\r\n\r\n<p>We make technology simple for businesses across industries and geographies and accelerate their digital journey cost-effectively.</p>','2020-01-29 08:25:03','2020-01-29 08:25:03',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`status`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Super Admin','superadmin@gmail.com','2019-12-19 09:16:21','$2y$10$yH9Y56rBpvz3YNQ1S5aTC.xk18rv6nQgBH.Q1tY4HDcxm0VjuX0Qe',NULL,NULL,'2019-12-19 09:16:21','2019-12-19 09:16:21',NULL),
(2,'malkesh','themalkesh@gmail.com',NULL,'123456',NULL,1,'2020-01-29 04:27:04','2020-01-29 04:27:04',NULL),
(3,'ishan','vyas.ishan@citrusbug.com',NULL,'123456',NULL,1,'2020-01-29 04:28:02','2020-01-29 04:28:02',NULL);

/*Table structure for table `users_rolls` */

DROP TABLE IF EXISTS `users_rolls`;

CREATE TABLE `users_rolls` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `roll_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_rolls_roll_id_user_id_index` (`roll_id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

/*Data for the table `users_rolls` */

insert  into `users_rolls`(`id`,`roll_id`,`user_id`,`created_at`,`updated_at`) values 
(1,1,1,'2019-12-19 09:16:21','2019-12-19 09:16:21'),
(2,1,2,NULL,NULL),
(3,2,2,NULL,NULL),
(4,3,2,NULL,NULL),
(5,4,2,NULL,NULL),
(6,1,3,NULL,NULL),
(7,2,3,NULL,NULL),
(8,3,3,NULL,NULL),
(9,4,3,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
