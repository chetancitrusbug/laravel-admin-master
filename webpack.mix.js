const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');


mix.copyDirectory('resources/app-assets', 'public/app-assets');

 
mix.sass('resources/app-assets/css/custom.scss', 'resources/app-assets/css/custom.css')

mix.styles([
    'public/app-assets/fonts/feather/style.min.css',  
    'public/app-assets/fonts/simple-line-icons/style.css',  
    'public/app-assets/fonts/font-awesome/css/font-awesome.min.css',  

    'public/app-assets/vendors/css/*.css',  

    'public/app-assets/css/app.css',
    'public/app-assets/lightbox/css/lightbox.css',
    'public/app-assets/css/custom.css'

], 'public/css/admin/admin.css');

 
mix.scripts([
    'public/app-assets/vendors/js/core/jquery-3.2.1.min.js',  
    'public/app-assets/vendors/js/core/popper.min.js',  
    'public/app-assets/vendors/js/core/bootstrap.min.js',  
    'public/app-assets/vendors/js/perfect-scrollbar.jquery.min.js',  
    'public/app-assets/vendors/js/prism.min.js',  
    'public/app-assets/vendors/js/jquery.matchHeight-min.js',  
    'public/app-assets/vendors/js/screenfull.min.js',  
    'public/app-assets/vendors/js/pace/pace.min.js', 
    'public/app-assets/vendors/js/chartist.min.js',  
    'public/app-assets/js/app-sidebar.js',  
    'public/app-assets/js/notification-sidebar.js',  
    'public/app-assets/js/customizer.js',  
    'public/app-assets/lightbox/js/lightbox.js' 
    
], 'public/js/admin/admin.js');



//Front-End

mix.copyDirectory('resources/assets/fonts', 'public/assets/fonts');
mix.copyDirectory('resources/assets/images', 'public/assets/images');

mix.sass('resources/assets/css/style.scss', 'public/css/site/style.css')
mix.sass('resources/assets/css/custom.scss', 'public/css/site/custom.css')
mix.sass('resources/assets/css/animation.scss', 'public/css/site/animation.css')
 
mix.styles([
    'resources/assets/css/owl.carousel.min.css',
    'resources/assets/css/owl.theme.default.css',
    'public/css/site/style.css',
    'public/css/site/custom.css', 
    'public/css/site/animation.css' 
], 'public/css/site/site.css'); 


mix.scripts([
    'resources/assets/js/owl.carousel.js',
    'resources/assets/js/custom.js',
    'resources/assets/js/animation.js' 
], 'public/js/site/custom.js');