<tr>
    <td class="header">
        <div class="header_container">
            <a href="{{ $url }}">
                <div class="logo-div">
                    <span class="logo_link clearfix" href="{{ route('site.home') }}">
                        <img src="{{ asset('assets/images/logo.svg') }}" class="img-fluid logo_img main-logo"
                            alt="logo" />
                    </span>
                </div>
                {{-- <div class="site_title">
                    {{ $slot }}
                </div> --}}
            </a>
        </div>
    </td>
</tr>