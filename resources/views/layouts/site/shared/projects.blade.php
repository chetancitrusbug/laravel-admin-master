<div id="pinContainer" class="project-container-main">
    <div class="project-menu">
        <div class="img-menu">
            <img src="{{asset('assets/images/briefcase.png')}}" alt="" /> 
        </div>
        {{-- <ul>
            @foreach ($projects as $key=>$item)
            <li><a href="#project-{{$key+1}}" class="active">0</a></li>
            @endforeach
             
        </ul> --}}
    </div>
    <div id="slideContainer" class="slide-conatiner owl-carousel">
        <section class="panel project-one" id="project-one">
            <div class="conatiner-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="project-content-one container-lg-2">

                            <h2>{!! getSession('site_home_project_title', '') !!} </h2>
                            {!! getSession('site_home_project_desc') !!}

                            {{-- <h2>A small selection
                                <span class="span-block">of our work, enjoy.</span></h2>

                            <p>We are a digital agency that specializes in User Experience Design</p> --}}

                            <div class="project-img">
                                <img src="{{asset('assets/images/project.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                     
                </div>
            </div>
            
        </section>
        @foreach ($projects as $key=>$item) 
        @php 
        if ($key > 7){
            continue;
        }
        @endphp
        <section class="panel project-two project-{{$key+1}}" id="project-{{$key+1}}">
            <div class="squre-shape">

            </div>
            <div class="conatiner-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="project-content-two container-lg-2">
                            <h2>
                                {{$item->title}}
                            </h2>

                            <div class="sub-heading">
                                <div class="row">
                                    <div class="col-lg-6 br-1">
                                        <h3>Technology we used</h3>
                                        @foreach($item->technologies as $tech)
                                        <a class="{{ route('site.technology') }}">
                                            {{ $tech->title }}
                                        </a>,
                                        @endforeach
                                    </div>
                                    <div class="col-lg-6">
                                        {{$item->desc}}
                                    </div>
                                </div>
                            </div>
                           
                            <div class="project-img">
                                <img src="{!!  @$item->projects_single_image->first()->file_thumb_url  !!}" alt="">
                            </div>

                            <div class="btn-case-study button-common-div">
                          
                                    {{-- <a href="#" class="btn btn-common btn-case"> 
                                        <span class="transform-text">View case study</span> <span class="arrow-right-circle"></span> 
                                    </a> --}}

                                    <a href="{{route('site.work',['slug'=>$item->slug])}}" class="btn btn-common btn-white"> 
                                        <span class="btn-background"></span>

                                        <span class="transform-text">View case study</span> 

                                        <span class="arrow-right-circle"></span> 
                                    </a>
                            
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
            
        </section>
        @endforeach
        <section class="panel project-five"  id="project-five">
            <div class="conatiner-fluid">
                <div class="row">
                
                    <div class="col-lg-12">
                        <div class="project-content-five ">
                           
                          
                            <div class="project-img">
                                <img src="{{asset('assets/images/view-all-project.png')}}" class="" alt="">
                            </div>
                            <div class="view-all-section">
                                <div class=" view-all-project">

                                    {!! getSession('site_home_project_last_slide_text')  !!}
                                    
                                    {{-- <div class="btn-case-study">
                                        
                                            <a href="#" class="btn btn-common btn-case"> <span class="transform-text">View case study</span> <span class="arrow-right-circle"></span> </a>
                                    
                                    </div> --}}

                                    <div class="btn-case-study button-common-div">
                           
                                        <a href="#" class="btn btn-common btn-black"> 
                                            <span class="btn-background"></span>
    
                                            <span class="transform-text">View case study</span> 
    
                                            <span class="arrow-right-circle"></span> 
                                        </a>
                                
                                </div>

                                </div>
                            </div>
                           
                            
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
        {{-- <section class="panel project-two"  id="project-two">
            <div class="conatiner-fluid">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="project-content-two container-lg-2">
                            <h2>World's Largest
                                <span class="span-block">Wine Community</span></h2>
                            <div class="sub-heading">
                                <div class="row">
                                    <div class="col-lg-6 br-1">
                                        <h3>Technology we used</h3>
                                        <a class="#">
                                            Angular JS Development & Web Maintenance
                                        </a>
                                    </div>
                                    <div class="col-lg-6">
                                        <p>The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn't find the right beige from a set of 200.</p>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="project-img">
                                <img src="assets/images/project-2.png" alt="">
                            </div>
                            <div class="btn-case-study">
                          
                                    <a href="#" class="btn btn-common btn-case"> <span class="transform-text">View case study</span> <span class="arrow-right-circle"></span> </a>
                              
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        
                    </div>
                </div>
            </div>
            <div class="squre-shape">

            </div>
            
        </section>
        <section class="panel project-three"  id="project-three">
            <div class="conatiner-fluid">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="project-content-three container-lg-2">
                            <h2>Online Weekly
                                <span class="span-block">Shopping</span></h2>
                            <div class="sub-heading">
                                <div class="row">
                                    <div class="col-lg-6 br-1">
                                        <h3>Technology we used</h3>
                                        <a class="#">
                                            Angular JS Development & Web Maintenance
                                        </a>
                                    </div>
                                    <div class="col-lg-6">
                                        <p>The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn't find the right beige from a set of 200.</p>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="project-img">
                                <img src="assets/images/project-3.png" alt="">
                            </div>
                            <div class="btn-case-study">
                          
                                    <a href="#" class="btn btn-common btn-case"> <span class="transform-text">View case study</span> <span class="arrow-right-circle"></span> </a>
                              
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        
                    </div>
                </div>
            </div>
            <div class="squre-shape">

            </div>
        </section>
        <section class="panel project-four"  id="project-four">
            <div class="conatiner-fluid">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="project-content-four container-lg-2">
                            <h2><span >The </span>Best Coffee
                                <span class="span-block">You've Ever Made</span></h2>
                            <div class="sub-heading">
                                <div class="row">
                                    <div class="col-lg-6 br-1">
                                        <h3>Technology we used</h3>
                                        <a class="#">
                                            Angular JS Development & Web Maintenance
                                        </a>
                                    </div>
                                    <div class="col-lg-6">
                                        <p>The perfectionism makes some sense, but seems at odds with the current infatuation with Jobs. Jobs couldn't find the right beige from a set of 200.</p>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="project-img">
                                <img src="assets/images/project-4.png" alt="">
                            </div>
                            <div class="btn-case-study">
                          
                                    <a href="#" class="btn btn-common btn-case"> <span class="transform-text">View case study</span> <span class="arrow-right-circle"></span> </a>
                              
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        
                    </div>
                </div>
            </div>
            <div class="squre-shape">

            </div>
        </section>

        <section class="panel project-five"  id="project-five">
            <div class="conatiner-fluid">
                <div class="row">
                
                    <div class="col-lg-12">
                        <div class="project-content-five ">
                           
                          
                            <div class="project-img">
                                <img src="assets/images/view-all-project.png" class="" alt="">
                            </div>
                            <div class="view-all-section">
                                <div class=" view-all-project">

                                    <h2>What we<span>'</span>re looking forword next</h2>
                                    <div class="btn-case-study">
                                        
                                        <a href="#" class="btn btn-common btn-case"> <span class="transform-text">View case study</span> <span class="arrow-right-circle"></span> </a>
                                  
                                </div>
                                </div>
                            </div>
                           
                            
                        </div>
                    </div>
                   
                </div>
            </div>
        </section> --}}
    </div>
</div>


 


 @push('scripts')
 <script>
     $(window).on("load", function() {

        $('#slideContainer').owlCarousel({
        loop:false,
        
        stagePadding: 300,
        nav:false,
        smartSpeed: 4000,
        dots:true,
        items:1, 
        responsive:{
                0:{
                    dots:false,
                    margin: 10,
                    stagePadding: 10,
                    items:1
                },
                600:{
                    dots:false,
                    margin: 10,
                    stagePadding: 10,
                    items:1
                },
                1000:{
                    items:1, 
                    dots:false,
                    nav:true,
                }
            }
    });

 
     });
</script>
@endpush