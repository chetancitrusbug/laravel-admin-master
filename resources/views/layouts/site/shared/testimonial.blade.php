<section class="testimonial-section">
    <!-- <div class="bg-ourtechnology" style="background-image: url(../assets/images/banner-inner.jpg);"></div> -->
        <div class="testimonial-inner">
            <div class="container-fluid">
                <div class="row ">
                    <div class="col-lg-12">
                        <div class="owl-carousel testimonial-carousel owl-theme">
                            @foreach($testimonials as $item)
                            <div class="item">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="testimonial-img">
                                            <div class="testi-thumb">
                                                <div class="owner-img">
                                                    <img src="{!!  @$item->manyfile->where('file_type','testimonials_single_main')->first()->file_thumb_url  !!}" alt="" />
                                                </div>
                                                <div class="owner-info">
                                                <h3>{{$item->name}}</h3>
                                                    <p>{{$item->position}}</p>
                                                </div>
                                                <div class="outlined-box"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="testimonial-content">
                                            <div class="quote-img">
                                                <img src="{{ asset('assets/images/quote.png') }}" alt="">
                                            </div>
                                            <h3>{!!$item->title!!}</h3>
                                            {!! $item->description !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>       
                    </div>
                </div>
            </div>
        </div>
</section>


@push('scripts')

<script> 
$(document).ready(function(){ 
    $('.testimonial-carousel').owlCarousel({
        loop:true,
        margin:0,
        stagePadding: 0,
        nav:true,
        smartSpeed: 2000,
        dots:true,
        items:1,
        navText: ['<span class="span-roundcircle left-roundcircle"><img src="{{ asset('assets/images/icons/arrow-left.png') }}" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="{{ asset('assets/images/icons/arrow-right.png') }}" class="right_arrow_icon" alt="arrow" /></span>'],
        responsive:{
            0:{
                items:1,
                dots:false,
                nav:false,
            },
            600:{
                items:1,
                 dots:false,
                nav:false,
            },
            1000:{
                items:1,
                dots:false,
                nav:true,
            },   
        }
    });
});
</script>
@endpush