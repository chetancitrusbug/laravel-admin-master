<section class="slides">
    
    <div class="slide slide1 " data-slide="we" data-slide-type="image">
        <div class="slide_inner"  >
            <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
        </div>
    </div>

    <div class="slide slide2 " data-slide="developement" data-slide-type="image">
        <div class="slide_inner"  >
            <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
        </div>
    </div>
    <div class="slide slide3 " data-slide="partner" data-slide-type="video">
        <div class="slide_inner"  >
            <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload  loop >
                <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
            </video>
        </div>
    </div>
</section>

<section class="banner-section">
    <div class="line-1"></div>
    <div class="banner-div">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <img src="assets/images/banner1.jpg" class="banner-img" alt="banner" />
                    <div class="content-div">
                        <div class="heading-content">
                            <h1>
                                <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">We are reliable</span>
                                <span class="span-block bold-line"> 
                                    <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light" >Developement</a> 
                                    <a href="#" class="main_slide" data-slide="partner" data-slide-theme="dark">Partner</a></span>
                                <span class="span-block">since 2013</span>
                            </h1>
                        </div>
                        <div class="bottom_content">
                            <p>We are a digital agency that specializes in User Experience Design</p>
                            <div class="mouse-icon-div">
                                <a href="#case-study-section" class="link"> <span class="mouse-icon"></span> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--  end of banner  -->