<div class="new-project start-project" id="new-project">
    <div class="new-project-top-div">
        <a href="#" class="project-btn-link">
            <span class="arrow-right-circle"></span>
        </a>
    </div>
    <div class="new-project-link-middle">
        <a href="#" class="link transform-link">START A NEW PROJECT</a>
    </div>
</div>
<div class="start-new-project-form">
    <div id="start-new-project" class="start-new-project">
        <a href="javascript:void(0)" class="closebtn closeform"><img src="{{ asset('assets/images/icons/close.svg')}}" alt=""></a>
        <header class="menubar-header menubar-header-open">
            <div class="header-div clearfix">
                <div class="logo-div">
                    <a class="logo_link clearfix closeform" href="javascript:void(0)">
                        <img src="{{ asset('assets/images/logo.svg') }}" class="img-fluid logo_img main-logo" alt="logo">
                    </a>
                </div>
                <nav class="nav-center-div">
                    <a href="javascript:void(0)" class="back-to-home closeform">Back TO HOME</a>
                </nav>
            </div>
        </header>
        
        <div class="project-form">
            <div class="project-inner">
                {!! Form::open(['url' => route('site.inquiries.save'), 'novalidate'=>true , 'id' => 'inquiries_form','autocomplete'=>'off','files'=>true]) !!}
							 
       
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-heaeding">
                                <h3><span>Start a new project</span></h3>
                                <p id="error_msg" ></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <div class="controls">
                                    <input type="text" class="form-control" placeholder="Name*" name="name" required  data-validation-required-message="Please enter Name" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <div class="controls">
                                    <input type="email" class="form-control" placeholder="Email*" name="email" required  data-validation-required-message="Please enter Email" />
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group" style="margin-left: -15px;" >
                                        <div class="controls">
                                         
                                            {!! Form::select('country_code', array_merge( [''=>'Country Code'], \App\Country::select( DB::raw('CONCAT( phonecode, " - ", name) AS name')  ,'phonecode')->get()->pluck('name','phonecode')->toarray() ) ,null,['class'=>'form-control','required'=>'','data-validation-required-message'=>"Please select country code"]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group" style="margin-right: -15px;">
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="Phone*" name="phone" required data-validation-required-message="Please enter Phone" />
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <div class="controls">
                                    <input type="text" class="form-control" placeholder="Company Name*" name="company" required data-validation-required-message="Please enter company name" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="controls">
                                    <textarea class="form-control" placeholder="Your message" name="message" required data-validation-required-message="Please enter message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex-center">
                            <input type="submit" class="btn btn-submit" id="submit" value="Submit">
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}


                <div class="container" id="thankyou_message" >
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="thankyou_message">
                                    {!! getSession('site_inquiries_thankyou_message', 'Thank You') !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

       
    </div>
</div>


 

@push('scripts')
<script src="{{ asset('app-assets/vendors/js/jqBootstrapValidation.js')}}"></script>


<script> 
$(document).ready(function(){ 

(function(window, document, $) {
	'use strict';

	// Input, Select, Textarea validations except submit button
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation(
        {
            submitError:  function(){
                console.log("error");
                return false
            },// function called if there is an error when trying to submit
            submitSuccess: function($form, e){
                e.preventDefault();
                e.stopImmediatePropagation();
                
                
                var formData = {
                    'name'              : $('input[name=name]').val(),
                    'email'             : $('input[name=email]').val(),
                    'phone'             : $('input[name=phone]').val(),
                    'company'             : $('input[name=company]').val(),
                    'message'             : $('textarea[name=message]').val(),
                    'country_code'             : $('select[name=country_code]').val(),
                     
                };

                $.ajax({
                    method: "POST",
                    url: "{{ route('site.inquiries.save') }}",
                    data: formData,
                    headers: {
                        "X-CSRF-TOKEN": "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        if(data.code == '200'){
                            $('#inquiries_form').hide();
                            $('#thankyou_message').show();
                        }else{
                            console.log(data);
                            $('#inquiries_form').show();
                            $('#thankyou_message').hide();
                            $("#error_msg").html(data.message)
                        }       
                    } 
                })  
                return false
            },  
        }
    );

})(window, document, jQuery);

 
    // $('#inquiries_form #submit').on('click',function(){
    //    return false;
    // });
    
});
</script>
@endpush
