<header class="menubar-header">
    <div class="header-div clearfix">
        <div class="logo-div">
            <a class="logo_link clearfix" href="{{ route('site.home') }}">
                <img src="{{ asset('assets/images/logo.svg') }}" class="img-fluid logo_img main-logo" alt="logo" />
            </a>
        </div>

        <div class="header-work-text long-text" itemscope itemtype="https://schema.org/BreadcrumbList" >

            @if(@$breadcrumbs)
                @foreach ($breadcrumbs as $k=>$item)
                    <div itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem" >
                    
                    @if(@$item->url)
                    <a itemprop="item" href="{{ @$item->url }}"> 
                        <span itemprop="name" >{{ $item->title }}</span> </a>
                    @else
                    <span itemprop="item" > 
                        <span itemprop="name" >{{ $item->title }}</span> </span>
                    @endif

                    <meta itemprop="position" content="{{ $k+1 }}" />
                </div>
                @endforeach
            @endif

        </div>

        {{-- <div class="header-work-text long-text">
            <a href="#!">Frontend </a> <span>Technology</span>
        </div> --}}

        <nav class="nav-center-div">
            <div class="nav-m-bar">
                <a href="#" class="open-nav" id="open-nav">
                    <div class="menu-icon">
                        <div class="bar top">
                            <div class="fill"></div>
                        </div>
                        <div class="bar middle">
                            <div class="fill"></div>
                        </div>
                        <div class="bar bottom">
                            <div class="fill"></div>
                        </div>
                    </div>
                </a>
            </div>
        </nav>
    </div>
</header><!--  End of header -->
<!-- Menu strat here -->
<div class="slider-menu">
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn closeNav" id="">
            <img src="{{ asset('assets/images/icons/close.svg') }}" alt="">
        </a>
        <!-- menu left header -->
        <header class="menubar-header menubar-header-open">
            <div class="header-div clearfix">
                <div class="logo-div">
                    <a class="logo_link clearfix closeNav" href="javascript:void(0)">
                        <img src="{{ asset('assets/images/logo.svg') }}" class="img-fluid logo_img main-logo"
                            alt="logo">
                    </a>
                </div>
                <nav class="nav-center-div">
                    <a href="javascript:void(0)" class="back-to-home closeNav">Back TO HOME</a>
                </nav>
            </div>
        </header>
        <!-- menu left header end-->
        <div class="cb-menu-conatiner">
            <!-- menu -->
            <div class="cb-menu-inner-welcome ">
                <div class="welcome-menu desktop">
                    <ul>
                        <li><a href="#" data-link="work" class="welcome-link"><span>01</span>Work</a></li>
                        <li><a href="#" data-link="technolgy" class="welcome-link"><span>02</span>Tech</a></li>
                        <li><a href="#" data-link="service" class="welcome-link"><span>03</span>Service</a></li>
                        <li><a href="{{ route('site.about') }}" class="welcome-link"><span>04</span>About</a></li>
                    </ul>
                </div>

                <div class="welcome-menu mobile">
                    <ul>
                        <li><a href="{{ route('site.works') }}"   class="welcome-link"><span>01</span>Work</a></li>
                        <li><a href="{{ route('site.technology') }}"  class="welcome-link"><span>02</span>Tech</a></li>
                        <li><a href="{{ route('site.service') }}"  class="welcome-link"><span>03</span>Service</a></li>
                        <li><a href="{{ route('site.about') }}"   class="welcome-link"><span>04</span>About</a></li>
                    </ul>
                </div>

                <!-- menu list -->
                <!-- menu option -->
                <div class="cb-menu-option" style="display: none;">
                    <a href="javascript:void(0)" class="closeSubNav" id=""><img
                            src="{{ asset('assets/images/icons/close.svg')}}" alt=""></a>
                    <div id="cb-menu-option-inner">
                        <div class="cb-submenu cb-submenu-work" id="cb-submenu-work" style="display: none;">
                            <ul>
                                @foreach ($menuProjects as $key=>$item)
                            <li class="{{ $key == 1 ? 'active' : '' }}" >
                                    <div class="work-item">
                                        <div class="work-number">
                                            <span>{{ $key+1 }}</span>
                                            <div class="work-content">
                                                <h3> <a href="{{route('site.work',['slug'=>$item->slug])}}">
                                                        {{$item->title}} </a> </h3>
                                                <div class="work-img">
                                                    @if($item->projects_single_image->count())
                                                    <a href="{{route('site.work',['slug'=>$item->slug])}}">
                                                        <img src="{!!  $item->projects_single_image->first()->file_thumb_url  !!}"
                                                            alt="{{$item->title}}" />
                                                    </a>
                                                    @endif
                                                </div>
                                                <p>12 . 02 . 2018 by citrusbug</p>
                                            </div>
                                        </div>
                                        <div class="work-arrow">
                                            <a href="{{route('site.work',['slug'=>$item->slug])}}">View more</a>
                                            <span class="arrow-right-circle"></span>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="cb-submenu cb-submenu-technolgy" style="display: none;">
                            <ul>
                                @foreach($technologies as $key=>$item)
                                
                                <li>
                                    <a href="{{ route('site.tech',['slug'=>$item->slug]) }}">
                                        <!-- icon -->
                                        <div class="cb-menu-icon">

                                            @php
                                                $style = "-webkit-mask: url(".$item->icon.") no-repeat 50% 50%; mask: url(".$item->icon.") no-repeat 50% 50%;";    
                                            @endphp

                                        <span class="service-icon service-circle1" style="{{ $style }}" >
                                            </span>
                                        </div>
                                        <!-- icon -->

                                        <!-- content -->
                                        <div class="cb-content">
                                            <h3>{{ $item->title }}</h3>
                                            <p>{{ \Str::limit( strip_tags($item->desc) ,100,'...')  }}</p>
                                        </div>
                                    </a>
                                    <!-- content end-->
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="cb-submenu cb-submenu-service" style="display: none;">
                            <ul>
                                @foreach($services as $key=>$service)
                                <li>
                                    <a href="{{ route('site.service',['slug'=>$service->slug]) }}">
                                        <!-- icon -->
                                        <div class="cb-menu-icon">

                                            @php
                                                $style = "-webkit-mask: url(".$service->icon.") no-repeat 50% 50%; mask: url(".$service->icon.") no-repeat 50% 50%;";    
                                            @endphp
                                            <span class="service-icon service-circle1" style="{{ $style }}" >
                                            
                                            </span>
                                        </div>
                                        <!-- icon -->

                                        <!-- content -->
                                        <div class="cb-content">
                                            <h3>{{ $service->title }}</h3>
                                            {!! $service->intro !!}
                                            {{-- <p>{{ \Str::limit( strip_tags($service->intro)  ,80,'...')  }}</p> --}}
                                            {{-- <p>We build modern client-server applications based on the latest PHP frameworks <span>Do something amazing for you !</span></p> --}}
                                        </div>
                                    </a>
                                    <!-- content end-->
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- menu option -->
            </div>

            <div class="cb-menu-footer">
                <div class="send-message">
                    {!! getSession('footer_send_message', '') !!}
                    {{-- <h3>Send message</h3>
                    <a href="mailto:info@citrusbug.com">info@citrusbug.com</a> --}}
                </div>
                <div class="cb-social-link">

                    {!! getSession('site_social_links', '') !!}
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Menu end here -->