<section class="slides">

    <div class="slide slide1 " data-slide="we" data-slide-type="image">
        <div class="slide_inner">
            <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
        </div>
    </div>

    <div class="slide slide2 " data-slide="developement" data-slide-type="image">
        <div class="slide_inner">
            <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
        </div>
    </div>

    <div class="slide slide3 " data-slide="partner" data-slide-type="video">
        <div class="slide_inner">
            <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload loop>
                <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
            </video>
        </div>
    </div>

</section>

<section class="banner-section banner-workdetail-page team-banner">
    <!-- <div class="line-1"></div> -->
    <div class="banner-div">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <img src="assets/images/banner1.jpg" class="banner-img" alt="banner" />
                    <div class="content-div">
                        <div class="heading-content heading-workdetail ">
                            <div class="view-allprojects ">
                                <a href="" class="main_slide" data-slide="we" data-slide-theme="dark">About us</a>
                            </div>

                            <h1>We are obsessed with delivering value for our customers through technology innovation</h1>

                        </div>
                        {{-- <div class="banner-content-workdetail">
                            <div class="service-request-quote">
                                <p><span>View all Openings</span></p>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-lg-5">

                    <div class="banner-content-workdetail">
                        <div class="service-request-quote">
                            <p><span>View all Openings</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="banner-img-right">
                            <div class="banner-work-img punchline-img ">
                                <img src="assets/images/Punchline.png" class="about-content-rotation" alt="about" />
                            </div>
                        </div>  

                </div>
            </div> --}}
        </div>
    </div>
</section><!--  end of banner  -->