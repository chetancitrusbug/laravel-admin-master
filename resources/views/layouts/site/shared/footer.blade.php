<footer>
    <div class="footer-root">
        <div class="footer-inner">
            <div class="container-fluid">
                <div class="messages-footer-div container-lg-2">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <div class="messages-text-heading">
                                <h3> Want To Say <span class="font-italic">Hello?</span> </h3>
                            </div>
                            <div class="button-row"> 
                                <a href="#" class="btn btn-messages">SEND US A MESSAGE</a>
                            </div> --}}

                            {!! getSession('footer_button_text', '') !!}
                        </div>
                    </div>
                </div>



                <div class="info-link-footer-div container-lg-2">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="footer-link">
                                <ul>
                                    <li><a href="{{ route('site.works') }}">Work </a></li>
                                    <li><a href="#services">Services </a></li>
                                    {{-- <li><a href="#">Blogs</a></li> --}}
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="footer-link">
                                <ul>
                                    <li><a href="{{ route('site.technology') }}">Technology </a></li>
                                    <li><a href="{{ route('site.about') }}">About us </a></li>
                                    {{-- <li><a href="{{ route('site.team') }}">Our team </a></li> --}}
                                    {{-- <li><a href="{{ route('site.careers') }}">Career</a></li> --}}
                                    {{-- <li><a href="#">Contact</a></li> --}}
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8">


                            <div class="contact-info-footer">
                                <div class="contact-info-row contact-info-row-full">
                                    {{-- <h5>Development Center</h5>
                                        <p>
                                            <span class="span-block">A 411, Shivalik Corporate Park, Above D Mart, </span>
                                            <span class="span-block">Nr. Shyamal Cross Road, Satellite, Ahmedabad - 380015 India.</span>
                                        </p> --}}
                                    {!! getSession('footer_development_center', '') !!}
                                </div>
                                <div class="contact-info-row contact-info">

                                    {!! getSession('footer_contact_info', '') !!}

                                    {{-- <ul>
                                       <li><a href="skype:19739476185" class="contact-icon">
                                           <span class="icon-svg"><img src="assets/images/icons/skype.svg" alt=""></span>
                                            <span class="contact-icon-slide"><p>thecitrubug.info</p></span></a>
                                        </li>
                                        <li><a href="mailto:hello@company.com" class="contact-icon">
                                            <span class="icon-svg"><img src="assets/images/icons/email.svg" alt=""></span>
                                             <span class="contact-icon-slide"><p>thecitrubug.info</p></span></a>
                                         </li>
                                    </ul> --}}
                                    <!-- <h5>Phone: </h5>
                                        <p> <a href="tel:19739476185" class="co-link">19739476185</a> </p>
                                    </div>
                                    <div class="contact-info-row">
                                        <h5>Email: </h5>
                                        <p> <a href="mailto:hello@company.com" class="co-link">hello@company.com</a> </p> -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="bottom-footer-div container-lg-2">
                    <div class="row">
                        <div class="col-lg-6 col-md-5">
                            <div class="messages-bottom-div">
                                {{-- <p><span>© 2020 CITRUSBUG.</span> All rights reserved</p> --}}
                                {!! getSession('footer_copy_info', '') !!}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7 d-flex justify-content-end">
                            <div class="social-links-bottom">
                                {!! getSession('footer_social_links', '') !!}
                                {{-- <ul>
                                    <li><a href="#"><span class="icon-svg icon-facebook"></span></a></li>
                                    <li><a href="#"><span class="icon-svg icon-instagram"></span></a></li>
                                    <li><a href="#"><span class="icon-svg icon-twitter"></span></a></li>
                                    <li><a href="#"><span class="icon-svg icon-linkedin"></span></a></li>
                                    <li><a href="#"><span class="icon-svg icon-behance"></span></a></li>
                                </ul> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>
</footer>

{{-- 

<footer>
    <div class="footer-root">

        <div class="container-fluid">
            <div class="messages-footer-div container-lg-2">
                <div class="row">
                    <div class="col-md-12">
                        {!! getSession('footer_button_text', '') !!}


                    </div>
                </div>
            </div>
            <div class="info-link-footer-div container-lg-2">
                <div class="row">
                    <div class="col-md-9">
                        <div class="contact-info-footer">
                            <div class="contact-info-row contact-info-row-full">
                                {!! getSession('footer_development_center', '') !!}


                            </div>
                            <div class="contact-info-row">
                                {!! getSession('footer_phone', '') !!}

                            </div>
                            <div class="contact-info-row">
                                {!! getSession('footer_email', '') !!}

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-link">
                            <ul>
                               
                                <li><a href="{{ route('site.works') }}">Work </a></li>
<li><a href="#services">Services </a></li>
<li><a href="{{ route('site.technology') }}">Technology </a></li>


<!-- <li><a href="#">Blogs</a></li> -->
</ul>
<ul>

    <li><a href="{{ route('site.about') }}">About us </a></li>
    <li><a href="{{ route('site.team') }}">Our team </a></li>
    <li><a href="{{ route('site.careers') }}">Career</a></li>
    <!-- <li><a href="#">Blogs</a></li> -->
</ul>
</div>
</div>
</div>
</div>
<div class="bottom-footer-div container-lg-2">
    <div class="border-top">
        <div class="row">
            <div class="col-lg-6 col-md-5">
                <div class="messages-bottom-div">
                    {!! getSession('footer_send_message', '') !!}


                </div>
            </div>
            <div class="col-lg-6 col-md-7 d-flex justify-content-end">
                <div class="social-links-bottom">
                    {!! getSession('site_social_links', '') !!}

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</footer> --}}