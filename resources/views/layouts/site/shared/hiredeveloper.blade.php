<section class="hire-developer changetheme">
    <div class="hire-developer-inner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hire-developer-content">
                        <h3><span>Hire</span> a {{ $developer }} developer</h3>
                        @if(@$hiretext)
                        <p>{!! $hiretext !!}</p>
                        @else
                        <p>{!! getSession('site_home_hire_text') !!} </p>
                        @endif
                        <a href="#" class="start-project" >Click here to hire us <img src="{{asset('assets/images/icons/arrow-right-circle.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>