<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    @section('seo')

    <title>{{ @$seo['title'].' | '.config('app.name', 'Laravel') }}</title>
    <meta name="description" content="{{ @$seo['description'] }}">
    <meta name="author" content="Citrusbug Technolabs">
    <!-- Facebook -->
    <meta property="og:title" content="{{ @$seo['title'] }}" />
    <meta property="og:type" content="{{ @$seo['og_type'] }}" />
    <meta property="og:url" content="{{ @$seo['url'] }}" />
    <meta property="og:image" content="{{ @$seo['og_image'] }}" />
    <meta property="fb:admins" content="{{ @$seo['fb_admins'] }}" />
    <meta property="fb:app_id" content="{{ @$seo['fb_admins'] }}" />
    <meta property="og:site_name" content="{{ @$seo['site_name'] }}" />
    <meta property="og:description" content="{{ @$seo['description'] }}" />
    <!-- Twitter -->
    <meta name="twitter:card" content="{{ @$seo['twitter_card'] }}" />
    <meta name="twitter:site" content="{{ '@'.@$seo['site_name'] }}" />
    <meta name="twitter:title" content="{{ @$seo['title'] }}" />
    <meta name="twitter:description" content="{{ @$seo['description'] }}" />
    <meta name="twitter:image" content="{{ @$seo['og_image'] }}" />
    @show

    <!-- responsive -->
    <meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
    <meta name="theme-color" content="ED5C37">
    <meta name="msapplication-navbutton-color" content="ED5C37">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="ED5C37">
    <!-- Responsive END -->

    @section('css')
    <!-- CSS -->
    <link href="{{ asset('assets/images/favicon.png')}}" rel='shortcut icon'>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" />

    <link rel="stylesheet" href="{{ asset('css/site/site.css')}}" media="all" type="text/css" />
    <!-- CSS END -->

     {!! getSession('google_analytics') !!}    
    @show

</head>

<body theme="{{@$theme}}" slide-theme="" >

    <div id="wrapper" class="wrapper home-wrapper inner-page">

        @include('layouts.site.shared.menu')

        @include('layouts.site.shared.projectinquiry')

        @yield('content')

        @include('layouts.site.shared.footer')

        <section class="loading"> 
            <div class="loding_container">
                <div class="logo-div">
                    <a class="logo_link clearfix" href="index.html">
                        <img src="{{ asset('assets/images/logo.svg')}}" class="img-fluid logo_img main-logo"
                            alt="logo" />
                    </a>
                </div>
            </div>
        </section>
    </div><!-- end of wrapper -->
    <!-- JS -->
    @stack('js')
    <!-- <script src="{{ asset('assets/js/modernizr.js')}}"></script> -->
    <!--[if IE]>
        <script src="assets/js/html5.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.0.4/gsap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/smooth-scrollbar/8.3.1/smooth-scrollbar.js"></script>

    <!-- 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.0.4/CSSRulePlugin.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.0.4/MotionPathPlugin.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.0.4/PixiPlugin.min.js"></script> 
    -->

    <script src="{{ asset('/js/site/custom.js')}}"></script>

    @show
    <!-- JS END -->
    @stack('scripts')
    @show

</body>

</html>