@extends('layouts.site.index')
@section('content')

<div class="inner-page">
 @php
     
    $sows = explode("\n",$work->sow);
    $sowlist = array_chunk($sows , (int)(count($sows)/2)+1 );
    
    $param = unserialize($work->param);

    $project_type = @$param['project_type'];

 @endphp

    <section class="banner-section banner-workdetail-page">
        <!-- <div class="line-1"></div> -->
        <div class="banner-div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <img src="{!!  @$work->manyfile->where('file_type','projects_single_cover')->first()->file_thumb_url  !!}" class="banner-img" alt="banner" />
                        <div class="content-div">
                            <div class="heading-content heading-workdetail">
                                <div class="view-allprojects">
                                    <a href="{{route('site.works')}}">View all Projets</a>
                                </div>

                                <h1>{{ $work->title}}</h1>
                                <p>
                                    {{ $work->desc}}
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">

                        <div class="banner-content-workdetail">
                            <div class="content-one">
                                @php 
                                    $techno = $work->technologies()->select('*')->pluck('title','slug');
                                    
                                    if($techno){
                                        foreach($techno as  $slug=>$tech){
                                            
                                            echo '<p><a href="'.route('site.tech',['slug'=>$slug]).'">'.$tech.'</a></p>';
                                             
                                        }
                                    }
                                @endphp
 
                            </div>
                            <div class="content-two">
                                <p><a href="#"> View Description </a></p>
                                <div class="arrrow-div">
                                    <a href="#"><img src="{{asset('assets/images/icons/arrow-verticle-line.png')}}" class="arrow-lines arrow-horizontal-lines" alt="arrow"></a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="banner-img-right">
                             
                             <div class="banner-work-img {{ $project_type == 'website' ? 'mac-frame' : 'mobile-mockup-img' }} ">

                                
                                @if($project_type == 'website')
                                <img src="{{asset('assets/images/imac-1.png')}}" alt="">
                                @else 
                                <img src="{{asset('assets/images/iphone.png')}}" alt="">
                                @endif
                                
                                <div class="mac-frame-inner">
                                    <img src="{!!  @$work->manyfile->where('file_type','projects_single_scroll')->first()->file_thumb_url  !!}" alt="">
                                </div>

                                 
                            </div>
                            
                        </div>

                         

                    </div>
                </div>
            </div>
        </div>
    </section><!--  end of banner  -->


    <div class="main-middle-area">




        <section class="our-challanges-section">
            <div class="container-fluid">
                <div class="our-challanges-inner">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="our-challanges-heading">

                                {!! $work->description !!}

                                 
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>
@if(count($sowlist)>0)
        <section class="scope-of-work-section">
            <div class="container-fluid">
                <div class="scope-of-work-inner ">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="scope-of-work-heading">
                                <h2>Features of  <a herf="#" class="heading-link"> {!! $work->title !!} </a>  </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        @foreach ($sowlist as $list)
                            <div class="col-lg-6">
                                <div class="scope-of-work-listing">
                                    <ul>
                                        @foreach ($list as $item)
                                        <li> {{ $item }} </li>
                                        @endforeach
                                    </ul>
                                </div>

                            </div>

                        @endforeach
                       
                         
                    </div>
                </div>
            </div>
        </section>
@endif

        <section class="image-slider-section">
            <div class="container-fluid">   
                <div class="image-slider-inner">
                    <div class="row">

                        <div id="project_images" class=" owl-carousel img-slide-carousel owl-theme">
                            <!-- item start -->
                            @php 
                                $images =   $work->manyfile->where('file_type','projects_multiple_images');
                            @endphp
                            @foreach ($images as $item)
                                <div class="item">
                                    <div class="col-lg-12">
                                        <div class="img-slide">
                                            <img src="{{ $item->file_thumb_url }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @component('layouts.site.shared.hiredeveloper', ['developer' => 'Dedicated' ]) 
        @endcomponent

        @component('layouts.site.shared.testimonial', ['testimonials' => $testimonials]) 
        @endcomponent

        @include('layouts.site.shared.technology')
        
        @include('layouts.site.shared.services')

        {{-- <section class="delivery-section">
            <div class="container-fluid">
                <div class="delivery-inner">
                    <div class="row">
                        <div class=" offset-lg-1 col-lg-5">
                            <div class="delivery-info">
                                <h4>feature Three</h4>

                                <h2>Delivery
                                    <spna class="span-block">readiness time</spna>
                                </h2>

                                <div class="form-group">
                                    <label>Get update for new design</label>
                                    <div class="form-group-merge">
                                        <input type="text" class="form-control" placeholder="*Your email address" />
                                        <button type="submit" class="btn-default"><i class="fe fe-arrow-right"></i></button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="offset-lg-1 col-lg-5">
                            <div class="service-info">
                                <h4>feature Three</h4>
                                <h2>Service
                                    <spna class="span-block">delivery point</spna>
                                </h2>

                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <div class="service-info">
                                        <h5>+120%</h5>
                                        <p>Your advantages help clients to make their decision</p>
                                    </div>
                                </div>
                                <div class=" col-lg-6 col-md-12">
                                    <div class="service-info">
                                        <h5>2 times</h5>
                                        <p>Your advantages help clients to make their decision</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="services-section">
            <div class="container-fluid">
                <div class="services-div container-lg">
                    <div class="heading-div">
                        <h2>Get to know a bit more about Our skills or feel free to look around <a href="#" class="heading-link">Our</a> <a href="#" class="heading-link">coding</a>and <a href="#" class="heading-link">design</a> <a href="#" class="heading-link">works.
                            </a></h2>
                        <p>We are a digital agency that specializes in User Experience Design</p>
                    </div>
                    <div class="services-root">

                        <div class="services-box services-box1">
                            <div class="services-icons">
                                <span class="icon-span">
                                    <span class="service-icon service-circle1"></span>
                                </span>
                            </div>
                            <div class="services-content">
                                <h4>Dedicated Development</h4>
                                <p>We all live in an age that belongs to the young at heart. Life that is
                                    becoming extremely fast, day to day, also asks us to remain</p>
                                <div class="link-box-div">
                                    <a href="#" class="btn btn-more">Learn more</a>
                                </div>
                            </div>
                        </div>

                        <div class="services-box services-box2">
                            <div class="services-icons">
                                <span class="icon-span">
                                    <span class="service-icon service-circle2"></span>
                                </span>
                            </div>
                            <div class="services-content">
                                <h4>UX/UI design</h4> 
                                <p>We all live in an age that belongs to the young at heart. Life that is
                                    becoming extremely fast, day to day, also asks us to remain</p>
                                <div class="link-box-div">
                                    <a href="#" class="btn btn-more">Learn more</a>
                                </div>
                            </div>
                        </div>

                        <div class="services-box services-box3">
                            <div class="services-icons">
                                <span class="icon-span">
                                    <span class="service-icon service-circle3"></span>
                                </span>
                            </div>
                            <div class="services-content">
                                <h4>Custom Software Development</h4>
                                <p>We all live in an age that belongs to the young at heart. Life that is
                                    becoming extremely fast, day to day, also asks us to remain</p>
                                <div class="link-box-div">
                                    <a href="#" class="btn btn-more">Learn more</a>
                                </div>
                            </div>
                        </div>

                        <div class="services-box services-box4">
                            <div class="services-icons">
                                <span class="icon-span">
                                    <span class="service-icon service-circle4"></span>
                                </span>
                            </div>
                            <div class="services-content">
                                <h4>Mobile app developement</h4>
                                <p>We all live in an age that belongs to the young at heart. Life that is
                                    becoming extremely fast, day to day, also asks us to remain</p>
                                <div class="link-box-div">
                                    <a href="#" class="btn btn-more">Learn more</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section> --}}

    </div>
</div>
@endsection


@push('scripts')
 <script>
     $(window).on("load", function() {
        $('#project_images').owlCarousel({
            loop:true,
            margin: 100,
            stagePadding: 100,
            nav:false,
            smartSpeed: 2000,
            dots:false,
            items:2,
            responsive:{
                0:{
                    margin: 10,
                    stagePadding: 10,
                    items:1
                },
                600:{
                    margin: 10,
                    stagePadding: 10,
                    items:1
                },
                1000:{
                    items:2,
                    dots:false,
                    nav:true,
                }
            }

        });
     });
</script>
@endpush