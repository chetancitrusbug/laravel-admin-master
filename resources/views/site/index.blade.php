@extends('layouts.site.index')
@section('content')

    @component('layouts.site.shared.banners')

        @slot('images')
            <div class="slide slide1 " data-slide="we" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide2 " data-slide="developement" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide3 " data-slide="partner" data-slide-type="video">
                <div class="slide_inner"  >
                    <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload  loop >
                        <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
                    </video>
                </div>
            </div>
        @endslot
        @slot('description')
            <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">We are reliable</span>
            <span class="span-block bold-line"> 
                <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light" >Developement</a> 
                <a href="#" class="main_slide" data-slide="partner" data-slide-theme="dark">Partner</a></span>
            <span class="span-block">since 2013</span>
        @endslot

        @slot('desc')
            <p>We are a digital agency that specializes in User Experience Design</p>
        @endslot
       
        @slot('nextsection')
            main-middle-area
        @endslot
        

    @endcomponent

    <div class="main-middle-area" id="main-middle-area" > 

        @include('layouts.site.shared.technology')
    
        @include('layouts.site.shared.projects')

    
        @component('layouts.site.shared.hiredeveloper', ['developer' => 'Dedicated']) 
        @endcomponent

        @component('layouts.site.shared.testimonial', ['testimonials' => $testimonials]) 
        @endcomponent

        @include('layouts.site.shared.services')
 
      

        


    </div>

@endsection