@extends('layouts.site.index')
@section('content')

@php
      
$param = unserialize($service->param);

$tag_line = @$param['tag_line'];
$hire_title = @$param['hire_title'];
$hire_text = @$param['hire_text'];
@endphp

<div class="inner-page">


    <section class="banner-section banner-workdetail-page service-banner team-banner">
        <!-- <div class="line-1"></div> -->
        <div class="banner-div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">        
                        <img src="{{ asset('assets/images/banner1.jpg')}}" class="banner-img" alt="banner" />
                        <div class="content-div">
                            <div class="heading-content heading-workdetail ">
                                <div class="view-allprojects">
                                    <a href="">{{$service->title}}</a>
                                </div>
                                
                                <h1>{{ $tag_line}} </h1>

                            </div>
                           
                        </div>
                    </div>
                    </div>
                    <div class="row"> 
                    <div class="col-lg-5">
                        
                        <div class="banner-content-workdetail">
                            <div class="service-request-quote">
                                 <p><span>Request for qoutes</span>
                                    <div class="arrrow-div">
                                        <a href="#"><img src="{{ asset('assets/images/icons/arrow-horizontal-line.svg')}}" class="arrow-lines arrow-horizontal-lines" alt="arrow" /></a>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div> 
                    {{-- <div class="col-lg-7">
                        <div class="banner-img-right">
                            <div class="banner-work-img top-80">
                                <img src="assets/images/our-team-img.png" alt="">
                            </div>
                        </div>
                        
                    </div> --}}
                </div>        
            </div>
        </div>
    </section><!--  end of banner  -->

{{-- 
    @component('layouts.site.shared.banners', ['banner' => @$banner])

    @slot('images')
        <div class="slide slide1 " data-slide="we" data-slide-type="image">
            <div class="slide_inner"  >
                <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
            </div>
        </div>
        <div class="slide slide2 " data-slide="developement" data-slide-type="image">
            <div class="slide_inner"  >
                <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
            </div>
        </div>
        <div class="slide slide3 " data-slide="partner" data-slide-type="video">
            <div class="slide_inner"  >
                <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload  loop >
                    <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
                </video>
            </div>
        </div>
    @endslot
    @slot('description')
        <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">Services</span>
        <span class="span-block bold-line"> 
            <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light" >{{$service->title}}</a> 
            
        </span>
        
    @endslot

    @slot('desc')
        {!! $service->intro !!}
    @endslot
   
    @slot('nextsection')
        main-middle-area
    @endslot
    

@endcomponent --}}






    <div class="main-middle-area">


        <section class="service-details-section" id="service-details">
            <div class="container-fluid">
                <div class="service-details-div">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-div heading-center-div mb-40">

                                {{-- {!! $service->intro !!} --}}

                                {!! $service->description !!}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        {{-- <section class="services-box-section">
            <div class="container-fluid">
                <div class="services-box-div align-items-center ">

                    @if ($service->servicedetails)
                    @foreach ($service->servicedetails as $key=>$item)



                    <div class="services-box-row {{ $key%2 ? 'services-box-row2' : '' }} ">
                        <div class="row">

                            <div class="col-lg-6 col-md-6 {{ $key%2 ? '' : 'order-2' }} ">
                                <div class="content-div">
                                    <div class="heading-div">
                                        <span class="number-span">{{ number_format($key+1) }}</span>
                                        <h3>{{$item->title}}</h3>
                                        <h2>{{$item->desc}}</h2>
                                        {!!$item->description!!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="services-box-thumb">
                                    <img src="{{ $item->manyfile->where('file_type','servicedetails_single_image')->first()->file_thumb_url }}"
                                        class="img-fluid img-center img-service-box" alt="service" />
                                    <div class="outlined-box"></div>
                                    <div class="rect-fill-box"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif



                </div>
            </div>
        </section> --}}


        <section class="related-project-section">
            <div class="container-fluid">
                <div class="related-project-div container-lg1">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-div">
                                <h2>
                                    <span class="span-block"> {{ $hire_title }}  <a href="#" class="link link6">Projects</a> </span>
                                </h2>
                                <p>We are a digital agency that specializes in User Experience Design</p>
                            </div>
                        </div>
                    </div>

                    @include('layouts.site.shared.projectslider',['projects'=>$service->projects])
                </div>
            </div>
        </section>

        @component('layouts.site.shared.hiredeveloper', ['developer' => $hire_title, 'hiretext' => $hire_text ]) 
        @endcomponent

        @include('layouts.site.shared.technology')

        @component('layouts.site.shared.testimonial', ['testimonials' => $testimonials]) 
        @endcomponent

        @include('layouts.site.shared.services')

    </div>
</div>
@endsection