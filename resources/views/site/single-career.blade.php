@extends('layouts.site.index')
@section('content')

<div class="inner-page">



    <section class="banner-section banner-workdetail-page banner-technology-detail">
        <!-- <div class="line-1"></div> -->
        <div class="banner-div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <img src="assets/images/banner1.jpg" class="banner-img" alt="banner" />
                        <div class="content-div">
                            <div class="heading-content heading-workdetail">
                                <div class="view-allprojects">
                                    <a href="route('site.careers')">{!! getSession('site_careers_middle_banner_title',  '') !!}</a>
                                </div>
                                <h1>{{$career->designation}}
                                    <span class="span-block">{{ $career->work_type }}</span></h1>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">

                        <div class="banner-content-workdetail">
                            <div class="content-one">
                                <p><a href="#">View all Openings</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="banner-img-right">
                            <div class="banner-work-img top-80">
                                <img src="assets/images/career-detail-img.png" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="main-middle-area">


        <section class="career-details-section">
            <div class="container-fluid">
                <div class="career-details-div container-lg">

                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="content-div-root">
                                <h2>{{ $career->description_title }}</h2>
                                {!! $career->description !!}
                                <!-- <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="content-prag">
                                            <p>Citrusbug is a design and experience agency that partners with modern, global brands. At Citrusbug we are on a mission to relentlessly generate smart, creative, forward-thinking solutions in the space between people and technology. As our new Senior Designer, you will be bringing not only your impeccable design skills and passion for art & technology, but also expertise in client management, problem solving and strategic thinking to our Creative Department.</p>
                                            <p>You’ll be part of an international team, comprised of some of the most talented makers, producers, and creative minds in the industry. We are people who aspire </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="content-prag">
                                            <p>Citrusbug is a design and experience agency that partners with modern, global brands. At Citrusbug we are on a mission to relentlessly generate smart, creative, forward-thinking solutions in the space between people and technology. As our new Senior Designer, you will be bringing not only your impeccable design skills and passion for art & technology, but also expertise in client management, problem solving and strategic thinking to our Creative Department.</p>
                                            <p>You’ll be part of an international team, comprised of some of the most talented makers, producers, and creative minds in the industry. We are people who aspire </p>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-center mt-30">
                        <div class="col-lg-12 col-md-12">
                            <div class="content-div-root">
                                <h2>{!! getSession('site_career_single_opportunity_title',  '') !!}</h2>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="content-apply">
                                        {!! $career->opportunity !!}
                                            <div class="button-common-div button-common-div2 mt-80">
                                                <a href="#" class="btn btn-common btn-black-common"> <span class="transform-text">Apply now</span> <span class="arrow-right-circle"></span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>
@endsection