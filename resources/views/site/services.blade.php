@extends('layouts.site.index')
@section('content')

<div class="inner-page">


    @component('layouts.site.shared.banners', ['banner' => $banner])

    @slot('images')
    <div class="slide slide1 " data-slide="we" data-slide-type="image">
        <div class="slide_inner">
            <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
        </div>
    </div>
    <div class="slide slide2 " data-slide="developement" data-slide-type="image">
        <div class="slide_inner">
            <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
        </div>
    </div>
    <div class="slide slide3 " data-slide="partner" data-slide-type="video">
        <div class="slide_inner">
            <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload loop>
                <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
            </video>
        </div>
    </div>
    @endslot
    @slot('description')
    <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">We are reliable</span>
    <span class="span-block bold-line">
        <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light">Developement</a>
        <a href="#" class="main_slide" data-slide="partner" data-slide-theme="dark">Partner</a></span>
    <span class="span-block">since 2013</span>
    @endslot

    @slot('desc')
    <p>We are a digital agency that specializes in User Experience Design</p>
    @endslot

    @slot('nextsection')
    main-middle-area
    @endslot


    @endcomponent

    <div class="main-middle-area" id="main-middle-area">



        {{-- <section class="services-section">
            <div class="container-fluid"> 
                <div class="services-div container-lg">
                    <div class="heading-div">


                        <h2>
                            {!! getSession('site_service_service_title', '') !!}
                        </h2>
                        <p>{{ getSession('site_service_service_desc',  '') }}</p>

    </div>
    <div class="services-root">

        @foreach($services as $key=>$service)
        <div class="services-box services-box{{$key+1}}">
            <a href="{{ route('site.service',['slug'=>$service->slug]) }}">
                <div class="services-icons">
                    <span class="icon-span">
                        <span class="service-icon service-circle{{$key+1}}"></span>
                    </span>
                </div>
            </a>
            <div class="services-content">
                <a href="{{ route('site.service',['slug'=>$service->slug]) }}">
                    <h4>{{ $service->title }}</h4>
                </a>
                <p>{{ $service->intro }}</p>
                <div class="link-box-div">
                    <a href="{{ route('site.service',['slug'=>$service->slug]) }}" class="btn btn-more">Learn more</a>
                </div>
            </div>
        </div>
        @endforeach

    </div>
</div>
</div>
</section> --}}

@include('layouts.site.shared.services')

@include('layouts.site.shared.technology')

<section class="related-project-section">
    <div class="container-fluid">
        <div class="related-project-div container-lg1">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading-div">
                        <h2>
                            <span class="span-block">Our Top <a href="#" class="link link6">Project</a> </span>
                        </h2>
                        <p>We are a digital agency that specializes in User Experience Design</p>
                    </div>
                </div>
            </div>

            @include('layouts.site.shared.projectslider',['projects'=>$projects])
        </div>
    </div>
</section>

@component('layouts.site.shared.hiredeveloper', ['developer' => 'Dedicated'])
@endcomponent

@component('layouts.site.shared.testimonial', ['testimonials' => $testimonials])
@endcomponent

</div>
</div>
@endsection