@extends('layouts.site.index')
@section('content')

<div class="inner-page">


    @component('layouts.site.shared.banners', ['banner' => @$banner])

        @slot('images')
            <div class="slide slide1 " data-slide="we" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide2 " data-slide="developement" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide3 " data-slide="partner" data-slide-type="video">
                <div class="slide_inner"  >
                    <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload  loop >
                        <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
                    </video>
                </div>
            </div>
        @endslot
        @slot('description')
            <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">We are reliable</span>
            <span class="span-block bold-line"> 
                <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light" >Developement</a> 
                <a href="#" class="main_slide" data-slide="partner" data-slide-theme="dark">Partner</a></span>
            <span class="span-block">since 2013</span>
        @endslot

        @slot('desc')
            <p>We are a digital agency that specializes in User Experience Design</p>
        @endslot
       
        @slot('nextsection')
            main-middle-area
        @endslot
        

    @endcomponent

    <div class="main-middle-area">

        @include('layouts.site.shared.technologydetailepage')

        {{-- <section class="technology-section" id="technology-section">
            <div class="container-fluid">
                <div class="technology-div container-lg">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-div">
                                <h2>
                                    {!! getSession('site_technology_technology_title',  '') !!}  
                                    <!-- <span class="span-block">Customer-first technology </span>
                                    <span class="span-block"><a href="#" class="link link5">crafted <span class="line-5span">specifically</span></a> for the
                                        <span class="span-block">IT industry.</span> -->
                                </h2>
                                <p>{!! getSession('site_technology_technology_description',  '') !!}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12">

                            

                            <div class="tech-root">
                                @if(count($technologies) > 0)
                                @foreach($technologies as $key => $technology)
                                    <div class="tech-box {{($key % 2 === 0) ? 'tech-box-right' : 'tech-box-left'}}">
                                        <div class="column-40 {{(!($key % 2 === 0)) ? 'order-2' : ''}}">
                                            <div class="img-shape-div">
                                                <img src="assets/images/icons/shape-arrow-view1.svg" class="shape-arrow-view shape-view1" alt="shapes">
                                                <img src="assets/images/icons/round-arrow-tech.svg" class="shape-arrow-tech shape-tech1" alt="shapes">
                                            </div>
                                        </div>
                                        <div class="column-60">
                                            <div class="tech-content-div">
                                                <h2>{{$technology->title}}
                                                    <span class="lines-hr lines-hr32 mt-40"></span>
                                                </h2>
                                                <p>{{$technology->desc}}</p>
                                                <div class="tech-icon-box-root mt-40">
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                    <div class="tech-icon-box">
                                                        <img src="assets/images/icons/tech/angular.svg" alt="technology">
                                                    </div>
                                                </div>
                                              
                                                <div class="button-common-div">
                                                    <a href="{{route('site.tech',['slug'=>$technology->slug])}}" class="btn btn-common btn-white"> 
                                                       <span class="btn-background"></span>
                   
                                                       <span class="transform-text">View case study</span> 
                   
                                                       <span class="arrow-right-circle"></span> 
                                                   </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section> --}}

        <section class="related-project-section">
            <div class="container-fluid">
                <div class="related-project-div container-lg1">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-div">
                                <h2>
                                    <span class="span-block">Our Top <a href="#" class="link link6">Project</a> </span>
                                </h2>
                                <p>We are a digital agency that specializes in User Experience Design</p>
                            </div>
                        </div>
                    </div>

                    @include('layouts.site.shared.projectslider',['projects'=>$projects])
                </div>
            </div>
        </section>

        @include('layouts.site.shared.services')
    </div>
</div>
@endsection