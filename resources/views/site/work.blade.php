@extends('layouts.site.index')
@section('content')

<div class="inner-page">

    @component('layouts.site.shared.banners', ['banner' => @$banner])

    @slot('images')
        <div class="slide slide1 " data-slide="we" data-slide-type="image">
            <div class="slide_inner"  >
                <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
            </div>
        </div>
        <div class="slide slide2 " data-slide="developement" data-slide-type="image">
            <div class="slide_inner"  >
                <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
            </div>
        </div>
        <div class="slide slide3 " data-slide="partner" data-slide-type="video">
            <div class="slide_inner"  >
                <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload  loop >
                    <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
                </video>
            </div>
        </div>
    @endslot
    @slot('description')
        <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">We are reliable</span>
        <span class="span-block bold-line"> 
            <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light" >Developement</a> 
            <a href="#" class="main_slide" data-slide="partner" data-slide-theme="dark">Partner</a></span>
        <span class="span-block">since 2013</span>
    @endslot

    @slot('desc')
        <p>We are a digital agency that specializes in User Experience Design</p>
    @endslot
   
    @slot('nextsection')
        main-middle-area
    @endslot
    

@endcomponent


    <div class="main-middle-area">



        <section class="project-view-section gray_bg" id="case-study-section">
            <div class="container-fluid">
                <div class="project-view-div">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-section">
                                {!! getSession('site_work_title',  '') !!} 
                                <p>{!! getSession('site_work_description',  '') !!}</p>
                            </div>
                        </div>
                    </div>

                    <div class="all-workslider">

                        <div class="work-tabs">

                            <ul class="nav nav-tabs">
                                <li><a data-toggle="tab" href="#all" class="active"><span>All</span></a></li>
                                @foreach($technologies as $key=>$item)
                                <li><a data-toggle="tab" href="#{{$item->slug}}"  ><span>{{$item->title}}</span></a></li>
                                @endforeach
                                 
                            </ul>
                            <div class="tab-content">

                                <div id="all" class="tab-pane fade  show in active ">
                                    <div class="row">
                                        @foreach ($projects as $work)

                                        <div class="col-lg-3 ">
                                            <div class="all-wrok-box">

                                                <div class="all-wrok-heading">
                                                    <h3>{{ $work->title}}</h3>
                                                </div>
                                                <div class="all-wrok-img">
                                                    
                                                    @if(isset($work) && $work->manyfile->where('file_type','projects_single_image')->count())
                                                        <a href="{{route('site.work',['slug'=>$work->slug])}}">
                                                            <img src="{{$work->manyfile->where('file_type','projects_single_image')->first()->file_thumb_url}}" alt="{{ $work->title}}">
                                                        </a>
                                                        @endif
                                                </div>
                                                {{-- <div class="all-wrok-date">
                                                    <p>12 . 02 . 2018 by citrusbug</p>
                                                </div> --}}
                                                 
                                                <div class="all-wrok-viewmore">
                                                    <a href="{{route('site.work',['slug'=>$work->slug])}}">View more</a> <span class="arrow-right-circle"></span>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        @endforeach
                                    </div>
                                </div>

                            @foreach($technologies as $key=>$item)
                                <div id="{{$item->slug}}" class="tab-pane fade  ">
                                    <div class="row">
                                        @if($item->projects)
                                            @foreach ($item->projects as $work)

                                            <div class="col-lg-3 ">
                                                <div class="all-wrok-box">
    
                                                    <div class="all-wrok-heading">
                                                        <h3>{{ $work->title}}</h3>
                                                    </div>
                                                    <div class="all-wrok-img">
                                                        
                                                        @if(isset($work) && $work->manyfile->where('file_type','projects_single_image')->count())
                                                        <a href="{{route('site.work',['slug'=>$work->slug])}}">
                                                            <img src="{{$work->manyfile->where('file_type','projects_single_image')->first()->file_thumb_url}}" alt="{{ $work->title}}">
                                                        </a>
                                                        @endif
                                                    </div>
                                                    {{-- <div class="all-wrok-date">
                                                        <p>12 . 02 . 2018 by citrusbug</p>
                                                    </div> --}}
                                                     
                                                    <div class="all-wrok-viewmore">
                                                        <a href="{{route('site.work',['slug'=>$work->slug])}}">View more</a> <span class="arrow-right-circle"></span>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            @endforeach

                           
                              
                            </div>
                        </div>
                        <div class="row">



                        </div>
                    </div>

                </div>
            </div>
        </section>

        {{-- <section class="project-view-section" id="case-study-section">
            <div class="container-fluid">
                <div class="project-view-div container-lg-2">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-section">
                                {!! getSession('site_work_title',  '') !!} 
                                <p>{!! getSession('site_work_description',  '') !!}</p>
                            </div>
                        </div>
                    </div>

                    <div class="all-workslider">
                        <div class="row">

                            <div class=" owl-carousel work-carousel owl-theme">

                                @if(count ($works) > 0)
                                @foreach($works as $key => $work)
                                <!-- item start -->
                                <div class="item">
                                    <div class="col-lg-12 pad-0">
                                        <div class="all-wrok-box">
                                            <div class="all-wrok-number">
                                                <h2>{{sprintf("%02d",$key+1)}}</h2>
                                            </div>
                                            <div class="all-wrok-heading">
                                                <h3>{{$work->title}}</h3>
                                            </div>
                                            <div class="all-wrok-img">
                                            @if(isset($work) && $work->manyfile->where('file_type','projects_single_image')->count())
                                            <img src="{{$work->manyfile->where('file_type','projects_single_image')->first()->file_thumb_url}}" alt="">
                                            @endif
                                                
                                            </div>
                                            <div class="all-wrok-date">
                                                <p>12 . 02 . 2018 by citrusbug</p>
                                            </div>
                                            <div class="all-wrok-viewmore">
                                                <a href="{{route('site.work',['slug'=>$work->slug])}}">View more</a> <span class="arrow-right-circle"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                <!-- item end -->
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section> --}}

        {{-- <section class="solution-work-section" style="background-image:url({!! getSession('site_work_below_banner_background_image',  '') !!})">
            <div class="container-fluid">
                <div class="row">
                    <div class="solution-work-inner">
                        <div class="offset-lg-1 col-lg-7 h-100">

                            <div class="solution-left">
                                <div class="solution-list">
                                    {!! getSession('site_work_below_banner_titles',  '') !!}
                                    <div class="solution-content">
                                        <p>{!! getSession('site_work_below_banner_description',  '') !!}</p>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="col-lg-4">
                            <div class="solution-right">
                                <div class="solution-userprofile">
                                    <img src="{!! getSession('site_work_below_banner_image',  '') !!}" alt="">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        @component('layouts.site.shared.hiredeveloper', ['developer' => 'Dedicated'])
@endcomponent

        @include('layouts.site.shared.technology')

        @component('layouts.site.shared.testimonial', ['testimonials' => $testimonials])
@endcomponent
        
        @include('layouts.site.shared.services')
        
    </div>
</div>



@endsection


@push('scripts')
<script> 
// $(document).ready(function(){ 

//     $('.work-carousel').owlCarousel({
//     loop:true,
//     margin:2,
//     nav:true,
//     smartSpeed: 2000,
//     dots:true,
//     navText: ['<span class="span-roundcircle left-roundcircle"><img src="{{asset('assets/images/icons/arrow-left.png')}}" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="{{asset('assets/images/icons/arrow-right.png')}}" class="right_arrow_icon" alt="arrow" /></span>'],
    
// });

// });

</script>
@endpush