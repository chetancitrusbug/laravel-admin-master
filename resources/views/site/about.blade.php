@extends('layouts.site.index')
@section('content')

<div class="inner-page">

    <section class="banner-section banner-workdetail-page team-banner">
        <!-- <div class="line-1"></div> -->
        <div class="banner-div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">        
                        <img src="{{ asset('assets/images/banner1.jpg')}}" class="banner-img" alt="banner" />
                        <div class="content-div">
                            <div class="heading-content heading-workdetail ">
                                <div class="view-allprojects">
                                    <a href="">About us</a>
                                </div>
                                
                                <h1>There’s more to us than just creating beautiful designs.</h1>

                            </div>
                           
                        </div>
                    </div>
                    </div>
                    <div class="row"> 
                    <div class="col-lg-5">
                        
                        {{-- <div class="banner-content-workdetail">
                            <div class="service-request-quote">
                                 <p><span>View all Openings</span></p>
                            </div>
                        </div> --}}
                    </div> 
                    {{-- <div class="col-lg-7">
                        <div class="banner-img-right">
                            <div class="banner-work-img punchline-img ">
                                <img src="{{ asset('assets/images/Punchline.png')}}" class="about-content-rotation" alt="about" />
                            </div>
                        </div>
                        
                    </div> --}}
                </div>        
            </div>
        </div>
    </section><!--  end of banner  -->
{{-- 
    @component('layouts.site.shared.banners', ['banner' => @$banner])

        @slot('images')
            <div class="slide slide1 " data-slide="we" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide2 " data-slide="developement" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide3 " data-slide="partner" data-slide-type="video">
                <div class="slide_inner"  >
                    <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload  loop >
                        <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
                    </video>
                </div>
            </div>
        @endslot
        @slot('description')
            <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">We are reliable</span>
            <span class="span-block bold-line"> 
                <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light" >Developement</a> 
                <a href="#" class="main_slide" data-slide="partner" data-slide-theme="dark">Partner</a></span>
            <span class="span-block">since 2013</span>
        @endslot

        @slot('desc')
            <p>We are a digital agency that specializes in User Experience Design</p>
        @endslot
       
        @slot('nextsection')
            main-middle-area
        @endslot
        

    @endcomponent --}}


    <div class="main-middle-area">


       

        <section class="about-us-section" id="about-us-details">
            <div class="container-fluid">
                <div class="about-us-div">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-div heading-center-div mb-40">
                                {!! getSession('site_about_title',  '') !!} 
                                {!! getSession('site_about_description',  '') !!} 
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="payment-option-root">
                                @if(count($partners) > 0)
                                @foreach($partners as $partner)
                                @php $image = ''; @endphp
                                <div class="payment-option-box">
                                    <a href="#" class="logo-payment">
                                    @if(isset($partner) && $partner->manyfile->where('file_type','partners_single_image')->count())
                                    @php $image =  $partner->manyfile->where('file_type','partners_single_image')->first()->file_thumb_url;  @endphp
                                   
                                    @endif
                                        <span class="payment-span" style="mask:url({{$image}}) no-repeat 50% 50%;-webkit-mask:url({{$image}}) no-repeat 50% 50%;width: 100%; height: 100%;"></span>
                                    </a>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div> --}}

                </div>
            </div>
        </section>

        <section class="img-banner-section">
            <div class="img-banner-div">
                <img src="{{ asset('assets/images/about-us.png')}}" class="img-fluid img-full" />
            </div>
        </section>

        {{-- <section class="branding-section">
            <div class="container-fluid">
                <div class="branding-div">
                    <img src="{{ asset('assets/images/ux-store.png')}}" class="img-fluid img-brand" alt="img" />
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="branding-root">
                                <ul>
                                    @foreach($services as $key=>$service)
                                        <li>
                                            <a href="{{ route('site.service',['slug'=>$service->slug]) }}" class="careers-div">
                                                <span class="number-span">{{sprintf("%02d",$key+1)}}</span>
                                                <h4>{{ $service->title }} </h4>
                                                <span class="arrow-right1"></span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="lines-hr lines-hr2 mt-40"></div>
                                <div class="button-common-div w100 mt-50">
                                    <a href="{{ route('site.work')}}" class="btn btn-common btn-red"> <span class="transform-text">View our case study!</span> <span class="arrow-right-circle"></span> </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section> --}}

        {{-- <section class="img-banner-section">
            <div class="img-banner-div">
                <img src="{{ asset('assets/images/about-us.png')}}" class="img-fluid img-full" />
            </div>
        </section> --}}

        <section class="committed-section">
            <div class="container-fluid">
                <div class="committed-div container-lg-2">
                    <div class="row align-items-center mt-30">
                        <div class="col-lg-12 col-md-12">
                            <div class="committed-root">
                                <h2>{!! getSession('site_about_excellence_title',  '') !!}</h2>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="content-apply">
                                            {!! getSession('site_about_excellence_description',  '') !!}
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="aboutus-value-section">
            <div class="container-fluid">
                <div class="the-team-div container-lg-2">

                    <div class="heading-div">
                        <div class="row align-items-center">
                            <div class="col-lg-4 col-md-4">
                            {!! getSession('site_about_core_values_title',  '') !!}
                                
                            </div>
                            <div class="col-lg-8 col-md-8">
                                <p>{!! getSession('site_about_core_values_description',  '') !!}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="image-slider-section">
            <div class="container-fluid">
                <div class="image-slider-inner">
                    <div class="row">

                        <div class=" owl-carousel img-slide-carousel owl-theme">
                            <!-- item start -->
                            <div class="item">
                                <div class="col-lg-12">
                                    <div class="img-slide">
                                        <img src="{{ asset('assets/images/image-slide.png')}}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="creative-agency-section ">
            <div class="container-fluid">
                <div class="creative-agency-inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="creative-agency-heading">

                                <div class="row">
                                    <div class="col-lg-6">
                                        {!! getSession('site_about_creative_agencies_title',  '') !!}
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                    <h4>{!! getSession('site_about_creative_agencies_description',  '') !!}</h4>
                                    </div>
                                </div>

                               
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section>


        @include('layouts.site.shared.technology')
        
        @include('layouts.site.shared.services')

    </div>
</div>
@endsection