@extends('layouts.site.index')
@section('content')

<div class="inner-page">

    @component('layouts.site.shared.banners', ['banner' => $banner])

        @slot('images')
            <div class="slide slide1 " data-slide="we" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/we.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide2 " data-slide="developement" data-slide-type="image">
                <div class="slide_inner"  >
                    <img src="{{ asset('assets/images/development.jpg') }}" alt="" srcset="">
                </div>
            </div>
            <div class="slide slide3 " data-slide="partner" data-slide-type="video">
                <div class="slide_inner"  >
                    <video src="{{ asset('assets/images/partner.mp4') }}" autoplay preload  loop >
                        <source src="{{ asset('assets/images/partner.mp4') }}" type="video/mp4">
                    </video>
                </div>
            </div>
        @endslot
        @slot('description')
            <span id="slide-we" data-slide="we" data-slide-theme="dark" class="span-block main_slide">We are reliable</span>
            <span class="span-block bold-line"> 
                <a href="#" class="main_slide" data-slide="developement" data-slide-theme="light" >Developement</a> 
                <a href="#" class="main_slide" data-slide="partner" data-slide-theme="dark">Partner</a></span>
            <span class="span-block">since 2013</span>
        @endslot

        @slot('desc')
            <p>We are a digital agency that specializes in User Experience Design</p>
        @endslot
       
        @slot('nextsection')
            main-middle-area
        @endslot
        

    @endcomponent
    <!--  end of banner  -->


    <div class="main-middle-area">

        <section class="career-link-section">
            <div class="container-fluid">
                <div class="career-link-div container-lg">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="link-root">
                                <ul>
                                @if(count($careers) > 0)
                                @foreach($careers as $key => $career)
                                    <li>
                                        <a href="{{route('site.career',['slug'=>$career->slug])}}" class="careers-div">
                                            <span class="number-span">{{sprintf("%02d",$key+1)}} </span>
                                            <h4>{{$career->designation}}</h4>
                                            <span class="arrow-right1"></span>
                                        </a>
                                    </li>
                                @endforeach
                                @endif
                                </ul>
                            </div>
                            <div class="lines-hr lines-hr2"></div>
                            <div class="career-p">
                                <p>{!! getSession('site_careers_tagline_below_career_list',  '') !!}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="interview-section changetheme">
            <div class="container-fluid">
                <div class="interview-div align-items-center  ">

                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6">
                            <div class="content-div">
                                <div class="heading-div">
                                    {!! getSession('site_careers_middle_banner_title',  '') !!}
                                    <p>{!! getSession('site_careers_middle_banner_desc',  '') !!}</p>
                                    <div class="button-common-div">
                                        <a href="#" class="btn btn-common btn-white"> <span class="transform-text">View case study</span> <span class="arrow-right-circle"></span> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="interview-thumb">
                                <img src="assets/images/interview.png" class="img-fluid img-center" alt="" />
                                <div class="outlined-box"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="view-6-section">
            <div class="container-fluid">
                <div class="view-6-div ">

                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 ">
                            <div class="content-div">
                                <div class="heading-div">
                                    <p>Want to see more?</p>
                                    <h2><a href="#" class="link"> <span class="span-block"> View Our</span> <span class="span-block">Case Study</span></a></h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 box-shadow">
                            <div class="content-div">
                                <div class="heading-div">
                                    <p>Discuss a project or arrange a demo?</p>
                                    <h2><a href="#" class="link"> <span class="span-block"> Want To </span> <span class="span-block">Say Hello!</span></a></h2>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


    </div>
</div>
@endsection
