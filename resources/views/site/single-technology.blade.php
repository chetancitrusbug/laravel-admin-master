@extends('layouts.site.index')

 

@section('content')


@php 
            $cover = $technology->manyfile->where('file_type','technologies_single_cover')->first();
            $param = unserialize($technology->param);
            
            $techservices = @$param['services'];
            if($techservices){
                $tmp = explode("\n",$techservices);
                if($tmp){
                    $techservices = array_chunk($tmp,count($tmp)/2);
                }
            }

            $hiretext = @$param['hiretext'];
             
@endphp
        

<div class="inner-page">

    <section class="banner-section banner-workdetail-page banner-technology-detail changetheme">
        <!-- <div class="line-1"></div> -->
        <div class="banner-div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">    
                        @if($cover)
                        <img src="{{ asset($cover->file_thumb_url) }}" class="banner-img" alt="banner" />
                        @endif

                        
                        <div class="content-div">
                            <div class="heading-content heading-workdetail">
                                <div class="view-allprojects">
                                    <a href="#!">{{$technology->title}}</a>
                                </div>
                                <h1> {!! @$param['tag_line']  !!} </h1>
                            </div>
                           
                        </div>
                    </div>
                    </div>
                    <div class="row"> 
                    <div class="col-lg-5">
                        
                        <div class="banner-content-workdetail">
                            {{-- <div class="content-one">
                                <p><a href="#">PHP and laravel</a></p>
                                <p><a href="#">Python</a></p>
                                <p><a href="#">App developement</a></p>
                            </div> --}}

                            


                            <div class="content-two">
                                <p><a href="#main-middle-area">View Description</a></p>

                                <div class="arrrow-div">
                                    <a href="#"><img src="{{ asset('assets/images/icons/arrow-verticle-line.png')}}" class="arrow-lines arrow-horizontal-lines" alt="arrow" /></a>
                                </div>
                            </div>
                            
                        </div>
                    </div> 
                    {{-- <div class="col-lg-7">
                        <div class="banner-img-right">
                            <div class="banner-work-img">
                                <img src="assets/images/iMac-technology-banner.png" alt="">
                            </div>
                        </div>
                        
                    </div> --}}
                </div>        
            </div>
        </div>
    </section><!--  end of banner  -->

     
 


    <div class="main-middle-area" id="main-middle-area" >


        <section class="creative-agency-section ">
            <div class="container-fluid">
                <div class="creative-agency-inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="creative-agency-heading">
                                <h4> 
                                    {!! $technology->desc !!} 
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="tech-detail-section ">
            <div class="container-fluid">
                <div class="tech-detail-inner container-lg-1">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="tech-detail-heading">
                                {!! $technology->description !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="tech-icon-box-root">
                                @php 
                                    $tech_images = $technology->manyfile->where('file_type','technologies_multiple_tech');
                                    
                                @endphp
                                @if($tech_images)
                                    @foreach($tech_images as $tech_image)
                                        <div class="tech-icon-box">
                                            <img src="{{ asset($tech_image->file_thumb_url) }}" alt="technology">
                                        </div>
                                    @endforeach
                                @endif
                                 
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </section>



        {{-- <section class="image-slider-section">
            <div class="container-fluid">
                <div class="image-slider-inner">
                    <div class="row">

                        <div class=" owl-carousel img-slide-carousel owl-theme">
                            <!-- item start -->
                            <div class="item">
                                <div class="col-lg-12">
                                    <div class="img-slide">
                                        <img src="{{ asset('assets/images/image-slide.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}


        



        <section class="scope-of-work-section changetheme">
            <div class="container-fluid">
                <div class="scope-of-work-inner">
                    <div class="row">
                        <div class="col-lg-12">
                             <div class="scope-of-work-heading">
                             <h2><a herf="#" class="heading-link"> {{$technology->title}} Development Services </a></h2>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        @if($techservices)
                        @foreach($techservices as $techservice)
                        <div class="col-lg-6">
                            <div class="scope-of-work-listing">
                                <ul>
                                    @foreach($techservice as $ser)
                                    <li>{{ $ser }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        
                    </div>
                </div>
            </div>
        </section>


 
        

        <section class="related-project-section">
            <div class="container-fluid">
                <div class="related-project-div container-lg1">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="heading-div">
                                <h2>
                                    <span class="span-block"> {{ $technology->title }}  <a href="#" class="link link6">Projects</a> </span>
                                </h2>
                                <p>We are a digital agency that specializes in User Experience Design</p>
                            </div>
                        </div>
                    </div>

                    @component('layouts.site.shared.projectslider', ['projects' => $technology->projects]) 
                    @endcomponent
                </div>
            </div>
        </section>

        @component('layouts.site.shared.hiredeveloper', ['developer' => $technology->title,'hiretext' => @$hiretext ]) 
        @endcomponent

        @include('layouts.site.shared.technology')

        @component('layouts.site.shared.testimonial', ['testimonials' => $testimonials]) 
        @endcomponent

        @include('layouts.site.shared.services')

    </div>
</div>
@endsection
