<div class="row ">


    {!! Form::hidden('module', $module ) !!}
   
    <div class="col-md-6">
      
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="url" class="">
                <span class="field_compulsory">*</span>@lang('settings.label.fkey')
            </label>

            {!! Form::text('fkey', null,['id'=>'fkey','class' => 'form-control']) !!}
            

			{!! $errors->first('url', '<p class="help-block text-danger">:message</p>') !!}
        </div>
 
		
		<div class="form-group{{ $errors->has('fvalue') ? ' has-error' : ''}}">
            <label for="description" >
                <span class="field_compulsory"></span>@lang('settings.label.fvalue')
            </label>
            <div >
                


                @if(@$item->ftype == 'text')
                {!! Form::text('fvalue', null , ['id'=>'fvalue','class' => 'form-control']) !!}
                @elseif(@$item->ftype == 'textarea')
                {!! Form::textarea('fvalue', null , ['id'=>'fvalue','class' => 'form-control']) !!}
                @elseif(@$item->ftype == 'editor')
                {!! Form::textarea('fvalue', null , ['id'=>'fvalue','class' => 'form-control editor']) !!}
                @elseif(@$item->ftype == 'image')
                
                 

                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="fvalue" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>
                    <input id="fvalue" name="fvalue" class="form-control" type="text" value="{{@$item->fvalue}}" name="filepath">
                </div>
                <img id="holder" src="{{@$item->fvalue}}"  style="margin-top:15px;max-height:100px;">

                @else
                {!! Form::textarea('fvalue', null , ['id'=>'fvalue','class' => 'form-control']) !!}
                @endif


				{!! $errors->first('fvalue', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <label for="description" >
                <span class=""></span>@lang('settings.label.finformation')
            </label>
            @if(getSess('is_super_admin'))
            <div >
                {!! Form::textarea('finformation', null , ['id'=>'finformation','class' => 'form-control']) !!}
				{!! $errors->first('finformation', '<p class="help-block text-danger">:message</p>') !!}
            </div>
            @else
            <div >
                
                {!! @$item->finformation !!}

            </div>
            @endif
            
        </div>

        @if(getSess('is_super_admin'))
        <div class="form-group">
            <label for="description" >
                <span class=""></span>@lang('settings.label.ftype')
            </label>
            <div >
                {!! Form::select('ftype',array('text' => 'Text', 'textarea' => 'Textarea', 'image' => 'Image','editor' => 'Editor'), null , ['id'=>'ftype','class' => 'form-control','disabled'=> !getSess('is_super_admin')]) !!}
				{!! $errors->first('ftype', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
        @endif
		
		 
		
		<div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
   
        
    </div>
   
    
</div>


@if(@$item->ftype == 'editor')
@section('js')
@parent
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
@endsection


@push('scripts')
<script>
  var options = {
    filebrowserImageBrowseUrl: '/media-manager?type=Images',
    filebrowserImageUploadUrl: '/media-manager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/media-manager?type=Files',
    filebrowserUploadUrl: '/media-manager/upload?type=Files&_token='
  };
 
CKEDITOR.replace('fvalue', options);
// $('textarea#desc').ckeditor(options);
</script>
 
@endpush
@endif