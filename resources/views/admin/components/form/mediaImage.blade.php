 
<div class="form-group{{ $errors->has($name) ? ' has-error' : ''}}">
    <label for="description" >
        @lang($context.'.label.'.$name) 
    </label>
    <div >
        <div class="input-group">
            <span class="input-group-btn">
                <a id="lfm_{{$name}}" data-input="{{$name}}" data-preview="holder_{{$name}}" class="btn btn-primary">
                    <i class="fa fa-picture-o"></i> Choose @lang($context.'.label.'.$name) 
                </a>
            </span>
            {!! Form::text($name, $value , array_merge(['id'=>$name] , $attributes )  ) !!}
        </div>
        <img id="holder_{{$name}}" src="{{@$value}}"  style="margin-top:15px;max-height:100px;">
    </div>
</div>


@push('scripts')
<script src="/vendor/laravel-filemanager/js/lfm.js"></script> 
<script>
var domain = "{{ URL::to('/media-manager') }}";
$('#lfm_icon').filemanager('image', {prefix: domain});
</script>
 
@endpush