<?php
return [
    'title' => 'Rolls',
    
    'create' => [
        'title' => "Create New Roll",
        'field' =>[
            'title' => 'Title',
            'backendAccess'=> 'Can Access Backend'
        ]
    ],
    'edit' => [
        'title' => "Edit Roll",
        'field' =>[
            'title' => 'Title',
            'backendAccess'=> 'Can Access Backend'
        ]
    ],
    'label' => [
        
    ]

];

?>