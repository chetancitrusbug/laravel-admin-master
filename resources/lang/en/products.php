<?php
return [
    'currency'=>[
        'USD'=>[
            'symbole'=>'$',
            'display_name'=>"USD"
        ],
        'EUR'=>[
            'symbole'=>'€',
            'display_name'=>"EURO"
        ],
    ],
];

?>